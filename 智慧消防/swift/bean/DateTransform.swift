//
//  DateTransform.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit

class DateTransform: TransformType {
    typealias Object = Date
    
    typealias JSON = NSNumber
    
    func transformFromJSON(_ value: Any?) -> Date? {
        let timeInterval:TimeInterval = TimeInterval(value as!NSInteger/1000)
        let date = Date(timeIntervalSince1970: timeInterval)
        
        return date;
    }
    
    func transformToJSON(_ value: Date?) -> NSNumber? {
        if(value == nil){
            return nil;
        }
        
        return NSNumber(value:(NSInteger)((value?.timeIntervalSince1970)!*1000))
    }
    
   
    
    

}

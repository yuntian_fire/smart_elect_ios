//
//  SearchView.m
//  智慧消防
//
//  Created by yuntian on 2019/1/2.
//  Copyright © 2019年 yuntian. All rights reserved.
//

#import "SearchView.h"

@implementation SearchView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib{
    [super awakeFromNib];
    [self addSubview:self.contentView];
    [_contentView addSubview:self.boundView];
    [_boundView addSubview:self.fieldTex];
    [_boundView addSubview:self.searchButton];
    _contentView.alpha = 0.6;
    self.userInteractionEnabled = NO;
}


-(UIView *)contentView{
    if (!_contentView ){
        _contentView = [[UIView alloc]init];
        _contentView.backgroundColor = [UIColor darkGrayColor];

    }
    return _contentView;
}

-(void)layoutSubviews{
    [super layoutSubviews];
     _contentView.frame = CGRectMake(10, -50, self.frame.size.width - 20, 50);
     _boundView.frame = CGRectMake(10, 10, _contentView.frame.size.width-20, 30);
    _fieldTex.frame = CGRectMake(0, 0, _boundView.frame.size.width-61, 30);
    _searchButton.frame = CGRectMake(_boundView.frame.size.width - 60, 0, 60, 30);
}

-(UIView *)boundView{
    if(!_boundView){
        _boundView = [[UIView alloc]init];
        _boundView.backgroundColor = [UIColor darkGrayColor];
                _boundView.layer.cornerRadius = 5;
                _boundView.layer.masksToBounds = YES;
    }
    return _boundView;
}

-(UIButton *)searchButton{
    if (!_searchButton) {
        _searchButton = [[UIButton alloc]init];
        [_searchButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_searchButton setBackgroundColor:[UIColor whiteColor]];
        [_searchButton setTitle:@"搜索" forState:UIControlStateNormal];
        _searchButton.font = [UIFont systemFontOfSize:12];

    }
    
    return _searchButton;
}

-(UITextField *)fieldTex{
    if (!_fieldTex) {
        _fieldTex = [[UITextField alloc]init];
        [_fieldTex setBackgroundColor:[UIColor whiteColor]];
        _fieldTex.placeholder = @"项目名称/项目编号/电话号码";
        _fieldTex.font = [UIFont systemFontOfSize:12];
    }
    return _fieldTex;
}

-(void)setIsOpen:(BOOL)isOpen{
    _isOpen = isOpen;
    CGRect frame = _contentView.frame;
    
    if (isOpen) {
        frame.origin.y = 0;
    }else{
        frame.origin.y = -50;
    }
    
    [UIView animateWithDuration:0.15 animations:^{
        _contentView.frame = frame;
    }];
    self.userInteractionEnabled = isOpen;
    
    
}
@end

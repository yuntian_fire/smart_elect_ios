//
//  DevMonitorPage.m
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "DevMonitorPage.h"
#import "MonitorAlarmCell.h"
#import "MonitorEventCell.h"
#import "Util.h"
#import "PagerPopViewController.h"
#import "DevInfoViewController.h"
@implementation DevMonitorPage

static NSString *monitorAlarmCell = @"MonitorAlarmCell";
static NSString *monitorEventCell = @"MonitorEventCell";
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
-(void)awakeFromNib{
    // Drawing code
    [super awakeFromNib];
    UINib *nib = [UINib nibWithNibName:@"MonitorAlarmCell" bundle:[NSBundle mainBundle]];
    [self.alarmTableView registerNib:nib forCellReuseIdentifier:monitorAlarmCell];
    self.alarmTableView.rowHeight = 25;
    self.alarmTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    nib = [UINib nibWithNibName:@"MonitorEventCell" bundle:[NSBundle mainBundle]];
    [self.eventTableView registerNib:nib forCellReuseIdentifier:monitorEventCell];
    self.eventTableView.rowHeight = self.eventTableView.frame.size.height/8;
    self.eventTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self initLineChartDesign];
}



-(void)initLineChartDesign{
    // 不显示数据描述
    _lineChartView.chartDescription.enabled = NO;
    // 没有数据的时候，显示“暂无数据”
    _lineChartView.noDataText = @"暂无数据";
    // 不显示表格颜色
    _lineChartView.drawGridBackgroundEnabled = NO;
    // 不可以缩放
    _lineChartView.scaleXEnabled = NO;
    _lineChartView.scaleYEnabled = NO;
    // 不显示y轴右边的值

    _lineChartView.rightAxis.enabled = NO;
    // 不显示图例
    ChartLegend *legend = _lineChartView.legend;
    legend.enabled = NO;
    
    //        // 向左偏移15dp，抵消y轴向右偏移的30dp
    //        chart.setExtraLeftOffset(-15);

    _lineChartView.extraBottomOffset = 10;
    
   
    ChartXAxis *xAxis = _lineChartView.xAxis;
    //显示x轴
    xAxis.drawAxisLineEnabled = YES;
    // 设置x轴数据的位置
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelTextColor = [UIColor blackColor];
    xAxis.labelFont = [UIFont systemFontOfSize:10];
    xAxis.drawGridLinesEnabled = NO;
//    xAxis.setGridColor(Color.parseColor("#30FFFFFF"));
    xAxis.labelCount = 10;
    xAxis.axisMaximum = 11;
    xAxis.axisMinimum = 0;
    NSArray *xValues =@[@"2:00",@"4:00",@"6:00",@"8:00",@"10:00",@"12:00",@"14:00",@"16:00",@"18:00",@"20:00",@"22:00",@"24:00"];
    
    
    xAxis.valueFormatter = [[ChartIndexAxisValueFormatter alloc]initWithValues:xValues];
   
    xAxis.yOffset = 7;
    
    ChartYAxis *yAxis = _lineChartView.leftAxis;
    // 不显示y轴
    yAxis.drawAxisLineEnabled = NO;
    // 设置y轴数据的位置
    yAxis.labelPosition = YAxisLabelPositionOutsideChart;
    // 不从y轴发出横向直线
    yAxis.drawGridLinesEnabled = NO;
    yAxis.labelTextColor = [UIColor blackColor];
    yAxis.labelFont = [UIFont systemFontOfSize:10];
    // 设置y轴数据偏移量
    yAxis.xOffset = 10;
    yAxis.yOffset = 3;
    
    yAxis.axisMinimum = 0;
}

- (IBAction)clickBtnVol:(id)sender {
    //点击电压按钮
    _btnTemp.selected = NO;
    _btnCurrent.selected = NO;
    _btnVol.selected = YES;
    _btnResiduleCurrent.selected = NO;
    [self setLineChartData:_voltageArray];
    
}
- (IBAction)clickBtnTemp:(id)sender {
    //点击温度按钮
    _btnTemp.selected = YES;
    _btnVol.selected = NO;
    _btnCurrent.selected = NO;
    _btnResiduleCurrent.selected = NO;
    [self setLineChartData:_tempArray];
}
- (IBAction)clickBtnCurrent:(id)sender {
    //点击电流按钮
    _btnTemp.selected = NO;
    _btnVol.selected = NO;
    _btnCurrent.selected = YES;
    _btnResiduleCurrent.selected = NO;
    [self setLineChartData:_currentArray];
    
}
- (IBAction)clickBtnResCurrent:(id)sender {
    //点击剩余电流按钮
    _btnTemp.selected = NO;
    _btnVol.selected = NO;
    _btnCurrent.selected = NO;
    _btnResiduleCurrent.selected = YES;
    [self setLineChartData:_rCurrentArray];
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (tableView == _eventTableView) {
        MonitorEventCell *cell = [self.eventTableView dequeueReusableCellWithIdentifier:monitorEventCell];
        if(indexPath.row<_dtuEventList.count){
            FireDtuEventBean *eventBean =  _dtuEventList[indexPath.row];
            cell.labelEventName.text = eventBean.eventName;
            cell.labelEventTime.text =[Util dateStrFromDate:eventBean.eventTime];
        }
        return cell;
    }else{
        MonitorAlarmCell *cell = [self.alarmTableView dequeueReusableCellWithIdentifier:monitorAlarmCell];
        FireDevAlarmBean *alarmBean = _devAlarmArray[indexPath.row];
        cell.labelAlarmChannel.text = alarmBean.passageName;
        cell.labelAlarmType.text = alarmBean.alarmTypeName;
        cell.labelNoteMessage.text = alarmBean.devStatusNm;
        
        cell.labelAlarmTime.text = [Util dateStrFromDate:alarmBean.alarmTime];
        
        
        return cell;
    }
  
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _alarmTableView) {
        return _devAlarmArray?_devAlarmArray.count:0;
    }else{
        return 8;
    }
    
}
-(void)setMonitorBean:(FireDevMonitorBean *)monitorBean{
    _monitorBean = monitorBean;
    _devAlarmArray = monitorBean.alarmList;
    [_alarmTableView reloadData];
    _dtuEventList = monitorBean.dtuEventList;
    [_eventTableView reloadData];
    [self initLineChartData:monitorBean.senseList];
    
    if(_btnTemp.isSelected){
        [self setLineChartData:_tempArray];
    }else if(_btnCurrent.isSelected){
        [self setLineChartData:_currentArray];
    }else if(_btnResiduleCurrent.isSelected){
        [self setLineChartData:_rCurrentArray];
    }else if(_btnVol.isSelected){
        [self setLineChartData:_voltageArray];
    }
    // 用户类型：0：项目负责人（可以派发维保任务）；1：维保人员（只能接收维保任务）2：其他人员（不能进行维保操作）
    int userType = _monitorBean.userType.intValue;
    
    // 报警处理按钮状态：0：设备正常（按钮灰色不可点，显示报警处理）；1：设备异常，需要维保，报警未确认（按钮不可点，显示报警处理）；2：设备异常，需要维保，报警已确认，维保处理为开始（按钮可点（维保人员），显示报警处理，对于项目负责人来说按钮不可点）；
    // 3：设备异常，需要维保，报警已确认，维保处理已开始（按钮不可点，显示报警处理中）
    int alarmExecStat = _monitorBean.alarmExecStat.intValue;
    if (userType&& alarmExecStat) {
        switch (userType) {
            case 0:
                _btnAlarmProcess.enabled = NO;
                _btnAlarmConfirm.enabled = NO;
                if (alarmExecStat == 1) {
                    _btnPushMsg.enabled = YES;
                    
                } else {
                    _btnPushMsg.enabled = NO;
                }
                
                
                break;
                
            case 1:
                
                switch (alarmExecStat) {
                    case 0:
                        _btnAlarmConfirm.enabled = NO;
                        _btnAlarmProcess.enabled = NO;
                        _btnPushMsg.enabled = NO;
                        
                        break;
                    case 1:
                        _btnAlarmConfirm.enabled = YES;
                        _btnAlarmProcess.enabled = NO;
                        _btnPushMsg.enabled = NO;
                        break;
                    case 2:
                        _btnAlarmConfirm.enabled = NO;
                        _btnAlarmProcess.enabled = YES;
                        _btnPushMsg.enabled = NO;
                        
                        
                        break;
                    case 3:
                        _btnAlarmConfirm.enabled = NO;
                        _btnAlarmProcess.enabled = NO;
                        _btnPushMsg.enabled = YES;
                        
                        break;
                }
                
                break;
                
                
            default:
                _btnAlarmConfirm.enabled = NO;
                _btnAlarmProcess.enabled = NO;
                _btnPushMsg.enabled = NO;
                break;
        }
        
        switch (alarmExecStat) {
            case 0:
            case 1:
            case 2:
                [_btnAlarmProcess setTitle:@"报警处理" forState:UIControlStateNormal];
                [_btnAlarmProcess setTitle:@"报警处理" forState:UIControlStateDisabled];
                break;
            case 3:
                [_btnAlarmProcess setTitle:@"报警处理中..." forState:UIControlStateNormal];
                [_btnAlarmProcess setTitle:@"报警处理中..." forState:UIControlStateDisabled];
                break;
                
        }
    }

}
-(void)setLineChartData:(NSArray<NSArray<FireDevSenseStatBean*>*>*)list{
    NSMutableArray *chartDatas = [[NSMutableArray alloc]init];
 
    float min = 0;
    if (list.count > 0 && list[0].count > 0){
       
        min =  [[[NSNumberFormatter alloc]init] numberFromString:list[0][0].devValue].floatValue;
    }
    
    for (int i = 0; i < list.count; i++) {
        NSMutableArray<ChartDataEntry*>* data1 = [[NSMutableArray alloc]init];
        for (int j = 0; j < list[i].count; j++) {
          
            [data1 addObject:  [[ChartDataEntry alloc]initWithX:list[i][j].xPoint.floatValue y:[[[NSNumberFormatter alloc]init] numberFromString:list[i][j].devValue].floatValue]];
            if ([[[NSNumberFormatter alloc]init] numberFromString:list[i][j].devValue].floatValue < min) {
                min = [[[NSNumberFormatter alloc]init] numberFromString:list[i][j].devValue].floatValue;
            }
        }
        LineChartDataSet *lineDataSet = [[LineChartDataSet alloc]initWithValues:data1];
        
      
        
        lineDataSet.drawValuesEnabled = NO;
        
        lineDataSet.colors =  @[[UIColor blackColor]];
        lineDataSet.circleColors = @[[UIColor blackColor]];
        lineDataSet.lineWidth = 2;
        //        lineDataSet.setCircleColor(randColorCode);
        lineDataSet.circleRadius = 3;
        lineDataSet.lineWidth = 2;
//        lineDataSet.setCircleColor(randColorCode);
        lineDataSet.drawCirclesEnabled = YES;
        lineDataSet.drawCircleHoleEnabled = NO;
        
        [chartDatas addObject:lineDataSet];
    }
//   假数据测试
//    for (int i = 0; i<10; i++) {
//        NSMutableArray<ChartDataEntry*>* data1 = [[NSMutableArray alloc]init];
//        for (int j = 0; j<12; j++) {
//             [data1 addObject:  [[ChartDataEntry alloc]initWithX:j y:j+2]];
//        }
//        LineChartDataSet *lineDataSet = [[LineChartDataSet alloc]initWithValues:data1];
//
//        lineDataSet.drawValuesEnabled = NO;
//
//
//        lineDataSet.colors =  @[[UIColor blackColor]];
//        lineDataSet.circleColors = @[[UIColor blackColor]];
//        lineDataSet.lineWidth = 2;
//        //        lineDataSet.setCircleColor(randColorCode);
//        lineDataSet.circleRadius = 3;
//        lineDataSet.drawCirclesEnabled = YES;
//        lineDataSet.drawCircleHoleEnabled = NO;
//        [chartDatas addObject:lineDataSet];
//    }
    
    ChartYAxis *axisLeff = _lineChartView.leftAxis;
    int minInt = (int) (min - 10);
    axisLeff.axisMinimum = minInt;
    
    LineChartData *lineData = [[LineChartData alloc]initWithDataSets:chartDatas];
   
    [_lineChartView clear];
    _lineChartView.data = lineData;
    [_lineChartView setNeedsDisplay];

}
-(void)initLineChartData:(NSArray<FireDevSenseStatBean*>*)array{
    NSMutableArray<NSMutableArray<FireDevSenseStatBean*>*> *rCurrentArray = [[NSMutableArray alloc]init];
    NSMutableArray<NSMutableArray<FireDevSenseStatBean*>*> *currentArray = [[NSMutableArray alloc]init];
    NSMutableArray<NSMutableArray<FireDevSenseStatBean*>*> *voltageArray = [[NSMutableArray alloc]init];
    NSMutableArray<NSMutableArray<FireDevSenseStatBean*>*> *tempArray = [[NSMutableArray alloc]init];

    if (array) {
        NSMutableArray<FireDevSenseStatBean*> *list = [[NSMutableArray alloc]init];
        for (int i = 0; i < array.count - 1; i++) {
            FireDevSenseStatBean *fireDevSenseStatBean = [array objectAtIndex:i];
            NSInteger circle = fireDevSenseStatBean.circle.integerValue;
            NSInteger passage = fireDevSenseStatBean.passage.integerValue;
            NSInteger partAddr = fireDevSenseStatBean.partAddr.integerValue;
            
            FireDevSenseStatBean *fireDevSenseStatBean1 = [array objectAtIndex:i+1];
            NSInteger passage1 = fireDevSenseStatBean1.passage.integerValue;
            NSInteger circle1 = fireDevSenseStatBean1.circle.integerValue;
            NSInteger partAddr1 = fireDevSenseStatBean1.partAddr.integerValue;
            
            NSString *format = [NSString stringWithFormat: @"%ld%ld%ld", circle, partAddr, passage];
             NSString *format1 = [NSString stringWithFormat: @"%ld%ld%ld", circle1, partAddr1, passage1];
            if ([format isEqualToString:format1]) {
                [list addObject:fireDevSenseStatBean];
                NSLog(@" %@",format);
                NSLog(@" %@",format1);
                NSLog(@" ------------------------------------------------");
            } else {
                [list addObject:fireDevSenseStatBean];
                NSLog(@" %@",format);
                NSLog(@" %@",format1);
                NSLog(@" ------------------***********************************************************************************************************-----------------------------");
                switch (fireDevSenseStatBean.testName ? fireDevSenseStatBean.testName.integerValue :0 ) {
                    case 2://剩余电流
                        [rCurrentArray addObject:list];
                        break;
                    case 5://三相电流
                    case 7://单相电流
                        [currentArray addObject:list];
                        break;
                    case 8://交流电压
                        [voltageArray addObject:list];
                        break;
                    case 11://温度
                        [tempArray addObject:list];
                        break;
                }
                list = [[NSMutableArray alloc]init];
            }
            
            if (i == array.count - 2) {
                [list addObject:fireDevSenseStatBean1];
                switch (fireDevSenseStatBean1.testName ? fireDevSenseStatBean1.testName.intValue : 0) {
                    case 2://剩余电流
                        [rCurrentArray addObject:list];
                        break;
                    case 5://三相电流
                    case 7://单相电流
                        [currentArray addObject:list];
                        break;
                    case 8://交流电压
                        [voltageArray addObject:list];
                        break;
                    case 11://温度
                        [tempArray addObject:list];
                        break;
                }
            }
            
        }
    }
    [self adjustList:rCurrentArray];
    [self adjustList:currentArray];
    [self adjustList:voltageArray];
    [self adjustList:tempArray];
    
    _rCurrentArray = rCurrentArray;
    _currentArray = currentArray;
    _voltageArray = voltageArray;
    _tempArray = tempArray;

}

-(void)adjustList:(NSMutableArray<NSMutableArray<FireDevSenseStatBean*> *>*)list{
    for (int i = 0; i < list.count; i++) {
        NSMutableArray<FireDevSenseStatBean*> *fDSSBList = [list objectAtIndex:i];
        int x = 0;
        NSMutableArray<FireDevSenseStatBean*> *averageList = [[NSMutableArray alloc]init];
        for (int j = 0; j < fDSSBList.count; j++) {
            FireDevSenseStatBean *fireDevSenseStatBean = [fDSSBList objectAtIndex:j];
            NSDate *detectTime = fireDevSenseStatBean.detectTime;
            
            NSInteger hourOfDay = [Util getDateComponentFromDate:detectTime].hour ;
            
            if (x < 24) {
                if (hourOfDay >= x && hourOfDay < x + 2) {
                    [averageList addObject:fireDevSenseStatBean];
                    if (j == fDSSBList.count - 1) {
                        float sum = 0;
                        for (int k = 0; k < averageList.count; k++) {
                            NSString * devValue = averageList[k].devValue;
                           
                            float v =  [[[NSNumberFormatter alloc]init]numberFromString:devValue].floatValue;
                            sum += v;
                            if (k < averageList.count - 1) {
                                [fDSSBList removeObject:averageList[k]];
                            } else {
                                float averageValue = sum / averageList.count;
                                averageList[k].devValue = [NSString stringWithFormat:@"%f",averageValue];
                               
                                averageList[k].xPoint =  [NSNumber numberWithFloat:x / 2];
                                [averageList removeAllObjects];
                            }
                        }
                    }
                    
                } else {
                    
                    float sum = 0;
                    for (int k = 0; k < averageList.count; k++) {
                        NSString *devValue = averageList[k].devValue;
                        float v = [[[NSNumberFormatter alloc]init]numberFromString:devValue].floatValue;
                        sum += v;
                        if (k < averageList.count - 1) {
                            [fDSSBList removeObject:averageList[k]];
                            j--;
                        } else {
                            float averageValue = sum / averageList.count;
                            averageList[k].devValue = [NSString stringWithFormat:@"%f",averageValue];
                            averageList[k].xPoint = [NSNumber numberWithFloat:x / 2];
                            [averageList removeAllObjects];
                        }
                    }
                    j--;
                    x += 2;
                }
            }
        }
    }
    
    
}

- (IBAction)clickRefresh:(UIButton *)sender {
    
}

- (IBAction)clickHeartBeat:(UIButton *)sender {
    sender.selected = !sender.selected;
}
- (IBAction)clickPushMessage:(UIButton *)sender {
    _selfVC.target = VCTargetPushMessage;
    [_selfVC performSegueWithIdentifier:@"dev_info_to_push" sender:_selfVC];
    
}
- (IBAction)clickSelfInsp:(id)sender {
    if (![ROLE_PROJ_PRIN isEqualToString:[User user].roleId]&&![ROLE_MAINT_USER isEqualToString:[User user].roleId]) {
        UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员和维保人员可以修改设备信息" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alerView show];
        return;
        
    }
    
    [self sendCmd:CMD_SELF_INSPECTION With:nil];
    
}
- (IBAction)clickReset:(id)sender {
    if (![ROLE_PROJ_PRIN isEqualToString:[User user].roleId]&&![ROLE_MAINT_USER isEqualToString:[User user].roleId]) {
        UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员和维保人员可以修改设备信息" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alerView show];
        return;
        
    }
    [self sendCmd:CMD_RESET With:nil];
    
}
- (IBAction)clickTheMute:(id)sender {
    if (![ROLE_PROJ_PRIN isEqualToString:[User user].roleId]&&![ROLE_MAINT_USER isEqualToString:[User user].roleId]) {
        UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员和维保人员可以修改设备信息" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alerView show];
        return;
        
    }
    [self sendCmd:CMD_THE_MUTE With:nil];
    
}

- (IBAction)clickAlarmProcess:(id)sender {
}

- (IBAction)clickShield:(id)sender {
    if (![ROLE_PROJ_PRIN isEqualToString:[User user].roleId]&&![ROLE_MAINT_USER isEqualToString:[User user].roleId]) {
        UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员和维保人员可以修改设备信息" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alerView show];
        return;
        
    }
     if(_monitorBean&&_monitorBean.senseShield.count>0){
         __block PagerPopViewController *popController = [[PagerPopViewController alloc]initWithConfirm:^{
             [self sendCmd:CMD_SHIELD With:  popController.cmd];
             
         } andCancel:^{
             
         }];
         FireSendCmdBean *cmd = [[FireSendCmdBean alloc]init];
         cmd.shieldBeanList = _monitorBean.senseShield;
         popController.cmd = cmd;
         
         popController.modalPresentationStyle = UIModalPresentationOverFullScreen;
         [_selfVC presentViewController:popController animated:NO completion:nil];
     }

}

- (IBAction)clickAlarmConfirm:(id)sender {
}

- (IBAction)clickExControl:(id)sender {
    if (![ROLE_PROJ_PRIN isEqualToString:[User user].roleId]&&![ROLE_MAINT_USER isEqualToString:[User user].roleId]) {
        UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员和维保人员可以修改设备信息" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alerView show];
        return;
        
    }
    if(_monitorBean&&_monitorBean.extShield.count>0){
        __block PagerPopViewController *popController = [[PagerPopViewController alloc]initWithConfirm:^{
            [self sendCmd:CMD_SHIELD With:  popController.cmd];
            
        } andCancel:^{
            
        }];
        FireSendCmdBean *cmd = [[FireSendCmdBean alloc]init];
        cmd.shieldBeanList = _monitorBean.extShield;
        popController.cmd = cmd;
        
        popController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [_selfVC presentViewController:popController animated:NO completion:nil];
        
    }
    

  
}

-(void)sendCmd:(int)CMD With:(FireSendCmdBean*)sendCmdBean{
    if (!sendCmdBean) {
        sendCmdBean = [[FireSendCmdBean alloc]init];
    }
    sendCmdBean.devId = _monitorBean.devId;
    sendCmdBean.cmd = [NSNumber numberWithInt:CMD];
    sendCmdBean.srcAddr = _monitorBean.srcAddr;
    [[NetWork network] sendCMD:sendCmdBean success:^(id data) {
        NSLog(@"%@",data);
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

@end

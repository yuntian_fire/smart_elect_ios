//
//  Const.h
//  智慧消防
//
//  Created by yuntian on 2018/11/28.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#ifndef Const_h
#define Const_h
#define MESSAGE_MAIN_HANDLE @"MESSAGE_MAIN_HANDLE"
#define MESSAGE_NAV_CONTROL @"MESSAGE_NAV_CONTROL"
#define MESSAGE_PAGE @"MESSAGE_PAGE"
#define MESSAGE_ADD_DEV @"MESSAGE_ADD_DEV"
#define MESSAGE_SCAN_RESULT @"MESSAGE_SCAN_RESULT"

//#define BASE_URL @"http://yuntiansoft.com:8080/product/"
//#define BASE_URL1 @"http://yuntiansoft.com:8080"

#define BASE_URL @"http://zjhrwl.net:8080/product/"
#define BASE_URL1 @"http://zjhrwl.net:8080"

#define API_LOGIN @"api/app/login"
#define API_GET_PROJS @"api/app/get/proj"
#define API_UPD_PROJ @"api/app/upd/proj"
#define API_GET_DEV_INFO @"api/app/get/devInfo"
#define API_GET_DEV_BASIC_INFO @"api/app/get/devBasicInfo"
#define API_UPD_DEV @"api/app/upd/dev"
#define API_GET_PROB_DEV @"api/app/get/probDev"
#define API_GET_DEV_CONFIG @"api/app/get/devConfig"
#define API_UPD_DEV_CONFIG @"api/app/upd/devConfig"
#define API_GET_DEV_MONITOR @"api/app/get/devMonitor"
#define API_SEND_CMD @"api/app/send/cmd"
#define API_GET_ALARM_HIST @"api/app/get/alarmHist"
#define API_GET_FAULT_HIST @"api/app/get/faultHist"
#define API_GET_INSP_PLAN @"api/app/plan/list"
#define API_GET_INSP_RECORD @"api/app/record/list"
#define API_SAVE_INSP_PLAN @"api/app/plan/save"
#define API_DEL_INSP_PLAN @"api/app/plan/delete"
#define API_QUERY_DEV_INFO @"api/app/plan/deviceInfo"
#define API_GET_SUBUSERS @"api/app/user/subordinate/list"
#define API_SAVE_MAINT @"api/app/maintenance/save"
#define API_PROCESS_MAINT @"api/app/maintenance/process"
#define API_GET_MAINT_LIST @"api/app/maintenance/list"
#define API_UPD_MAINT_TYPE @"api/app/maintenance/proc/state/update"
#define API_GET_USER_INFO @"api/app/get/userInfo"
#define USER_TYPE_SELF @"1"
#define DATE_FORMAT @"yyyy-MM-dd HH:mm:ss"
//命令集
#define CMD_THE_MUTE 3
#define CMD_RESET 4
#define CMD_SELF_INSPECTION 5
#define CMD_SHIELD 7
#define CMD_EXT_SHEILD 6
//刷新类型
#define TYPE_REFRESH @"1"
#define TYPE_GET_DEV_CONFIG @"2"

/**
 * 人员类型
 */
/** 角色：系统管理员 */
#define ROLE_SYS_ADMIN @"1"

/** 角色：普通管理员 */
#define ROLE_NORMAL_ADMIN @"2"

/** 角色：省级代理 */
#define String ROLE_PROV_AGENT @"3"

/** 角色：市级代理 */
#define ROLE_CITY_AGENT @"4"

/** 角色：县级或区域代理 */
#define ROLE_COUNTY_AGENT @"5"

/** 角色：项目负责人 */
#define ROLE_PROJ_PRIN @"6"

/** 角色：维保人员 */
#define ROLE_MAINT_USER @"7"

/** 角色：运营人员 */
#define ROLE_OPER_USER @"8"


typedef NS_ENUM(uint,VCTarget) {
    VCTargetDevInfo,
    VCTargetProjInfo,
    VCTargetAlarmList,
    VCTargetFaultList,
    VCTargetMaintTask,
    VCTargetMaintRecord,
    VCTargetChoosePosition,
    VCTargetPushMessage,
    VCTargetScanCode,
    VCTargetAddDev
};



#endif /* Const_h */

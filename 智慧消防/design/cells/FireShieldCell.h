//
//  FireShieldCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/21.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "智慧消防-Swift.h"
@interface FireShieldCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lableChannel;
@property (strong,nonatomic)FireDevShieldBean *shieldBean;
@property (weak, nonatomic) IBOutlet UIButton *btnShield;

@end

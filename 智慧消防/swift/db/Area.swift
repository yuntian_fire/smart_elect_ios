//
//  Area.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/13.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit

class Area: NSObject {
    var pid:NSNumber?
    var code:NSString?
    var name:NSString?
    var level:NSNumber?
    var cityCode:NSString?
    var center:NSString?
    var parentId:NSNumber?
    
}

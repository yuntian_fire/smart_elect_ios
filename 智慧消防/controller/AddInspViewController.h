//
//  AddInspViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/25.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EBDropdownListView.h"
#import "NetWork.h"
#import "Util.h"
@interface AddInspViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelInspP;

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *fieldDevId;
@property (weak, nonatomic) IBOutlet UITextField *fieldInspCycle;
@property (weak, nonatomic) IBOutlet UITextField *fieldAsiPhone;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropAsiNm;
@property (strong,nonatomic) FireInspPlanRequest *inspRequest;
@property (strong,nonatomic) NSString *devId;

@end

//
//  AddRandomInspVC.m
//  智慧消防
//
//  Created by yuntian on 2018/12/26.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "AddRandomInspVC.h"
#import "Util.h"
#import "ScanCodeViewController.h"
@interface AddRandomInspVC ()

@end

@implementation AddRandomInspVC{
    BOOL _keyboardIsVisible;
    UITapGestureRecognizer *backTap;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    [self.view addSubview:self.contentView];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center  addObserver:self selector:@selector(keyboardDidShow)  name:UIKeyboardDidShowNotification  object:nil];
    [center addObserver:self selector:@selector(keyboardDidHide)  name:UIKeyboardWillHideNotification object:nil];
    _keyboardIsVisible = NO;
    if (!_inspRequest) {
        _inspRequest = [[FireInspPlanRequest alloc]init];
    }
    
    if (_inspResponse) {
        _devIdContentView.hidden = NO;
        _devIdScanContentView.hidden = YES;
    }else{
        _devIdContentView.hidden = YES;
        _devIdScanContentView.hidden = NO;
    }
    backTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(back)];
    // Do any additional setup after loading the view.
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
 
}

- (void)keyboardDidShow
{
    _keyboardIsVisible = YES;
}

- (void)keyboardDidHide
{
    _keyboardIsVisible = NO;
}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    CGRect contentFrame = CGRectMake(10, self.view.frame.size.height/2 - 225, self.view.frame.size.width-20, 450);
    _contentView.frame = contentFrame;
    if (!_inspResponse) {

        _btnDelete.translatesAutoresizingMaskIntoConstraints = YES;
        CGRect frame = _btnDelete.frame;
        frame.size.width = 0;
        _btnDelete.frame = frame;
        
        _btnAdd.translatesAutoresizingMaskIntoConstraints = YES;
        frame = _btnAdd.frame;
        frame.origin.x = _contentView.frame.size.width/2 - frame.size.width - 0.5;
        _btnAdd.frame = frame;
        
        
        _btnCancel.translatesAutoresizingMaskIntoConstraints = YES;
        frame = _btnCancel.frame;
        frame.origin.x = _contentView.frame.size.width/2 + 0.5;
        _btnCancel.frame = frame;
        
        
        
    }

}


-(UIView *)contentView{
    if (!_contentView) {
        [[NSBundle mainBundle]loadNibNamed:@"AddRandomInspVc" owner:self options:nil];
    }

    return _contentView;
}
- (IBAction)clickBtnStartDate:(UIButton *)sender {

    UIDatePicker *datePicker = [[UIDatePicker alloc] init];//初始化一个UIDatePicker
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.locale =  [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"选择时间" message:@"\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];//初始化一个标题为“选择时间”，风格是ActionSheet的UIAlertController，其中"\n"是为了给DatePicker腾出空间
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];//设置输出的格式
        [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
        [sender setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
        _inspRequest.startTime = datePicker.date;

    }];



    [alert.view addSubview:datePicker];//将datePicker添加到UIAlertController实例中
    [alert addAction:confirm];//将确定按钮添加到UIAlertController实例中
    [self presentViewController:alert animated:YES completion:nil];

}
- (IBAction)clickBtnEndDate:(id)sender {
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];//初始化一个UIDatePicker
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.locale =  [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"选择时间" message:@"\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];//初始化一个标题为“选择时间”，风格是ActionSheet的UIAlertController，其中"\n"是为了给DatePicker腾出空间
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];//设置输出的格式
        [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
        [sender setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
        _inspRequest.endTime = datePicker.date;

    }];



    [alert.view addSubview:datePicker];//将datePicker添加到UIAlertController实例中
    [alert addAction:confirm];//将确定按钮添加到UIAlertController实例中
    [self presentViewController:alert animated:YES completion:nil];

}
-(void)changeWindowTouchBack:(BOOL)isWindowTouchBack{
    NSArray * arrayViews = [UIApplication sharedApplication].keyWindow.subviews;
    if (arrayViews.count>0) {
        //array会有两个对象，一个是UILayoutContainerView，另外一个是UITransitionView，我们找到最后一个
        UIView * backView = arrayViews.lastObject;
        //我们可以在这个backView上添加点击事件，当然也可以在其子view上添加，如下：
        //        NSArray * subBackView = [backView subviews];
        //        backView = subBackView[0];  或者如下
        //        backView = subBackView[1];
        
        if(isWindowTouchBack){
            backView.userInteractionEnabled = YES;
            [backView addGestureRecognizer:backTap];
        }else{
            backView.userInteractionEnabled = NO;
            [backView removeGestureRecognizer:backTap];
        }
      
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleMessage:) name:MESSAGE_SCAN_RESULT object:nil];
    [self changeWindowTouchBack:YES];
   
    
    NSString *user = [NSString stringWithFormat:@"%@/%@",[User user].userSelf.name,[User user].userSelf.mobile];
    _labelInspP.text = user;
    NSMutableArray *dataSource = [[NSMutableArray alloc]init];
    for (FirePrincipalBean *user in [User user].maintUserList) {
        [dataSource addObject:[[EBDropdownListItem alloc]initWithItem:nil itemName:user.name]];
    }
    _dropAsiNm.dataSource = dataSource;
    _dropAsiNm.customData = [User user].maintUserList;
    [_dropAsiNm setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        
        _fieldAsiPhone.text = ((FirePrincipalBean*)_dropAsiNm.customData[_dropAsiNm.selectedIndex]).mobile;
        _inspRequest.designUserId = ((FirePrincipalBean*)_dropAsiNm.customData[_dropAsiNm.selectedIndex]).userId;
        
    }];
    if (_inspResponse) {
        _labelDevId.text = _inspResponse.devId;
        _dropAsiNm.textLabel.text = _inspResponse.designUser.name;
        _fieldAsiPhone.text = _inspResponse.designUser.mobile;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];//设置输出的格式
        [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
        if (_inspResponse.startTime) {
             [_btnStartTime setTitle:[dateFormatter stringFromDate:_inspResponse.startTime] forState:UIControlStateNormal];
        }else{
            [_btnStartTime setTitle:@"无" forState:UIControlStateNormal];
        }
        if (_inspResponse.endTime) {
              [_btnEndTime setTitle:[dateFormatter stringFromDate:_inspResponse.endTime] forState:UIControlStateNormal];
        }else{
            [_btnEndTime setTitle:@"无" forState:UIControlStateNormal];
        }
        if (_inspResponse.inspPeriod) {
            _fieldInspCycle.text = [NSString stringWithFormat:@"%d",_inspResponse.inspPeriod.intValue];
        }
        
        
        
      
    }
    
    
    [self initDevinfo];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)initDevinfo{
    NSString *devId = nil;
    if (_devIdContentView.isHidden) {
        devId = _fieldDevId.text;
    }else{
        devId = _labelDevId.text;
    }
    if ([Util isNull:devId]) {
        NSLog(@"设备ID不能为空");
    }else{
        NSDictionary *param = [[NSDictionary alloc]initWithObjectsAndKeys:devId,@"devId", nil];
        [[NetWork network] queryDevInfo:param success:^(id data) {
            FireDevBaseInfoBean *devInfo = (FireDevBaseInfoBean *)data;
            dispatch_async(dispatch_get_main_queue(), ^{
                _labelProjNm.text = devInfo.projNm;
                _labelCurrentStat.text = devInfo.deviceStat;
            });
          
            
            
        } fail:^(NSString *message) {
            
        } error:^(NSError *error) {
            
        }];
    
    }
    
}



-(void)back{
    if (_keyboardIsVisible) {
        [self.view endEditing:YES];
    }else{
        [self dismissViewControllerAnimated:NO completion:nil];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)clickCancel:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (IBAction)clickGetData:(id)sender {
    [self initDevinfo];
    
    
}
-(void)handleMessage:(id)message{
    if ([[message name]isEqualToString:MESSAGE_SCAN_RESULT]) {
        self.view.hidden = NO;
        [self changeWindowTouchBack:YES];
        if([message object]){
            _fieldDevId.text = [message object];
        }
        

    }
}
- (IBAction)clickScan:(id)sender {
    ScanCodeViewController *controller = [[ScanCodeViewController alloc]init];
    
    [_lastVC.navigationController pushViewController:controller animated:YES];
    self.view.hidden = YES;
    [self changeWindowTouchBack:NO];
}



- (IBAction)clickAdd:(id)sender {
    [[NetWork network] saveInsp:self.inspRequest success:^(id data) {
     
        [self dismissViewControllerAnimated:NO completion:nil];
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
    
}

- (IBAction)clickDelete:(id)sender {
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:_inspResponse.inspPlanId,@"planId", nil];
    
    
    [[NetWork network] deleteInsp:param success:^(id data) {
         [self dismissViewControllerAnimated:NO completion:nil];
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
   
}

-(FireInspPlanRequest *)inspRequest{
    NSString *devId = nil;
    if (_devIdContentView.isHidden) {
        devId = _fieldDevId.text;
    }else{
        devId = _labelDevId.text;
    }
    _inspRequest.devId = devId;

    if (_inspResponse) {
        _inspRequest.id = _inspResponse.id;
        _inspRequest.inspPlanId = _inspResponse.inspPlanId;
    }
    
    NSString *period = _fieldInspCycle.text;
    if (![Util isNull:period]) {
        _inspRequest.inspPeriod = [NSNumber numberWithInt:period.intValue];
        _inspRequest.inspType = [NSNumber numberWithInt:0];
    
    }else{
        _inspRequest.inspType = [NSNumber numberWithInt:1];
        
        
    }
    _inspRequest.responUserId = [User user].userSelf.userId;
    _inspRequest.currentUserId = [User user].userSelf.userId;
    
    
    return _inspRequest;
}
-(void)viewDidUnload{
    [super viewDidUnload];
}
@end

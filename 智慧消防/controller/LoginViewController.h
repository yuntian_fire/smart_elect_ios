//
//  LoginViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetWork.h"
#import "智慧消防-Swift.h"
#import "智慧消防-Bridging-Header.h"
@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *fieldUserName;
@property (weak, nonatomic) IBOutlet UITextField *fieldPassword;

@end

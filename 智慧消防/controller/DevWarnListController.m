//
//  DevWarnListController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "DevWarnListController.h"
#import "UIColor+Utils.h"
#import "DevListCell.h"
@interface DevWarnListController ()
@property (strong,nonatomic) NSArray *xNames;
@end
static NSString *devListCell = @"DevListCell";
@implementation DevWarnListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initBarChartDesign];
    [self initBarChartData];

    UINib *nib = [UINib nibWithNibName:@"DevListCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:devListCell];
    self.tableView.rowHeight = 50;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
    
  
    

    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self refresh];
    
}

-(void)refresh{
    UserIdBody *userIdBody = [[UserIdBody alloc]init];
    userIdBody.userId = [User user].userSelf.userId;
    userIdBody.roleId = [User user].roleId;
    
    if (_vcSelf == VCTargetAlarmList) {
        
        [[NetWork network] getDevAlarmHist:userIdBody success:^(id data) {
            _alarmInfo = data;
            dispatch_queue_t main_queue = dispatch_get_main_queue();
            dispatch_async(main_queue, ^{
                [self.tableView reloadData];
            });
            
            
        } fail:^(NSString *message) {
            NSLog(@"%@",message);
        } error:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }else if(_vcSelf == VCTargetFaultList){
        [[NetWork network] getDevFaultHist:userIdBody success:^(id data) {
            _alarmInfo = data;
            dispatch_queue_t main_queue = dispatch_get_main_queue();
            dispatch_async(main_queue, ^{
                [self.tableView reloadData];
            });
            
            
        } fail:^(NSString *message) {
            NSLog(@"%@",message);
        } error:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

-(void)initBarChartDesign{
    //说明图标
    self.barChartView.legend.form = ChartLegendFormNone;
    //拖动手势
    self.barChartView.dragEnabled = NO;
    //捏合手势
    self.barChartView.pinchZoomEnabled = NO;
    //隐藏右Y轴
    self.barChartView.rightAxis.enabled = NO;
    //不显示描述label
    self.barChartView.chartDescription.enabled = NO;
    //禁止双击缩放
    self.barChartView.doubleTapToZoomEnabled = NO;
    
    self.barChartView.drawGridBackgroundEnabled = NO;
    self.barChartView.drawBordersEnabled = NO;
    //拖动气泡
    self.barChartView.dragEnabled = NO;
    
    self.barChartView.drawValueAboveBarEnabled = NO;
    
}

//为柱形图设置数据
- (void)initBarChartData{
    _xNames = @[@"2:00",@"4:00",@"6:00",@"8:00",@"10:00",@"12:00",@"14:00",@"16:00",@"18:00",@"20:00",@"22:00",@"24:00"];
    
    ChartXAxis *xAxis = self.barChartView.xAxis;
    xAxis.valueFormatter = self;
    xAxis.axisLineWidth = 1;//设置X轴线宽
    xAxis.labelPosition = XAxisLabelPositionBottom;//X轴的显示位置，默认是显示在上面的
    xAxis.drawGridLinesEnabled = NO;//不绘制网格线
    xAxis.forceLabelsEnabled = YES;
    xAxis.labelCount = 12;
    xAxis.labelTextColor = [UIColor darkGrayColor];//label文字颜色
    xAxis.labelFont = [UIFont systemFontOfSize:10];
    
    
    ChartYAxis *leftAxis = self.barChartView.leftAxis;//获取左边Y轴
    leftAxis.forceLabelsEnabled = NO;//不强制绘制制定数量的label
    leftAxis.inverted = NO;//是否将Y轴进行上下翻转
    leftAxis.axisLineWidth = 0;//Y轴线宽
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.forceLabelsEnabled = YES;
    leftAxis.axisMinimum = 0;
   
    int maxValue = 0;
//    ChartLimitLine *limitLine = [[ChartLimitLine alloc] initWithLimit:80 label:@"限制线"];
//    limitLine.lineWidth = 2;
//    limitLine.lineColor = [UIColor greenColor];
//    limitLine.lineDashLengths = @[@5.0f, @5.0f];//虚线样式
//    limitLine.labelPosition = ChartLimitLabelPositionRightTop;//位置
//    [leftAxis addLimitLine:limitLine];//添加到Y轴上
//    leftAxis.drawLimitLinesBehindDataEnabled = YES;
    
//    int xVals_count = 12;//X轴上要显示多少条数据
    
    
    //对应Y轴上面需要显示的数据
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for (int i = 0; i < _alarmInfo.todAlarmCount.count; i++) {
        int value = _alarmInfo.todAlarmCount[i].intValue;
        if (value > maxValue) {
            maxValue = value;
        }
     
        BarChartDataEntry *entry = [[BarChartDataEntry alloc] initWithX:i y:value];
        [yVals addObject:entry];
        
    }
    
    
    //对应Y轴上面需要显示的数据
    NSMutableArray *yValsTwo = [[NSMutableArray alloc] init];
    for (int i = 0; i < _alarmInfo.yesAlarmCount.count; i++) {
        int value = _alarmInfo.yesAlarmCount[i].intValue;
        if (value>maxValue) {
            maxValue = value;
        }
     
        BarChartDataEntry *entry = [[BarChartDataEntry alloc] initWithX:i y:value];
        
        [yValsTwo addObject:entry];
    }
    
    leftAxis.axisMaximum = maxValue;
    
    //创建BarChartDataSet对象，其中包含有Y轴数据信息，以及可以设置柱形样式
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals label:nil];
    set1.barBorderWidth =0;//边学宽
    set1.drawValuesEnabled = NO;//是否在柱形图上面显示数值
    set1.highlightEnabled = YES;//点击选中柱形图是否有高亮效果，（双击空白处取消选中）
    [set1 setColors:@[[UIColor colorWithHexRGB:0x3795ED]]];//设置柱形图颜色
    //    [set1 setColors:ChartColorTemplates.material];
    
    //创建BarChartDataSet对象，其中包含有Y轴数据信息，f以及可以设置柱形样式
    BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithValues:yValsTwo label:nil];
    
    set2.barBorderWidth =0;//边学宽
    set2.drawValuesEnabled = NO;//是否在柱形图上面显示数值
    set2.highlightEnabled = YES;//点击选中柱形图是否有高亮效果，（双击空白处取消选中）
    [set2 setColors:@[[UIColor colorWithHexRGB:0x69CA72]]];
    //将BarChartDataSet对象放入数组中
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    [dataSets addObject:set2];
    _barData = dataSets;
    
    [self changeBarDara];
   
   
    

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if(_target == VCTargetDevInfo){
        DevInfoViewController *controller = (DevInfoViewController *)[segue destinationViewController];
        controller.devInfoBean = _alarmInfo.alarmHistList[_rowIndex];
    }
    
}


- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis {
    
    return self.xNames[((int)value)%self.xNames.count];
}



- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    DevListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:devListCell];
    FireDevInfoBean *alarmDev = self.alarmInfo.alarmHistList[indexPath.row];
    cell.labelDevId.text = alarmDev.devId;
    cell.labelLocation.text = alarmDev.location;
    cell.labelTime.text = [Util dateStrFromDate:alarmDev.alarmTime];
    switch (alarmDev.devStatus.intValue) {
        case 0:{
            cell.labelDevStat.text = @"正常";
            cell.imgDevStat.image = [UIImage imageNamed:@"dtu_normal"];
            cell.imgPoint.image = [UIImage imageNamed:@"point_normal"];
            cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
            
        }
            
            break;
            
        case 1:{
            cell.labelDevStat.text = @"离线";
            cell.imgDevStat.image = [UIImage imageNamed:@"dtu_offline"];
            cell.imgPoint.image = [UIImage imageNamed:@"point_offline"];
            cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_offline"];
        }
            
            break;
        case 2:{
            cell.labelDevStat.text = @"报警";
            cell.imgDevStat.image = [UIImage imageNamed:@"dtu_warn"];
            cell.imgPoint.image = [UIImage imageNamed:@"point_alarm"];
            cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
        }
            
            break;
            
        case 3:{
            cell.labelDevStat.text = @"故障";
            cell.imgDevStat.image = [UIImage imageNamed:@"dtu_fault"];
            cell.imgPoint.image = [UIImage imageNamed:@"point_fault"];
            cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
        }
            
            
            break;
        default:
            break;
    }
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.alarmInfo.alarmHistList?_alarmInfo.alarmHistList.count:0;
}


- (IBAction)clickNow:(id)sender {
    if (!(_btnNow.selected&&!_btnLast.selected)) {
         _btnNow.selected = !_btnNow.selected;
        [self changeBarDara];
        
    }
    
   
    
}

-(void)changeBarDara{
     NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if (_barData) {
        if (_btnNow.isSelected) {
            [array addObject:_barData[0]];
        }
        
        if (_btnLast.isSelected) {
            [array addObject:_barData[1]];
        }
      
        
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:array];
        //设置宽度   柱形之间的间隙占整个柱形(柱形+间隙)的比例
        [data setBarWidth:0.34];
        [data groupBarsFromX: -0.5 groupSpace: 0.32 barSpace: 0];
        self.barChartView.data = data;
    }

}

- (IBAction)clickLast:(id)sender {
    if (!(_btnLast.selected&&!_btnNow.selected)) {
        _btnLast.selected = !_btnLast.selected;
        [self changeBarDara];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _target = VCTargetDevInfo;
    _rowIndex = indexPath.row;
    [self performSegueWithIdentifier:@"warn_list_to_dev_info" sender:self];
}


@end

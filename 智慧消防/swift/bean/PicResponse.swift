//
//  PicResponse.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class PicResponse: BaseBean {
    var projId:String?
    var picTime :NSNumber?
    
    override public func mapping(map: Map) {
        projId <- map["projId"]
        picTime <- map["picTime"]
    }
    
}

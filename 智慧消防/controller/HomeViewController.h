//
//  HomeViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/11/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeDevCell.h"
#import "NetWork.h"
#import "DevWarnListController.h"
#import "DevInfoViewController.h"
@interface HomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *devTableView;
@property (strong,nonatomic) NSMutableArray<FireDevInfoBean*>*probDevArray;
@property (nonatomic)VCTarget target;
@property (nonatomic)NSInteger rowIndex;
@end

//
//  DevWarnListController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetWork.h"
#import "智慧消防-Bridging-Header.h"
#import "Util.h"
#import "DevInfoViewController.h"
@interface DevWarnListController : UIViewController<IChartAxisValueFormatter,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet BarChartView *barChartView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnNow;
@property (weak, nonatomic) IBOutlet UIButton *btnLast;
@property (nonatomic)VCTarget vcSelf;
@property (strong,nonatomic) FireDevAlarmHistBean* alarmInfo;
@property (nonatomic)VCTarget target;
@property (nonatomic)NSInteger rowIndex;
@property (strong ,nonatomic) NSArray *barData;
@end

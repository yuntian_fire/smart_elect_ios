//
//  Util.m
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "Util.h"

@implementation Util
+(NSString *)dateStrFromDate:(NSDate*)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = DATE_FORMAT;
    return [formatter stringFromDate:date];
}
+(BOOL)isNull:(NSString*)str{

    
    return str == NULL||[@"" isEqualToString:str];
}

+(NSDateComponents*)getDateComponentFromDate:(NSDate*)date{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitYear | //年
    NSCalendarUnitMonth | //月份
    NSCalendarUnitDay | //日
    NSCalendarUnitHour |  //小时
    NSCalendarUnitMinute |  //分钟
    NSCalendarUnitSecond;  // 秒
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:date];
    return dateComponent;
    
    
}
@end

//
//  ProjViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/11/28.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "ProjViewController.h"

#import "ProjInfoViewController.h"
#import "User.h"
#import "DevBasicPage.h"

@interface ProjViewController ()

@end

@implementation ProjViewController{
    dispatch_block_t _refreshBlock;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self refresh];
    
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
//    _searchView.isOpen = YES;
    
    
    ProjMapView *projMapView = (ProjMapView*)_pageControl.secondView;
    projMapView.delegate = projMapView;
    [projMapView viewWillAppear];
    projMapView.selfVC = self;
  
  
  
    [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_MAIN_HANDLE object:@"proj"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessage:) name:MESSAGE_NAV_CONTROL object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleMessage:) name:MESSAGE_PAGE object:nil];
    
//    [[self pageControl]initChild];
    [self addRefreshBlock];
    
}

-(void)refresh{
    ProjListView *projListView = (ProjListView*)self.pageControl.firstView;
    ProjMapView *projMapView = (ProjMapView*)_pageControl.secondView;
    UserIdBody *userIdBody = [[UserIdBody alloc]init];
    userIdBody.userId = [User user].userSelf.userId;
    userIdBody.roleId = [User user].roleId;
    
    [[NetWork network] getProjsWithUserIdBody:userIdBody success:^(id data) {
        dispatch_queue_t main_queue = dispatch_get_main_queue();
        dispatch_async(main_queue, ^{
            [User user].projArray = self.projArray = projListView.projArray = projMapView.projArray =data;
        });
        
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

-(void)addRefreshBlock{
    dispatch_queue_t queue =
    dispatch_queue_create("devRefreshBlock", DISPATCH_QUEUE_SERIAL);
    _refreshBlock = dispatch_block_create(DISPATCH_BLOCK_ASSIGN_CURRENT, ^{
        [self refresh];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), queue, _refreshBlock);
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)),queue, _refreshBlock);
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}
-(void)setProjArray:(NSArray<FireProjBean *> *)projArray{
    _projArray = projArray;
    _labelProjCount.text = [NSString stringWithFormat:@"%ld",projArray.count];
  
    int normalNum = 0;
    int offlineNum = 0;
    int warmNum = 0;
    int faultNum = 0;
    for (int i = 0; i < projArray.count; i++) {
        FireProjBean *fireProjBean = projArray[i];
        normalNum += fireProjBean.nomal.intValue;
        offlineNum += fireProjBean.offline.intValue;
        warmNum += fireProjBean.alarm.intValue;
        faultNum += fireProjBean.fault.intValue;
    }
    int devSum = normalNum + offlineNum + warmNum + faultNum;
    _labelDevCount.text = [NSString stringWithFormat:@"%d",devSum];
    _labelNormalCount.text = [NSString stringWithFormat:@"%d",normalNum];
    _labelWarnCount.text = [NSString stringWithFormat:@"%d",warmNum];
    _labelFaultCount.text = [NSString stringWithFormat:@"%d",faultNum];
    _labelOfflineCount.text = [NSString stringWithFormat:@"%d",faultNum];
}


-(void)handleMessage:(id)message{
    if ([message isKindOfClass:NSClassFromString(@"NSConcreteNotification")]) {
        if([[message name] isEqualToString:MESSAGE_NAV_CONTROL]){
            if([[message object] intValue]>=0){
                [self.pageControl setCurrentPage:[[message object] intValue]];
            }else if([[message object] intValue] == -1){
                _searchView.isOpen = !_searchView.isOpen;
            }
            
        }else if([[message name] isEqualToString:MESSAGE_PAGE]){
            
            NSDictionary *info = [message userInfo];
            NSString * to = [info objectForKey:@"to"];
            NSNumber *rowIndex = [info objectForKey:@"rowIndex"];
            self.rowIndex = rowIndex.integerValue;
            if ([@"proj_info" isEqualToString:to]) {
                
                
                
                _target = VCTargetProjInfo;
                [self performSegueWithIdentifier:@"proj_to_proj_info" sender:self];
                
            }else if([@"dev_add" isEqualToString:to]){
                if (![ROLE_PROJ_PRIN isEqualToString:[User user].roleId]&&![ROLE_MAINT_USER isEqualToString:[User user].roleId]){
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员和维保人员可以添加设备" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView show];
                    return;
                }
                
                
                _target = VCTargetDevInfo;
                [self performSegueWithIdentifier:@"proj_to_dev_info" sender:self];
              
            }
            
            
            
        }
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
     [((ProjMapView*)(_pageControl.secondView)) viewWillDisappear];
    ((ProjMapView*)(_pageControl.secondView)).delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    dispatch_block_cancel(_refreshBlock);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if (_target == VCTargetProjInfo) {
        ProjInfoViewController *projInfoVC = [segue destinationViewController];
        projInfoVC.projBean = self.projArray[_rowIndex];
    }else if(_target == VCTargetDevInfo){
        DevInfoViewController *controller = [segue destinationViewController];
        controller.devBasic = [[FireDevBasicInfo alloc]init];
        controller.devBasic.projId = _projArray[_rowIndex].projId;
        controller.devBasic.projNm = _projArray[_rowIndex].projNm;
        
    }
    
    _target = -1;
    
 
    
}


@end

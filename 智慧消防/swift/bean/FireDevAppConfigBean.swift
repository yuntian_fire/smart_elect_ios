//
//  FireDevAppConfigBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevAppConfigBean: BaseBean {
    var devId:String?
    var heartTime:NSNumber?
    var regisTime:Date?
    var mobile:String?
    var onlineStat:NSNumber?
    var devMode:String?
    
    var testNameDisplay:NSMapTable<String,String>?
    var installPosDisplay:NSMapTable<String,String>?
    var eleclRatioDisplay:NSMapTable<String,String>?
    var expSwitchDisplay:NSMapTable<String,String>?
    var expPulseDisplay:NSMapTable<String,String>?
    
    var eleclRatioJudge:Array<NSNumber>?
    var configSenseList:Array<ConfigSense>?
    var configExtList:Array<ConfigExt>?
    override public func mapping(map: Map) {
        devId <- map["devId"]
        heartTime <- map["heartTime"]
        regisTime <- (map["regisTime"],DateTransform())
        mobile <- map["mobile"]
        onlineStat <- map["onlineStat"]
        devMode <- map["devMode"]
        testNameDisplay <- map["testNameDisplay"]
        installPosDisplay <- map["installPosDisplay"]
        eleclRatioDisplay <- map["eleclRatioDisplay"]
        expSwitchDisplay <- map["expSwitchDisplay"]
        expPulseDisplay <- map["expPulseDisplay"]
        eleclRatioJudge <- map["eleclRatioJudge"]
        configSenseList <- map["configSenseList"]
        configExtList <- map["configExtList"]
        
    }
    
}

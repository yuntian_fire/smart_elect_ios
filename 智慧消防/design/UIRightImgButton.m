//
//  UIRightImgButton.m
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "UIRightImgButton.h"

@implementation UIRightImgButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (CGRect)titleRectForContentRect:(CGRect) contentRect{
    CGRect titleFrame = [super titleRectForContentRect:contentRect];
    titleFrame.origin.x = contentRect.origin.x + 10;
    
    
    return titleFrame;
    
    
}

- (CGRect)imageRectForContentRect:(CGRect) contentRect{
    CGRect imageRect = [super imageRectForContentRect:contentRect];
    imageRect.origin.x = contentRect.size.width - 10 - imageRect.size.width;
    
    return imageRect;
    
}
@end

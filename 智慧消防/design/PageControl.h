//
//  ProjPageControl.h
//  智慧消防
//
//  Created by yuntian on 2018/11/29.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageControl : UIView
@property (strong, nonatomic) IBOutlet UIView *firstView;
@property (strong, nonatomic) IBOutlet UIView *secondView;
@property (strong, nonatomic) IBOutlet UIView *thirdView;
@property (nonatomic) NSInteger currentPage;
@property (strong,nonatomic) IBInspectable NSString *firstNib;
@property (strong,nonatomic) IBInspectable NSString *secondNib;
@property (strong,nonatomic) IBInspectable NSString *thirdNib;

@property (nonatomic) IBInspectable NSInteger pageCount;
-(void)initChild;
@end

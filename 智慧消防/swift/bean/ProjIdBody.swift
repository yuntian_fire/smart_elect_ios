//
//  projIdBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/11.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class ProjIdBody: BaseBean {
    var projId:String?
    
    override public func mapping(map: Map) {
        projId <- map["projId"]
    }
    

}

//
//  FireDevMaintHandlerRequest.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevMaintHandlerRequest: BaseBean {
    var devId:String?
    var maintId:String?
    var maintType:NSNumber?
    var process:String?
    var comment:String?
    
    override public func mapping(map: Map) {
        devId <- map["devId"]
        maintId <- map["maintId"]
        maintType <- map["maintType"]
        process <- map["process"]
        comment <- map["comment"]
        
    }
    
}

//
//  MaintHeadCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EBDropdownListView.h"
@interface MaintHeadCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelHeadTitle;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropMaintHead;
@property (weak, nonatomic) IBOutlet UITextField *fieldMaintHeadPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnRemove;

@end

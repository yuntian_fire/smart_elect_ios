//
//  ProjInfoViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/4.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageControl.h"
#import "NetWork.h"
#import "DevDetailPage.h"
#import "ProjBasicPage.h"
#import "DevInfoViewController.h"

@interface ProjInfoViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet PageControl *pageControl;
@property(nonatomic)NSInteger rowIndex;
@property(strong,nonatomic)FireProjBean *projBean;
@property(nonatomic)VCTarget target;
@property (strong, nonatomic) IBOutlet UIView *navRightItem;
@property (weak, nonatomic) IBOutlet UIButton *btnSet;
-(void)choosePhoto;
@end

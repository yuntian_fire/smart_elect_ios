//
//  FireDevMonitorBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevMonitorBean: BaseBean {
    var devId:String?
    var srcAddr:String?
    var dtuEventList:Array<FireDtuEventBean>?
    var senseList:Array<FireDevSenseStatBean>?
    var alarmList:Array<FireDevAlarmBean>?
    var senseShield:Array<FireDevShieldBean>?
    var extShield:Array<FireDevShieldBean>?
    var userType:NSNumber?
    var alarmExecStat:NSNumber?
    var maintId:String?
    
    override public func mapping(map: Map) {
        devId <- map["devId"]
        srcAddr <- map["srcAddr"]
        dtuEventList <- map["dtuEventList"]
        alarmList <- map["alarmList"]
        senseShield <- map["senseShield"]
        extShield <- map["extShield"]
        userType <- map["userType"]
        alarmExecStat <- map["alarmExecStat"]
        maintId <- map["maintId"]
    }

}

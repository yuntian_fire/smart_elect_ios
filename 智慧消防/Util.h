//
//  Util.h
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Const.h"
@interface Util : NSObject
+(NSString *)dateStrFromDate:(NSDate*)date;

+(BOOL)isNull:(NSString*)str;
+(NSDateComponents*)getDateComponentFromDate:(NSDate*)date;
@end

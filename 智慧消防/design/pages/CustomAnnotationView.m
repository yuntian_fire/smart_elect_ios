//
//  CustomAnnotationView.m
//  智慧消防
//
//  Created by yuntian on 2018/12/25.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "CustomAnnotationView.h"
@implementation CustomAnnotationView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.label];
    }
    
    return self;
}

-(UILabel *)label{
    if (!_label) {
        _label = [[UILabel alloc]init];
        _label.backgroundColor = [UIColor whiteColor];
        _label.textColor = [UIColor blackColor];
        _label.font = [UIFont systemFontOfSize:12];
        
    }
    return  _label;
}

-(void)layoutSubviews{
    [super layoutSubviews];
  
   
}



@end

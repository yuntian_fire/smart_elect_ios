//
//  DevListCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DevListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDevId;
@property (weak, nonatomic) IBOutlet UILabel *labelLocation;
@property (weak, nonatomic) IBOutlet UILabel *labelDevStat;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgDevStat;
@property (weak, nonatomic) IBOutlet UIImageView *imgPoint;
@property (weak, nonatomic) IBOutlet UIImageView *imgOnlineStat;

@end

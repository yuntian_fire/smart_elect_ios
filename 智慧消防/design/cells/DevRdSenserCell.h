//
//  DevRdSenserCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/19.
//  Copyright © 2018年 yuntian. All rights reserved.

//

#import <UIKit/UIKit.h>
#import "智慧消防-Swift.h"
#import "EBDropdownListView.h"
@interface DevRdSenserCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelChannel;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropSensor;

@property (weak, nonatomic) IBOutlet UILabel *labelCurrentData;
@property (weak, nonatomic) IBOutlet UITextField *fieldRD;
@property (weak, nonatomic) IBOutlet UITextField *fieldAlarmThre;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropMl;
@property (weak, nonatomic) IBOutlet UILabel *labelUnit;
@property(strong,nonatomic)ConfigSense* configSense;
-(void)initSensors;
@end

//
//  MaintListViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "MaintListViewController.h"
#import "DevListCell.h"
#import "DevInfoViewController.h"
@interface MaintListViewController ()

@end

@implementation MaintListViewController
static NSString *devListCell = @"DevListCell";
- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *nib = [UINib nibWithNibName:@"DevListCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:devListCell];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.rowHeight = 50;
    // Do any additional setup after loading the view.
    
    self.maintArray = [[NSMutableArray alloc]init];
    
   
    
    
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UserIdBean *userIdBean = [[UserIdBean alloc]init];
    userIdBean.currentUserId = [User user].userSelf.userId;
    userIdBean.currentRoleId = [User user].roleId;
    [self.maintArray removeAllObjects];
    [[NetWork network]getMaintList:userIdBean success:^(id data) {
        NSArray<FireMaintResponse*> * maintData = data;
        for (FireMaintResponse *maintResp in maintData) {
            if (self.vcSelf == VCTargetMaintTask) {
                if([Util isNull:maintResp.endTime]){
                    
                    [self.maintArray addObject:maintResp];
                }
            }else if(self.vcSelf == VCTargetMaintRecord){
                if(![Util isNull:maintResp.endTime]){
              
                    [self.maintArray addObject:maintResp];
                }
            }
        }
        dispatch_queue_t main_queue = dispatch_get_main_queue();
        dispatch_async(main_queue, ^{
            [self.tableView reloadData];
        });
        
        
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    DevInfoViewController *controller = (DevInfoViewController *)[segue destinationViewController];
    FireDevInfoBean *devInfoBean = [[FireDevInfoBean alloc]init];
    devInfoBean.devId = _maintArray[_rowIndex].devId;
    controller.devInfoBean = devInfoBean;
    
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    DevListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:devListCell];
    FireMaintResponse *maintResp = self.maintArray[indexPath.row];
    cell.labelDevId.text = maintResp.devId;
    cell.labelTime.text = maintResp.createTime;
    cell.labelDevStat.text = maintResp.devStat;
    cell.labelLocation.text = maintResp.location;
    if([maintResp.devStat isEqualToString:@"正常"]){
        cell.labelDevStat.text = @"正常";
        cell.imgDevStat.image = [UIImage imageNamed:@"dtu_normal"];
        cell.imgPoint.image = [UIImage imageNamed:@"point_normal"];
        cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
    }else if([maintResp.devStat isEqualToString:@"掉线"]){
        cell.labelDevStat.text = @"离线";
        cell.imgDevStat.image = [UIImage imageNamed:@"dtu_offline"];
        cell.imgPoint.image = [UIImage imageNamed:@"point_offline"];
        cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_offline"];
    }else if([maintResp.devStat isEqualToString:@"报警"]){
        cell.labelDevStat.text = @"报警";
        cell.imgDevStat.image = [UIImage imageNamed:@"dtu_warn"];
        cell.imgPoint.image = [UIImage imageNamed:@"point_alarm"];
        cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
    }else if([maintResp.devStat isEqualToString:@"故障"]){
        cell.labelDevStat.text = @"故障";
        cell.imgDevStat.image = [UIImage imageNamed:@"dtu_fault"];
        cell.imgPoint.image = [UIImage imageNamed:@"point_fault"];
        cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
    }
  
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _maintArray?_maintArray.count:0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _rowIndex = indexPath.row;
    [self performSegueWithIdentifier:@"maint_list_to_dev_info" sender:self];
    
}

@end

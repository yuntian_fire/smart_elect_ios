//
//  InspSelectVc.h
//  智慧消防
//
//  Created by yuntian on 2018/12/26.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InspSelectVc : UIViewController
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic) void(^clickAll)(void);
@property (nonatomic) void(^clickRandom)(void);
@property (nonatomic) void(^clickSys)(void);
@end

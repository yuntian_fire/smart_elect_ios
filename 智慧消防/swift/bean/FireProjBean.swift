//
//  FireProjBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireProjBean: BaseBean {
    var projId:String?
    var projNm:String?
    var prov:String?
    var city:String?
    var county:String?
    var lng:String?
    var lat:String?
    var picPath:String?
    var projType:NSNumber?
    var projAddr:String?
    var projRemark:String?
    var totalCount:NSNumber?
    var offline:NSNumber?
    var nomal:NSNumber?
    var fault:NSNumber?
    var alarm:NSNumber?
    var userId:String?
    var primPrincInfo:FirePrincipalBean?
    var optPrincInfoList:[FirePrincipalBean]?
    var picTime:NSNumber?
    override public func mapping(map: Map) {
        projId <- map["projId"]
        projNm <- map["projNm"]
        prov <- map["prov"]
        city <- map["city"]
        county <- map["county"]
        lng <- map["lng"]
        lat <- map["lat"]
        picPath <- map["picPath"]
        projType <- map["projType"]
        projAddr <- map["projAddr"]
        projRemark <- map["projRemark"]
        totalCount <- map["totalCount"]
        offline <- map["offline"]
        nomal <- map["nomal"]
        fault <- map["fault"]
        alarm <- map["alarm"]
        userId <- map["userId"]
        primPrincInfo <- map["primPrincInfo"]
        optPrincInfoList <- map["optPrincInfoList"]
        picTime <- map["picTime"]
        
    }
    
    
}

//
//  UIColor+Utils.h
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

struct RGBA {
    UInt8 r;
    UInt8 g;
    UInt8 b;
    UInt8 a;
};

@interface UIColor (Utils)
//---------------------------------------------------------------------------
#pragma mark macros for generating colors
//---------------------------------------------------------------------------
#define RGBInt(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBAInt(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)/255.0]

#define RGBHex(rgb) [UIColor colorWithHexRGB:rgb]
#define RGBAHex(rgba) [UIColor colorWithHexRGBA:rgba]
#define ARGBHex(argb) [UIColor colorWithHexARGB:argb]

//---------------------------------------------------------------------------
#pragma mark methods for generating colors
//---------------------------------------------------------------------------

+ (UIColor *)colorWithHexRGB:(NSUInteger)hexRGB;
+ (UIColor *)colorWithHexRGBA:(NSUInteger)hexRGBA;
+ (UIColor *)colorWithHexARGB:(NSUInteger)hexARGB;

//---------------------------------------------------------------------------
#pragma mark methods for color converting
//---------------------------------------------------------------------------

- (NSString *)toHexRGB;
- (NSString *)toHexRGBA;
- (NSString *)toHexARGB;

- (struct RGBA)toRGBA;
@end

//
//  PagerPopViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/20.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "PagerPopViewController.h"
#import "DevShieldTableViewController.h"
@interface PagerPopViewController ()

@end

@implementation PagerPopViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    [self initSenserData];
    self.delegate = self;
    self.dataSource = self;
    
    
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma View Pager Data Source
-(NSInteger)numberOfViewControllersInViewPager:(LSYViewPagerVC *)viewPager
{
    return _sensorTitlesList?_sensorTitlesList.count:0;
}
-(UIViewController *)viewPager:(LSYViewPagerVC *)viewPager indexOfViewControllers:(NSInteger)index
{
    DevShieldTableViewController *vc = [[DevShieldTableViewController alloc]init];
    vc.dataSource = _detList[index];
    
    
    return vc;
}
-(UIView *)headerViewForInViewPager:(LSYViewPagerVC *)viewPager
{
    return nil;
}
-(CGFloat)heightForHeaderOfViewPager:(LSYViewPagerVC *)viewPager
{
    return 0;
}
-(CGFloat)heightForTitleOfViewPager:(LSYViewPagerVC *)viewPager
{
    return 50;
}
-(NSString *)viewPager:(LSYViewPagerVC *)viewPager titleWithIndexOfViewControllers:(NSInteger)index
{
    
    return _sensorTitlesList[index];
}


#pragma View Pager Delegate
-(void)viewPagerViewController:(LSYViewPagerVC *)viewPager willScrollerWithCurrentViewController:(UIViewController *)ViewController
{
    
}
-(void)viewPagerViewController:(LSYViewPagerVC *)viewPager didFinishScrollWithCurrentViewController:(UIViewController *)viewController
{
    
}

-(void)initSenserData{
    NSMutableArray<NSMutableArray<FireDevShieldBean*>*>* detList = [[NSMutableArray alloc]init];
    NSMutableArray<FireDevShieldBean*>* list = [[NSMutableArray alloc]init];
    NSMutableArray<NSString*> *sensorTitlesList = [[NSMutableArray alloc]init];
    int index = 0;
    NSArray<FireDevShieldBean*> *configSenseList = self.cmd.shieldBeanList;
    if (configSenseList) {
        
        for (int i = 0; i < configSenseList.count - 1; i++) {
            if (configSenseList[i].devType) {
                NSInteger circle = configSenseList[i].circle.integerValue;
                NSInteger partAddr = configSenseList[i].partAddr.integerValue;
                NSInteger circle1 = configSenseList[i+1].circle.integerValue;
                NSInteger partAddr1 = configSenseList[i+1].partAddr.integerValue;
                
                
                
                if ([[NSString stringWithFormat:@"%ld%ld",circle,partAddr]isEqualToString:[NSString stringWithFormat:@"%ld%ld",circle1,partAddr1]]) {
                    
                    [list addObject:configSenseList[i]];
                    
                } else {
                    [list addObject:configSenseList[i]];
                    [detList addObject:list];
                    if (configSenseList[i].devType.intValue == 0) {
                        [sensorTitlesList addObject:@"DTU"];
                    } else {
                        index++;
                        
                        [sensorTitlesList addObject:[NSString stringWithFormat:@"探测器%d",index]];
                    }
                    list = [[NSMutableArray alloc]init];
                }
                if (i == configSenseList.count - 2) {
                    
                    [list addObject:configSenseList[i+1]];
                    [detList addObject:list];
                    if (configSenseList[i+1].devType.intValue == 0) {
                        [sensorTitlesList addObject:@"DTU"];
                    } else {
                        index++;
                        [sensorTitlesList addObject:[NSString stringWithFormat:@"探测器%d",index]];
                    }
                }
            }
        }
        
        
     
        _detList = detList;
        _sensorTitlesList = sensorTitlesList;
    }
 
    
}

@end

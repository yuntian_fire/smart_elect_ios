//
//  ProjViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/11/28.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageControl.h"
#import "ProjListView.h"
#import "ProjMapView.h"
#import "NetWork.h"
#import "Const.h"
#import "SearchView.h"
@interface ProjViewController : UIViewController
@property (weak, nonatomic) IBOutlet PageControl *pageControl;
@property (weak,nonatomic) NSArray<FireProjBean *> *projArray;
@property (weak, nonatomic) IBOutlet UILabel *labelNormalCount;
@property (nonatomic)VCTarget target;
@property (weak, nonatomic) IBOutlet UILabel *labelProjCount;
@property (weak, nonatomic) IBOutlet UILabel *labelDevCount;
@property (weak, nonatomic) IBOutlet UILabel *labelWarnCount;
@property (weak, nonatomic) IBOutlet UILabel *labelFaultCount;
@property (weak, nonatomic) IBOutlet UILabel *labelOfflineCount;
@property (nonatomic) NSInteger rowIndex;
@property (weak, nonatomic) IBOutlet SearchView *searchView;

@end

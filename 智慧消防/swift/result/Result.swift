//
//  Result.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper

class Result: BaseBean {
    var state:NSNumber?
    var message:String?
    override public func mapping(map: Map) {
        state <- map["state"]
        message <- map["message"]
        
       
    }
    
  
   
    
}

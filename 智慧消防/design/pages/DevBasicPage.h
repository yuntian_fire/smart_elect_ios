//
//  DevBasicPage.h
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EBDropdownListView.h"
#import "NetWork.h"
@class DevInfoViewController;
@interface DevBasicPage : UIView<UITableViewDataSource,UITableViewDelegate>{
    FireDevBasicInfo *_devBasicInfo;
}

@property (weak, nonatomic) IBOutlet UITextField *fieldLat;
@property (weak, nonatomic) IBOutlet UITextField *fieldLon;
@property (weak, nonatomic) IBOutlet UITextField *fieldLocation;
@property (weak, nonatomic) IBOutlet UITextField *fieldDevId;
@property (weak, nonatomic) IBOutlet UITextField *fieldProjHeadPhone;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropProj;
@property (weak, nonatomic) IBOutlet UITextField *fieldProjHead;
@property (weak, nonatomic) IBOutlet UITextView *tvRemark;
@property (weak, nonatomic) IBOutlet UITextField *fieldEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgTop;
@property (nonatomic)BOOL isSet;
@property (strong,nonatomic)DevInfoViewController *selfVC;
-(void)setDevBasicInfo:(FireDevBasicInfo *)devBasicInfo;
-(FireDevBasicInfo *)devBasicInfo;
-(void)setImage:(UIImage *)image;
-(void)refreshLocation;
@end

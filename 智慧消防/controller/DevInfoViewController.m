//
//  DevInfoViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/4.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "DevInfoViewController.h"
#import "MessagePushVc.h"
#import "ChoosePosViewController.h"

@interface DevInfoViewController (){
    UIImage *_image;
    BOOL _isRefreshPosition;
}

@end

@implementation DevInfoViewController{
    dispatch_block_t _timeBlock;
    dispatch_block_t _refreshBlock;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSBundle mainBundle]loadNibNamed:@"DevInfoRightItem" owner:self options:nil];
    [self changeSetBtnStatus];
    
    
    
    DevBasicPage *basicPage = (DevBasicPage *)_pageControl.firstView;
    DevMonitorPage *monitorPage = (DevMonitorPage*)_pageControl.thirdView;
    DevRDPage *rdPage = (DevRDPage*)_pageControl.secondView;
    monitorPage.selfVC = self;
    basicPage.selfVC = self;
    rdPage.selfVC = self;
    basicPage.isSet = NO;
    rdPage.isSet = NO;
    if (_devInfoBean) {
        [self refreshBasic];
        [self refreshRd:TYPE_REFRESH];
        [self refreshMonitor];
    }else if(_devBasic)
    {
        basicPage.devBasicInfo = _devBasic;
    }
  
    

    // Do any additional setup after loading the view.
}



-(void)refreshLatAndLng{
    DevBasicPage *basicPage = (DevBasicPage *)_pageControl.firstView;
    [basicPage refreshLocation];;
}

-(void)refreshRd:(NSString *)refreshType{
    DevRDPage *rdPage = (DevRDPage *)_pageControl.secondView;
    DevIdBody *devIdBody = [[DevIdBody alloc]init];
    devIdBody.devId = _devInfoBean.devId;
    devIdBody.userId = [User user].userSelf.userId;
    devIdBody.type = refreshType;
    [[NetWork network] getDevConfig:devIdBody success:^(id data) {
        if (!rdPage.isSet) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                rdPage.devConfigBean = data;
                
                
            });
        }
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)refreshMonitor{
    DevMonitorPage *monitorPage = (DevMonitorPage*)_pageControl.thirdView;
    DevIdBody *devIdBody = [[DevIdBody alloc]init];
    devIdBody.devId = _devInfoBean.devId;
    devIdBody.userId = [User user].userSelf.userId;
    devIdBody.type = TYPE_REFRESH;
    [[NetWork network] getDevMonitor:devIdBody success:^(id data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            monitorPage.monitorBean = data;
        });
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)refreshBasic{
    DevBasicPage *basicPage = (DevBasicPage *)_pageControl.firstView;
    DevIdBody *devIdBody = [[DevIdBody alloc]init];
    devIdBody.devId = _devInfoBean.devId;
    devIdBody.userId = [User user].userSelf.userId;
    NSLog(@"devId = %@",_devInfoBean.devId);
    
    [[NetWork network] getDevBasicInfo:devIdBody success:^(id data) {
        dispatch_queue_t main_queue = dispatch_get_main_queue();
        if (!basicPage.isSet) {
            dispatch_async(main_queue, ^{
                basicPage.devBasicInfo = data;
            });
        }
       
        
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_isRefreshPosition) {
        [self refreshLatAndLng];
        _isRefreshPosition = NO;
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessage:) name:MESSAGE_PAGE object:nil];
    
    [self addTimeBlock];
    [self addRefreshBlock];
    
}

-(void)handleMessage:(id)message{
    if ([message isKindOfClass:NSClassFromString(@"NSConcreteNotification")]) {
        if ([MESSAGE_PAGE isEqualToString:[message name]]) {
            
            
        }
        
    }
}


-(void)choosePhoto{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePickerController animated:YES completion:nil];
        
    }
}

-(void)addSetBtn{
   
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.navRightItem];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    dispatch_block_cancel(_timeBlock);
    dispatch_block_cancel(_refreshBlock);
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (IBAction)clickBtnSet:(UIButton *)sender {
    if (![ROLE_PROJ_PRIN isEqualToString:[User user].roleId]&&![ROLE_MAINT_USER isEqualToString:[User user].roleId]){
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员和维保人员可以修改设备信息" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    
    sender.selected = !sender.selected;
        switch (_pageControl.currentPage) {
            case 0:{
                if (!sender.isSelected){
                    FireDevBasicInfo *basicInfo = ((DevBasicPage *)_pageControl.firstView).devBasicInfo;
                    [[NetWork network]updDevWithDev:basicInfo AndImage:_image success:^(id data) {
                       
                    } fail:^(NSString *message) {
                        
                    } error:^(NSError *error) {
                 
                    }];
                }
                
            }
              
                break;
            case 1:{
                if (!sender.isSelected) {
                    FireDevAppConfigBean *devConfig = ((DevRDPage*)_pageControl.secondView).devConfigBean;
                    [[NetWork network]updDevConfig:devConfig success:^(id data) {
                        NSLog(@"%@",data);
                    } fail:^(NSString *message) {
                        NSLog(@"%@",message);
                    } error:^(NSError *error) {
                        NSLog(@"%@",error);
                    }];
                }
                
            }
                break;
            case 2:
                
                
                break;
                
            default:
                
                break;
        }
    [self changePageStatus];
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    switch (_target) {
        case VCTargetPushMessage:{
            MessagePushVc *controller = (MessagePushVc *)[segue destinationViewController];
            DevMonitorPage *monitorPage = (DevMonitorPage*)_pageControl.thirdView;
            NSNumber *userType = monitorPage.monitorBean.userType;
            if (userType&&userType.intValue == 0) {
                
            }else if(userType&&userType.intValue == 1){
                controller.maintId = monitorPage.monitorBean.maintId;
            }
            controller.devId = monitorPage.monitorBean.devId;
        }
            
            break;
            
        case VCTargetChoosePosition:{
            ChoosePosViewController *controller = (ChoosePosViewController*)[segue destinationViewController];
            controller.devBasicInfo = ((DevBasicPage*)self.pageControl.firstView).devBasicInfo;
            _isRefreshPosition = YES;
        }
          
            break;
            
        default:
            break;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)clickSeg:(UISegmentedControl *)sender {
    self.pageControl.currentPage = sender.selectedSegmentIndex;
    [self changeSetBtnStatus];
  
}

-(void)changeSetBtnStatus{
    if (_pageControl.currentPage == 2) {
        self.navigationItem.rightBarButtonItem = nil;
    }else{
        [self addSetBtn];
        if (_pageControl.currentPage == 0) {
            _btnSet.selected = ((DevBasicPage *)_pageControl.firstView).isSet;
          
        }else if(_pageControl.currentPage == 1){
            _btnSet.selected = ((DevRDPage *)_pageControl.secondView).isSet;
            
        }
    }
}

-(void)changePageStatus{
    switch (_pageControl.currentPage) {
        case 0:
            ((DevBasicPage *)_pageControl.firstView).isSet = _btnSet.isSelected;
            break;
        case 1:
            ((DevRDPage *)_pageControl.secondView).isSet = _btnSet.isSelected;
            break;
        case 2:
            
            break;
            
        default:
            break;
    }
    
}
-(void)addRefreshBlock{
    dispatch_queue_t queue =
    dispatch_queue_create("devRefreshBlock", DISPATCH_QUEUE_SERIAL);
    _refreshBlock = dispatch_block_create(DISPATCH_BLOCK_ASSIGN_CURRENT, ^{
        [self refreshRd:TYPE_REFRESH];
        [self refreshMonitor];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), queue, _refreshBlock);
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)),queue, _refreshBlock);
}

-(void)addTimeBlock{
    dispatch_queue_t queue =
    dispatch_queue_create("timeQueue", DISPATCH_QUEUE_SERIAL);
    _timeBlock = dispatch_block_create(DISPATCH_BLOCK_ASSIGN_CURRENT, ^{
    dispatch_async(dispatch_get_main_queue(), ^{
            [((DevRDPage*)(self.pageControl.secondView)) changeDevRuntime];
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), queue, _timeBlock);
    });
    dispatch_async(queue, _timeBlock);
}

-(void)openCamera{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType =UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:nil];
        
    }
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:nil];
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    
    if ([mediaType isEqualToString:@"public.image"]) {
        //得到照片
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        _image = image;
        [((DevBasicPage*)self.pageControl.firstView) setImage:image];
    }
    
}


@end

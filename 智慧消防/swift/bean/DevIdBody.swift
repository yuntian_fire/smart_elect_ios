//
//  DevIdBody.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/13.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class DevIdBody: BaseBean {
    var devId:String?
    var type:String?
    var userId:String?
    override public func mapping(map: Map) {
        devId <- map["devId"]
        type <- map["type"]
        userId <- map["userId"]
    }
}

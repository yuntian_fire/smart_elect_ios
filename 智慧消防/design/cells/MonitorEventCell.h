//
//  MonitorEventCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/17.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonitorEventCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelEventName;
@property (weak, nonatomic) IBOutlet UILabel *labelEventTime;

@end

//
//  NetWork.m
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "NetWork.h"
#import "UIView+Toast.h"
@implementation NetWork

+ (NetWork*)network
{
    static NetWork* instance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

- (NSURLSessionTask *)getDataWithApi:(NSString *)api param:(NSDictionary*)param completionHandler:(void (^)(NSData *data, NSURLResponse *response,NSError *  error))completionHandler{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 30;
  
    
    NSError *requestError = nil;
    NSString *url = [BASE_URL stringByAppendingString:api];
    
    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:@"GET" URLString:url parameters:param error:&requestError];
    
    NSURLSessionTask *task = [manager.session dataTaskWithRequest:request completionHandler:completionHandler];
    
    return task;
}

- (NSURLSessionTask *)postBodyWithApi:(NSString *)api json:(BaseBean*)json completionHandler:(void (^)(NSData *data, NSURLResponse *response,NSError *  error))completionHandler{
    NSString *jsonStr = [json toJsonStr];
    NSLog(@"send data : %@",jsonStr);
    NSData *data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 30;
    // 设置header
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    NSError *requestError = nil;
    NSString *url = [BASE_URL stringByAppendingString:api];
    
    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:@"POST" URLString:url parameters:nil error:&requestError];
    
    [request setHTTPBody:data];
    NSURLSessionTask *task = [manager.session dataTaskWithRequest:request completionHandler:completionHandler];
    
    return task;
}
#pragma mark -登陆
-(void)loginWithUserBody:(UserBody*)userBody success:(Success)success fail:(Fail)fail
                    error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_LOGIN json:userBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            NSLog(@"loginResult = %@",jsonData);
            LoginResResult *result = [JsonUtil loginResResultWithJson:jsonData];
            NSLog(@"loginResult.state = %@",result.state);
            NSLog(@"loginResult.message = %@",result.message);
            if (result.state.intValue == 0) {
                User *user = [User user];
                user.userName = userBody.userName;
                NSArray<LoginResBean*> * userList = result.data;
                user.userList = userList;
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:user.userSelf.userId forKey:@"userId"];
                [userDefaults setObject:user.userName forKey:@"userName"];
                [userDefaults setObject:jsonData forKey:@"userInfo"];
                [userDefaults synchronize];
                success(result.data);
            }else{
                fail(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:result.message];
                });
            }
            
        }
    }];
    [task resume];
}
#pragma mark -获取项目列表
-(void)getProjsWithUserIdBody:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_PROJS json:userIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            ProjectsResult *result = [JsonUtil projectsResultFromJsonWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
}
#pragma mark -获取首页设备列表
-(void)getProbDev:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_PROB_DEV json:userIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            ProbDevResult *result = [JsonUtil probDevResultFromJsonWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
}
#pragma mark -获取巡检计划列表
-(void)getInspPlanList:(UserIdBean*)userIdBean success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_INSP_PLAN json:userIdBean completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            InspPlanResult *result = [JsonUtil inspPlanResultFromJsonWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
}
#pragma mark -获取巡检记录列表
-(void)getInspRecordList:(UserIdBean*)userIdBean success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_INSP_RECORD json:userIdBean completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            InspRecordResult *result = [JsonUtil inspRecordResultFromJsonWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
}
#pragma mark -获取设备列表
-(void)getProjDevList:(ProjIdBody*)projIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_DEV_INFO json:projIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            ProjDevResult *result = [JsonUtil projDevResultWithJson:jsonData];
            
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
}

#pragma mark -获取报警列表
-(void)getDevAlarmHist:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_ALARM_HIST json:userIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            DevAlarmsResult *result = [JsonUtil devAlarmsResultWithJson:jsonData];
            
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
    
}

#pragma mark -获取故障列表
-(void)getDevFaultHist:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_FAULT_HIST json:userIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            DevAlarmsResult *result = [JsonUtil devAlarmsResultWithJson:jsonData];
            
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
    
}

#pragma mark -获取维保列表
-(void)getMaintList:(UserIdBean*)userIdBean success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_MAINT_LIST json:userIdBean completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            MaintResult *result = [JsonUtil maintResultWithJson:jsonData];

            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
}
#pragma mark -更新项目详情
-(void)updProjWithProj:(FireProjBean *)projBean AndImage:(UIImage *)image success:(Success)success fail:(Fail)fail error:(Error)erro{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    manager.requestSerializer.timeoutInterval = 30;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,API_UPD_PROJ];
    NSString *jsonStr = [projBean toJsonStr];
    NSLog(@"updProjJson = %@",jsonStr);
    NSData * data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
   
    
    
    [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if(image){
            [formData appendPartWithFileData:imageData name:@"file" fileName:[NSString stringWithFormat:@"%@.jpg",projBean.projId ] mimeType:@"image/jpg"];
        }
     
        
        [formData appendPartWithFormData:data name:@"data"];
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        int state = [[responseObject objectForKey:@"state"] intValue];
        NSString *message = [responseObject objectForKey:@"message"];
        id resData = [responseObject objectForKey:@"data"];
        PicResponse *data = [[PicResponse alloc]init];
        data.picTime = [resData objectForKey:@"picTime"];
        NSLog(@"data.picTime = %@",data.picTime);
        data.projId = [resData objectForKey:@"projId"];
         if (state == 0) {
             success(data);
             if (data.picTime) {
                 NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                 [userDefaults setObject:data.picTime forKey:data.projId];
                 [userDefaults synchronize];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[UIApplication sharedApplication].keyWindow makeToast:@"成功"];
             });
         }else{
             fail(message);
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[UIApplication sharedApplication].keyWindow makeToast:message];
             });
         }
   
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        erro(error);
    }];
    
 
    
}
#pragma mark -更新设备详情
-(void)updDevWithDev:(FireDevBasicInfo *)devBasic AndImage:(UIImage *)image success:(Success)success fail:(Fail)fail error:(Error)erro{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    manager.requestSerializer.timeoutInterval = 30;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,API_UPD_DEV];
    NSString *jsonStr = [devBasic toJsonStr];
    NSLog(@"updProjJson = %@",jsonStr);
    NSData * data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    
    [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if(image){
            [formData appendPartWithFileData:imageData name:@"file" fileName:[NSString stringWithFormat:@"%@.jpg",devBasic.devId ] mimeType:@"image/jpg"];
        }
        
        
        [formData appendPartWithFormData:data name:@"data"];
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        int state = [[responseObject objectForKey:@"state"] intValue];
        NSString *message = [responseObject objectForKey:@"message"];
        NSNumber *data = [responseObject objectForKey:@"data"];
        
      
        if (state == 0) {
            success(data);
            if (data) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:data forKey:devBasic.devId];
                [userDefaults synchronize];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication].keyWindow makeToast:@"成功"];
            });
        }else{
            fail(message);
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication].keyWindow makeToast:message];
            });
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        erro(error);
    }];
}

#pragma mark -获取设备详情
-(void)getDevBasicInfo:(DevIdBody*)devIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_DEV_BASIC_INFO json:devIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//                        NSLog(@"jsonData = %@",jsonData);
            DevBasicInfoResult *result = [JsonUtil devBasicInfoResultWithJson:jsonData];
            
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
}




#pragma mark -更新设备数据
-(void)updDevConfig:(FireDevAppConfigBean*)devConfigBean success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_UPD_DEV_CONFIG json:devConfigBean completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            //                        NSLog(@"jsonData = %@",jsonData);
            Result *result = [JsonUtil resultFromJsonWithJson:jsonData];
            
            if (result.state.intValue == 0) {
                success(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:@"成功"];
                });
                
            }else{
                fail(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:result.message];
                });
            }
            
        }
    }];
    [task resume];
}






#pragma mark-获取监控视图数据
-(void)getDevMonitor:(DevIdBody*)devIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_DEV_MONITOR json:devIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//                                    NSLog(@"jsonData = %@",jsonData);
            DevmonitorResult *result = [JsonUtil devMonitorResultWithJson:jsonData];
            
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
            
        }
    }];
    [task resume];
}



#pragma -mark 发送命令
-(void)sendCMD:(FireSendCmdBean*)sendCmdBean success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_SEND_CMD json:sendCmdBean completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            Result *result = [JsonUtil resultFromJsonWithJson:jsonData];
            
            if (result.state.intValue == 0) {
                success(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:@"成功"];
                });
            }else{
                fail(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:result.message];
                });
            }
            
        }
    }];
    [task resume];
}

#pragma -mark获取设备实时数据
-(void)getDevConfig:(DevIdBody*)devIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_DEV_CONFIG json:devIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
                        NSLog(@"jsonData = %@",jsonData);
            
            DevConfigResult *result = [JsonUtil devConfigResultWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
        }
    }];
    [task resume];
}

#pragma mark -添加巡检计划
-(void)saveInsp:(FireInspPlanRequest*)inspRequest success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_SAVE_INSP_PLAN json:inspRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            
            Result *result = [JsonUtil resultFromJsonWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:@"成功"];
                });
            }else{
                fail(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:result.message];
                });
            }
        }
    }];
    [task resume];
}
#pragma mark -查询设备信息
-(void)queryDevInfo:(NSDictionary*)param success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self getDataWithApi:API_QUERY_DEV_INFO param:param completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            
            QueryDevInfoResult *result = [JsonUtil devBaseInfoBeanWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.data);
                
            }else{
                fail(result.message);
            }
        }
    }];
    [task resume];
}
#pragma mark-删除巡检计划
-(void)deleteInsp:(NSDictionary*)param success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self getDataWithApi:API_DEL_INSP_PLAN param:param completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            
            Result *result = [JsonUtil resultFromJsonWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:@"成功"];
                });
            }else{
                fail(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:result.message];
                });
            }
        }
    }];
    [task resume];
}

#pragma mark-获取维保人员
-(void)getSubUsers:(NSDictionary*)param success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self getDataWithApi:API_GET_SUBUSERS param:param completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            NSLog(@"subUsers : %@",jsonData);
            SubUserResult *result = [JsonUtil subUserResultWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
        }
    }];
    [task resume];
}
#pragma mark-维保任务派发
-(void)saveMaintence:(FireMaintRequest*)maintRequest success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_SAVE_MAINT json:maintRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
//            NSLog(@"jsonData = %@",jsonData);
            
            Result *result = [JsonUtil resultFromJsonWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:@"成功"];
                });
            }else{
                fail(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:result.message];
                });
            }
        }
    }];
    [task resume];
}

#pragma mark-维保任务处理
-(void)processMaintenance:(FireDevMaintHandlerRequest*)maintHandleRequest success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_PROCESS_MAINT json:maintHandleRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            //            NSLog(@"jsonData = %@",jsonData);
            
            Result *result = [JsonUtil resultFromJsonWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:@"成功"];
                });
            }else{
                fail(result.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow makeToast:result.message];
                });
            }
        }
    }];
    [task resume];
}

#pragma mark-获取用户信息
-(void)getUserInfo:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro{
    NSURLSessionTask *task = [self postBodyWithApi:API_GET_USER_INFO json:userIdBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"erro %@",error);
            erro(error);
        }else{
            NSString *jsonData = [[NSString alloc]initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
            //            NSLog(@"jsonData = %@",jsonData);
            
            UserInfoResult *result = [JsonUtil userInfoResultWithJson:jsonData];
            if (result.state.intValue == 0) {
                success(result.data);
            }else{
                fail(result.message);
            }
        }
    }];
    [task resume];
}
@end


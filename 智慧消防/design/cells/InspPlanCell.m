//
//  InspPlanCell.m
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "InspPlanCell.h"

@implementation InspPlanCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setInspPlan:(FireInspPlanResponse *)inspPlan{
    _inspPlan = inspPlan;
    self.labelDevId.text = inspPlan.devId;
    
    self.labelCreateTime.text = [Util dateStrFromDate:inspPlan.createTime];
    switch (inspPlan.inspType.intValue) {
        case 0:
            if(inspPlan.inspPeriod.intValue){
                self.labelInspPeriod.text = [NSString stringWithFormat:@"%d天",inspPlan.inspPeriod.intValue];
            }
            break;
            
        case 1:
            self.labelInspPeriod.text = @"自定义";
            break;
        default:
            break;
    }
    
    switch (inspPlan.inspStat.intValue) {
        case 0:
            self.labelInspStat.text = @"待指派";
            break;
        case 1:
            self.labelInspStat.text = @"进行中";
            break;
            
        case 2:
            self.labelInspStat.text = @"已结束";
            break;
            
        default:
            break;
    }
    
    switch (inspPlan.inspPlanStat.intValue) {
        case 0:
            self.labelInspType.text = @"自添加";
            break;
        case 1:
            self.labelInspType.text = @"系统指派";
            break;
        default:
            break;
    }
}

@end

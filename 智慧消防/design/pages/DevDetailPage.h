//
//  DevDetailPage.h
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "智慧消防-Bridging-Header.h"
#import "智慧消防-Swift.h"
#import "Util.h"
@interface DevDetailPage : UIView<UITableViewDelegate,UITableViewDataSource>{

}
@property (weak, nonatomic) IBOutlet PieChartView *pieChartView;
@property (weak, nonatomic) IBOutlet UIButton *btnNormal;
@property (weak, nonatomic) IBOutlet UIButton *btnOffline;
@property (weak, nonatomic) IBOutlet UIButton *btnFault;
@property (weak, nonatomic) IBOutlet UIButton *btnWarn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic)NSArray<FireDevInfoBean*>* devArray;
@property (strong,nonatomic)NSMutableArray * devCountsArray;
@property (weak, nonatomic) IBOutlet UILabel *labelNormal;
@property (weak, nonatomic) IBOutlet UILabel *labelOffline;
@property (weak, nonatomic) IBOutlet UILabel *labelFault;
@property (weak, nonatomic) IBOutlet UILabel *labelAlarm;
@property (strong,nonatomic)NSMutableArray * countArray;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalCount;
@property (strong,nonatomic)FireProjBean *projBean;
@end

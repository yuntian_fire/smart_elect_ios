//
//  MainViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/11/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "NetWork.h"
#import "ScanCodeViewController.h"

@interface MainViewController : UITabBarController{
    int _projPageIndex;
    int _inspIndex;
}
@property (strong, nonatomic) IBOutlet UIView *leftItems;
@property (strong, nonatomic) IBOutlet UIView *rightItems;

@property (weak, nonatomic) IBOutlet UIButton *btnListMode;
@property (weak, nonatomic) IBOutlet UIButton *btnMapMode;
@property (weak, nonatomic) IBOutlet UIButton *btnInspPlan;
@property (weak, nonatomic) IBOutlet UIButton *btnInspRecord;
@property (nonatomic)VCTarget target;

@end

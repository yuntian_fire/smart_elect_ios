//
//  RadiusButton.m
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "RadiusButton.h"

@implementation RadiusButton


- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius = self.radius;
    self.layer.masksToBounds = true;
    
}


@end

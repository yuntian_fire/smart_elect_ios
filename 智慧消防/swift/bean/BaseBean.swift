//
//  BaseBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
public class BaseBean: NSObject,Mappable {
  
    override public init(){}

    public required init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        
    }
    public func toJsonStr() -> String?{
        return self.toJSONString();
    }
   
   
    
}

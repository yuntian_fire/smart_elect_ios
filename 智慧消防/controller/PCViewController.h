//
//  PCViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/11/29.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetWork.h"
@interface PCViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelUserGroup;
@property (weak, nonatomic) IBOutlet UILabel *labelRegistTime;
@property (weak, nonatomic) IBOutlet UILabel *labelVersion;
@property (weak, nonatomic) IBOutlet UILabel *labelProjCount;
@property (weak, nonatomic) IBOutlet UILabel *labelDevCount;
@property (weak, nonatomic) IBOutlet UILabel *labelChildUserCount;
@property (weak, nonatomic) IBOutlet UILabel *labelNormalCount;
@property (weak, nonatomic) IBOutlet UILabel *labelPermmision;
@property (strong,nonatomic) FireUserInfoBean *userInfoBean;
@end

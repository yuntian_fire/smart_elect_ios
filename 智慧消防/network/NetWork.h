//
//  NetWork.h
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "智慧消防-Swift.h"
#import <AFNetworking/AFNetworking.h>
#import "Const.h"
#import "User.h"
@protocol NetWorkDeleget<NSObject>

@required
- (void)netRequestSuccess:(id)data;
- (void)netRequestFail:(NSString*)message;
- (void)netRequestErro:(NSError*)erro;
@end

@interface NetWork : NSObject
@property(weak,nonatomic)id<NetWorkDeleget> delegete;
typedef void(^Success)(id data);
typedef void(^Fail)(NSString* message);
typedef void(^Error)(NSError* error);

- (NSURLSessionTask *)getDataWithApi:(NSString *)api param:(NSDictionary*)param completionHandler:(void (^)(NSData *data, NSURLResponse *response,NSError *  error))completionHandler;
-(NSURLSessionTask *)postBodyWithApi:(NSString *)api json:(id)json completionHandler:(void (^)(NSData *  data, NSURLResponse *  response, NSError *  error))completionHandler;
+(NetWork*)network;
-(void)loginWithUserBody:(UserBody*)userBody success:(Success)success fail:(Fail)fail
error:(Error)erro;
-(void)getProjsWithUserIdBody:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getProbDev:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getInspPlanList:(UserIdBean*)userIdBean success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getInspRecordList:(UserIdBean*)userIdBean success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getProjDevList:(ProjIdBody*)projIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getDevAlarmHist:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getDevFaultHist:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getMaintList:(UserIdBean*)userIdBean success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getDevBasicInfo:(DevIdBody*)devIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getDevMonitor:(DevIdBody*)devIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)sendCMD:(FireSendCmdBean*)sendCmdBean success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getDevConfig:(DevIdBody*)devIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)updDevConfig:(FireDevAppConfigBean*)devConfigBean success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)updProjWithProj:(FireProjBean *)projBean AndImage:(UIImage *)image success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)updDevWithDev:(FireDevBasicInfo *)devBasic AndImage:(UIImage *)image success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)saveInsp:(FireInspPlanRequest*)inspRequest success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)queryDevInfo:(NSDictionary*)param success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)deleteInsp:(NSDictionary*)param success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getSubUsers:(NSDictionary*)param success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)saveMaintence:(FireMaintRequest*)maintRequest success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)processMaintenance:(FireDevMaintHandlerRequest*)maintHandleRequest success:(Success)success fail:(Fail)fail error:(Error)erro;
-(void)getUserInfo:(UserIdBody*)userIdBody success:(Success)success fail:(Fail)fail error:(Error)erro;
@end

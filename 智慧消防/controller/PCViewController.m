//
//  PCViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/11/29.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "PCViewController.h"
#import "Const.h"
@interface PCViewController ()

@end

@implementation PCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:MESSAGE_MAIN_HANDLE object:@"pc"];
    _labelUserGroup.text = [User user].roleName;
    _labelPermmision.text = [User user].roleName;
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    // 获取App的版本号
    NSString *appVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    _labelVersion.text = appVersion;
    UserIdBody *userIdBody = [[UserIdBody alloc]init];
    userIdBody.userId = [User user].userSelf.userId;
    userIdBody.roleId = [User user].roleId;
    
    [[NetWork network] getUserInfo:userIdBody success:^(id data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.userInfoBean = data;
        });
      
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
        
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickMyRemind:(id)sender {
    [self performSegueWithIdentifier:@"pc_to_my_remind" sender:self];
}


-(void)setUserInfoBean:(FireUserInfoBean *)userInfoBean{
    _userInfoBean = userInfoBean;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    _labelRegistTime.text = [formatter stringFromDate:userInfoBean.createDate];
    _labelProjCount.text = userInfoBean.projNum.stringValue;
    _labelDevCount.text = userInfoBean.deviceNum.stringValue;
    _labelChildUserCount.text = userInfoBean.subUserNum.stringValue;
    _labelNormalCount.text = userInfoBean.norDevNum.stringValue;
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

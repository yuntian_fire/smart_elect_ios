//
//  FireInspPlanResponse.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireInspPlanResponse: BaseBean {
    var id:NSNumber?
    var inspPlanId:String?
    var devId:String?
    var inspType:NSNumber?
    var inspPeriod:NSNumber?
    var inspStat:NSNumber?
    var responUser:UserBean?
    var designUser:UserBean?
    var createTime:Date?
    var startTime:Date?
    var endTime:Date?
    var inspPlanStat:NSNumber?
    
    override public func mapping(map: Map) {
        id <- map["id"]
        inspPlanId <- map["inspPlanId"]
        devId <- map["devId"]
        inspType <- map["inspType"]
        inspPeriod <- map["inspPeriod"]
        inspStat <- map["inspStat"]
        responUser <- map["responUser"]
        designUser <- map["designUser"]
        createTime <- (map["createTime"],DateTransform())
        startTime <- (map["startTime"],DateTransform())
        endTime <- (map["endTime"],DateTransform())
        inspPlanStat <- map["inspPlanStat"]
        
        
    }

}

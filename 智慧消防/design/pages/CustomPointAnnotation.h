//
//  CustomPointAnnotation.h
//  智慧消防
//
//  Created by yuntian on 2018/12/25.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKPointAnnotation.h>

@interface CustomPointAnnotation : BMKPointAnnotation
@property (nonatomic)NSInteger index;
@end

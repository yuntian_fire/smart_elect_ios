//
//  SearchView.h
//  智慧消防
//
//  Created by yuntian on 2019/1/2.
//  Copyright © 2019年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchView : UIView
@property (strong,nonatomic)UIView *contentView;
@property (strong,nonatomic)UITextField *fieldTex;
@property (strong,nonatomic)UIButton *searchButton;
@property (strong,nonatomic)UIView* boundView;
@property (nonatomic)BOOL isOpen;
@end

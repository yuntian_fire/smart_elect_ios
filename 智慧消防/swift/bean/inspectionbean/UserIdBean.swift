//
//  UserIdBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class UserIdBean: BaseBean {
    var currentUserId:String?
    var currentRoleId:String?
    
    override public func mapping(map: Map) {
        currentUserId <- map["currentUserId"]
        currentRoleId <- map["currentRoleId"]
    }
}

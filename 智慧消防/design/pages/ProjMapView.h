//
//  ProjMapView.h
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import "CustomAnnotationView.h"
#import "智慧消防-Swift.h"
#import "CustomPointAnnotation.h"
@class ProjViewController;
@interface ProjMapView : BMKMapView<BMKMapViewDelegate,BMKLocationServiceDelegate>
@property (strong,nonatomic) BMKLocationService *locationService;
@property (strong,nonatomic) BMKUserLocation *userLocation;
@property(strong,nonatomic) NSArray<FireProjBean*> * projArray;
@property (strong,nonatomic)ProjViewController *selfVC;
@property (strong,nonatomic)NSArray<CustomPointAnnotation *> *annoArray;
@end


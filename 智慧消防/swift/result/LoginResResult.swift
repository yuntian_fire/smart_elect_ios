//
//  LoginResResult.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class LoginResResult:BaseBean{
    var state:NSNumber?
    var message:String?
    var data:Array<LoginResBean>?
    
    override public func mapping(map: Map) {
        state <- map["state"]
        message <- map["message"]
        data <- map["data"]
    }
}

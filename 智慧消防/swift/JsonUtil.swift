//
//  JsonUtil.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit

class JsonUtil: NSObject {
    public static func resultFromJson(json:String)->Result?{
        return Result(JSONString:json);
    }

    public static func loginResResult(json:String)->LoginResResult?{
        return LoginResResult(JSONString:json);
    }
    
    public static func projectsResultFromJson(json:String)->ProjectsResult?{
        return ProjectsResult(JSONString:json);
    }
    
    public static func probDevResultFromJson(json:String)->ProbDevResult?{
        return ProbDevResult(JSONString:json);
    }
    
    public static func inspPlanResultFromJson(json:String)->InspPlanResult?{
        return InspPlanResult(JSONString:json);
    }
    
    
    public static func inspRecordResultFromJson(json:String)->InspRecordResult?{
        return InspRecordResult(JSONString:json);
    }
    
    public static func projDevResult(json:String)->ProjDevResult?{
        return ProjDevResult(JSONString:json);
    }
    
    public static func devAlarmsResult(json:String)->DevAlarmsResult?{
        return DevAlarmsResult(JSONString:json);
    }
    
    public static func maintResult(json:String)->MaintResult?{
        return MaintResult(JSONString:json);
    }
    
    public static func devBasicInfoResult(json:String)->DevBasicInfoResult?{
        return DevBasicInfoResult(JSONString:json);
    }
    public static func devMonitorResult(json:String)->DevmonitorResult?{
        return DevmonitorResult(JSONString:json);
    }
    public static func devConfigResult(json:String)->DevConfigResult?{
        return DevConfigResult(JSONString:json);
    }
    public static func updProjResult(json:String)->UpdProjResult?{
        return UpdProjResult(JSONString:json);
    }
    public static func devBaseInfoBean(json:String)->QueryDevInfoResult?{
        return QueryDevInfoResult(JSONString:json);
    }
    
    public static func subUserResult(json:String)->SubUserResult?{
        return SubUserResult(JSONString:json);
    }
    public static func userInfoResult(json:String)->UserInfoResult?{
        return UserInfoResult(JSONString:json);
    }

}

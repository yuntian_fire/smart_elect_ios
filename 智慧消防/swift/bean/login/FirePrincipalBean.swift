//
//  FirePrincipalBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FirePrincipalBean: BaseBean {
    var userId:String?
    var name:String?
    var mobile:String?
    var email:String?
    
    override public func mapping(map: Map) {
        userId <- map["userId"]
        name <- map["name"]
        mobile <- map["mobile"]
        email <- map["email"]
    }
}

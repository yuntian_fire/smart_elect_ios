//
//  FireMaintResponse.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/11.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireMaintResponse: BaseBean {
    var taskType:String?
    var degree:String?
    var fireProjName:String?
    var location:String?
    var createTime:String?
    var createUserName:String?
    var expeTime:NSNumber?
    var designUser:UserBean?
    var applicationName:String?
    var devId:String?
    var maintId:String?
    var confirmTime:String?
    var finishTime:String?
    var consuTime:NSNumber?
    var realTime:NSNumber?
    var maintType:NSNumber?
    var process:String?
    var comment:String?
    var endTime:String?
    var devStat:String?
    
    override public func mapping(map: Map) {
        taskType <- map["taskType"]
        degree <- map["degree"]
        fireProjName <- map["fireProjName"]
        location <- map["location"]
        createTime <- map["createTime"]
        createUserName <- map["createUserName"]
        expeTime <- map["expeTime"]
        designUser <- map["designUser"]
        applicationName <- map["applicationName"]
        devId <- map["devId"]
        maintId <- map["maintId"]
        confirmTime <- map["confirmTime"]
        finishTime <- map["finishTime"]
        consuTime <- map["consuTime"]
        realTime <- map["realTime"]
        maintType <- map["maintType"]
        process <- map["process"]
        comment <- map["comment"]
        endTime <- map["endTime"]
        devStat <- map["devStat"]
    }
    

    
    

}

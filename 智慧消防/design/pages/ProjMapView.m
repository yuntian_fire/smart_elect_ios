//
//  ProjMapView.m
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "ProjMapView.h"
#import "ProjViewController.h"

@implementation ProjMapView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.delegate =self;
  
    
    self.showsUserLocation = YES;
    self.userTrackingMode = BMKUserTrackingModeNone;
 
    self.locationService =    [[BMKLocationService alloc]init];
    _locationService.delegate = self;
    [_locationService startUserLocationService];

   
}

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[CustomPointAnnotation class]]) {
        static NSString *annotationReuseIndentifier = @"annotationReuseIndentifier";
        CustomAnnotationView*annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationReuseIndentifier];
//            annotationView.centerOffset = CGPointMake(41, 0);
            annotationView.canShowCallout = NO;
        }
        annotationView.label.text = ((CustomPointAnnotation*)annotation).title;
        NSDictionary*attrs =@{NSFontAttributeName: annotationView.label.font};
        CGFloat fontWidth = [annotationView.label.text  boundingRectWithSize:CGSizeMake(270,MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size.width;
        CGRect labelFrame = CGRectMake(18, 0, fontWidth, 16);
        annotationView.label.frame = labelFrame;
        annotationView.image = [UIImage imageNamed:@"poi"];
        
        return annotationView;
    }
    return nil;
}



- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    if (userLocation) {
        self.userLocation = userLocation;
        [self updateLocationData:userLocation];
        CLLocationCoordinate2D coordinate=userLocation.location.coordinate;
        NSLog(@"您的当前位置:经度：%f,纬度：%f,海拔：%f,航向：%f,速度：%f",coordinate.longitude,coordinate.latitude,userLocation.location.altitude,userLocation.location.course,userLocation.location.speed);
    }
   
}

-(void)setProjArray:(NSArray *)projArray{
    _projArray = projArray;
    if(_annoArray){
        [self removeAnnotations:_annoArray];
    }
    
    NSMutableArray<CustomPointAnnotation*> *annoArray = [[NSMutableArray alloc]init];
  
    for (int i = 0;i<_projArray.count;i++) {
        
        FireProjBean *projBean = _projArray[i];
        CustomPointAnnotation* annotation = [[CustomPointAnnotation alloc]init];
        annotation.coordinate = CLLocationCoordinate2DMake(projBean.lat.doubleValue, projBean.lng.doubleValue);
        annotation.index = i;
        annotation.title = projBean.projNm;
        [annoArray addObject:annotation];
        

    }
    _annoArray = annoArray;

    [self addAnnotations:_annoArray];
    
    
   
}
-(void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view{
    if([view isKindOfClass:[CustomAnnotationView class]]){
        _selfVC.rowIndex = ((CustomPointAnnotation*)(((CustomAnnotationView*)view).annotation)).index;
        _selfVC.target = VCTargetProjInfo;
        [_selfVC performSegueWithIdentifier:@"proj_to_proj_info" sender:_selfVC];
        if (view.isSelected) {
            view.selected = NO;
        }
    }
}
@end

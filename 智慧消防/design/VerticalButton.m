//
//  VerticalButton.m
//  智慧消防
//
//  Created by yuntian on 2018/11/28.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "VerticalButton.h"

@implementation VerticalButton

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.

*/
- (void)drawRect:(CGRect)rect {
    self.titleLabel.textAlignment = UITextAlignmentCenter;
}

-(CGRect)titleRectForContentRect:(CGRect)contentRect{
    CGRect titleBounds = [super titleRectForContentRect:contentRect];
    titleBounds.origin.y = contentRect.size.height - titleBounds.size.height;
    titleBounds.origin.x = 0;
    titleBounds.size.width = self.bounds.size.width;
    return titleBounds;
}

-(CGRect)imageRectForContentRect:(CGRect)contentRect{
    CGRect imgRect = [super imageRectForContentRect:contentRect];
    imgRect.origin.y = 0;
    imgRect.origin.x = (self.bounds.size.width - imgRect.size.width)/2;
    return imgRect;
}


@end

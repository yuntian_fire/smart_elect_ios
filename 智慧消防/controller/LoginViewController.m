//
//  LoginViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "LoginViewController.h"
#import <TACMessaging/XGPush.h>
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [userDefaults objectForKey:@"userName"];
    if(userName){
        self.fieldUserName.text = userName;
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickBtnLogin:(id)sender {
    UserBody *userBody = [[UserBody alloc]init];
    userBody.userName = self.fieldUserName.text;
    userBody.password = self.fieldPassword.text;

    
    NetWork *network = [NetWork network];
    
    [network loginWithUserBody:userBody success:^(id data) {
       
        dispatch_queue_t main_queue = dispatch_get_main_queue();
        dispatch_async(main_queue, ^{
            [[XGPushTokenManager defaultTokenManager]registerDeviceToken:[User user].pushDevToken];
            
            [[XGPushTokenManager defaultTokenManager]bindWithIdentifier:userBody.userName type:XGPushTokenBindTypeAccount];
           
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            id navViewController = [storyboard instantiateViewControllerWithIdentifier:@"nav_view_controller"];
            [self presentViewController:navViewController animated:YES completion:^{
                
            }];
        });
       
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
        
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

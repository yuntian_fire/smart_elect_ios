//
//  RadiusView.m
//  智慧消防
//
//  Created by yuntian on 2018/11/28.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "RadiusView.h"

@implementation RadiusView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius = self.radius;
    self.layer.masksToBounds = true;
}


@end

//
//  LoginResBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class LoginResBean: BaseBean {
    var userType:String?
    var roleId:String?
    var projUser:FirePrincipalBean?
    var maintUser:Array<FirePrincipalBean>?
    
    override public func mapping(map: Map) {
        userType <- map["userType"]
        roleId <- map["roleId"]
        projUser <- map["projUser"]
        maintUser <- map["maintUser"]
    }
}

//
//  TACMessagingApplicationDelegateInjection.h
//  TACMessaging
//
//  Created by Dong Zhao on 2017/12/19.
//

#import <Foundation/Foundation.h>
#import <TACCore/TACCore.h>
@interface TACMessagingApplicationDelegateInjection : NSObject <QCloudApplicationDelegateInjectionProtocol>

@end

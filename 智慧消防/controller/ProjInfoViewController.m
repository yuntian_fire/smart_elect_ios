//
//  ProjInfoViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/4.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "ProjInfoViewController.h"
#import "Const.h"
@interface ProjInfoViewController (){
    UIImage *_image;
    BOOL _isRefreshPosition;
}

@end

@implementation ProjInfoViewController{
    dispatch_block_t _refreshBlock;
}

- (void)viewDidLoad {
    [super viewDidLoad]; 
    [[NSBundle mainBundle] loadNibNamed:@"ProjInfoRightItem" owner:self options:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_navRightItem];
    ProjBasicPage *projBasicPage = (ProjBasicPage*)_pageControl.firstView;
    projBasicPage.selfVC = self;
    DevDetailPage* devPage = (DevDetailPage*)self.pageControl.secondView;
    devPage.projBean = _projBean;
    if(_projBean){
        //        dispatch_queue_t main_queue = dispatch_get_main_queue();
        projBasicPage.projBean = _projBean;
        
    }
    if (_btnSet.isSelected) {
        for (UIView *view in [((ProjBasicPage *)self.pageControl.firstView).scrollView subviews]) {
            view.userInteractionEnabled = YES;
        }
    }else{
        for (UIView *view in [((ProjBasicPage *)self.pageControl.firstView).scrollView subviews]) {
            view.userInteractionEnabled = NO;
        }
    }
    [self refreshDevList];
}
-(void)handleMessage:(id)message{
     if ([message isKindOfClass:NSClassFromString(@"NSConcreteNotification")]) {
         if ([MESSAGE_PAGE isEqualToString:[message name]]) {
             NSDictionary *info = [message userInfo];
             NSString * to = [info objectForKey:@"to"];
             NSNumber *rowIndex = [info objectForKey:@"rowIndex"];
             self.rowIndex = rowIndex.integerValue;
             if ([@"dev_info" isEqualToString:to]) {
                 _target = VCTargetDevInfo;
                 [self performSegueWithIdentifier:@"proj_info_to_dev_info" sender:self];
             }else if([@"camera" isEqualToString:to]){
//                 if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//
//
//                 }
                 if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                     UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                     imagePickerController.delegate = self;
                     imagePickerController.allowsEditing = YES;
                     imagePickerController.sourceType =UIImagePickerControllerSourceTypeCamera;
                     [self presentViewController:imagePickerController animated:YES completion:nil];
                     
                 }
                 
                 
                 
              
                 
                 
             }
         }
         
     }
}

-(void)choosePhoto{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePickerController animated:YES completion:nil];
        
    }
}

-(void)refreshPosition{
    ProjBasicPage *basicPage = (ProjBasicPage *)self.pageControl.firstView;
    basicPage.fieldLat.text = _projBean.lat;
    basicPage.fieldLon.text = _projBean.lng;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_isRefreshPosition) {
        [self refreshPosition];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessage:) name:MESSAGE_PAGE object:nil];
    
    [self addRefreshBlock];
   

    
   
}

-(void)refreshDevList{
    if(_projBean){
        DevDetailPage* devPage = (DevDetailPage*)self.pageControl.secondView;
        
        ProjIdBody *projIdBody = [[ProjIdBody alloc]init];
        projIdBody.projId = self.projBean.projId;
        [[NetWork network] getProjDevList:projIdBody success:^(id data) {
            
            dispatch_queue_t main_queue = dispatch_get_main_queue();
            dispatch_async(main_queue, ^{
                devPage.devArray = data;
            });
            
        } fail:^(NSString *message) {
            NSLog(@"%@",message);
        } error:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    

   
   
}
-(void)viewDidLayoutSubviews{
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segValueChanged:(UISegmentedControl *)sender {
    self.pageControl.currentPage = [sender selectedSegmentIndex];
    if(sender.selectedSegmentIndex == 0){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_navRightItem];
        _btnSet.selected = ((ProjBasicPage*)_pageControl.firstView).isSet;
    }else{
        self.navigationItem.rightBarButtonItem = nil;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    dispatch_block_cancel(_refreshBlock);
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
   
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (_target == VCTargetDevInfo) {
        DevInfoViewController *controller  = [segue destinationViewController];
        DevDetailPage *devDetailPage = (DevDetailPage *)self.pageControl.secondView;
        controller.devInfoBean = devDetailPage.devArray[_rowIndex];
    }else if(_target == VCTargetChoosePosition ){
        ChoosePosViewController *controller  = [segue destinationViewController];
        controller.projBean = self.projBean;
        _isRefreshPosition = true;
    }
    
    _target = -1;
  
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:nil];
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    
    if ([mediaType isEqualToString:@"public.image"]) {
        //得到照片
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        _image = image;
        [((ProjBasicPage*)self.pageControl.firstView) setImage:image];
    }

}

- (IBAction)clickSet:(UIButton *)sender {
    if (![[User user].roleId isEqualToString:ROLE_PROJ_PRIN]) {
       UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员可以修改项目信息" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    sender.selected = !sender.isSelected;
    ((ProjBasicPage*)_pageControl.firstView).isSet = sender.isSelected;
    if (sender.isSelected) {
        for (UIView *view in [((ProjBasicPage *)self.pageControl.firstView).scrollView subviews]) {
            view.userInteractionEnabled = YES;
        }
    }else{
        for (UIView *view in [((ProjBasicPage *)self.pageControl.firstView).scrollView subviews]) {
            view.userInteractionEnabled = NO;
        }
        [[NetWork network] updProjWithProj:((ProjBasicPage*)(_pageControl.firstView)).projBean AndImage:_image success:^(id data) {
            NSLog(@"%@",data);
        } fail:^(NSString *message) {
            NSLog(@"%@",message);
        } error:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
    
    
}

-(void)addRefreshBlock{
    dispatch_queue_t queue =
    dispatch_queue_create("devListRefreshBlock", DISPATCH_QUEUE_SERIAL);
    _refreshBlock = dispatch_block_create(DISPATCH_BLOCK_ASSIGN_CURRENT, ^{
        [self refreshDevList];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), queue, _refreshBlock);
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)),queue, _refreshBlock);
}

@end

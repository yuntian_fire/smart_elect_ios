//
//  ProjCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelAddDev;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalCount;
@property (weak, nonatomic) IBOutlet UILabel *labelNormalCount;
@property (weak, nonatomic) IBOutlet UILabel *labelWarnCount;
@property (weak, nonatomic) IBOutlet UILabel *labelOfflineCount;
@property (weak, nonatomic) IBOutlet UILabel *labelFaultCount;
@property (weak, nonatomic) IBOutlet UILabel *labelProjNm;
@property (weak, nonatomic) IBOutlet UILabel *labelLocation;
@property (weak, nonatomic) IBOutlet UILabel *labelProjId;
@property (weak, nonatomic) IBOutlet UIImageView *imgProj;

@end

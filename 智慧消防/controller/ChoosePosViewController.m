//
//  ChoosePosViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/25.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "ChoosePosViewController.h"
#import "Util.h"
@interface ChoosePosViewController (){
    BOOL _isFirstLoad;
}
@property (nonatomic,strong)BMKPointAnnotation *annotation;
@end

@implementation ChoosePosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSBundle mainBundle] loadNibNamed:@"CPRightItem" owner:self options:nil];
    _isFirstLoad = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_rightItem];
    _mapView.delegate = self;
    if(_projBean){
        _annotation = [[BMKPointAnnotation alloc]init];
      
        if(_projBean.lat&&_projBean.lng){
            double lat = _projBean.lat.doubleValue;
            double lng = _projBean.lng.doubleValue;
            [self addAnnotationWith:lat and:lng];
        }else{
            self.locationService =    [[BMKLocationService alloc]init];
            _locationService.delegate = self;
            [_locationService startUserLocationService];
        }
        
        
    }
    
    
    if (_devBasicInfo) {
        _annotation = [[BMKPointAnnotation alloc]init];
        
        if (![Util isNull:_devBasicInfo.devLat]&&![Util isNull:_devBasicInfo.devLng]) {
            double lat = _devBasicInfo.devLat.doubleValue;
            double lng = _devBasicInfo.devLng.doubleValue;
            [self addAnnotationWith:lat and:lng];
        }else{
            self.locationService =    [[BMKLocationService alloc]init];
            _locationService.delegate = self;
            [_locationService startUserLocationService];
        }
        
    }
 
   
    
    
    // Do any additional setup after loading the view.
}
-(void)addAnnotationWith:(double)lat and:(double)lng{
    _annotation.coordinate = CLLocationCoordinate2DMake(lat, lng);
    [_mapView addAnnotation:_annotation];
    [_mapView setCenterCoordinate:_annotation.coordinate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickBtnComplete:(id)sender {
    if (_projBean) {
        _projBean.lat = [NSString stringWithFormat:@"%f",_annotation.coordinate.latitude];
        _projBean.lng = [NSString stringWithFormat:@"%f",_annotation.coordinate.longitude];
    }
  
    if (_devBasicInfo) {
        _devBasicInfo.devLat = [NSString stringWithFormat:@"%f",_annotation.coordinate.latitude];
        _devBasicInfo.devLng = [NSString stringWithFormat:@"%f",_annotation.coordinate.longitude];
    }
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation{
    
  
    
    return nil;
    
}

-(void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate{
    _annotation.coordinate = coordinate;
}

- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    if (userLocation) {
       
        CLLocationCoordinate2D coordinate=userLocation.location.coordinate;
        if (_isFirstLoad) {
            [self addAnnotationWith:coordinate.latitude and:coordinate.longitude];
            _isFirstLoad = NO;
            [_locationService stopUserLocationService];
        }
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  AddInspViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/25.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "AddInspViewController.h"

@interface AddInspViewController ()

@end

@implementation AddInspViewController{
    BOOL _keyboardIsVisible;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center  addObserver:self selector:@selector(keyboardDidShow)  name:UIKeyboardDidShowNotification  object:nil];
    [center addObserver:self selector:@selector(keyboardDidHide)  name:UIKeyboardWillHideNotification object:nil];
    _keyboardIsVisible = NO;
    if (!_inspRequest) {
        _inspRequest = [[FireInspPlanRequest alloc]init];
    }
    
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    [self.view addSubview:self.contentView];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSArray * arrayViews = [UIApplication sharedApplication].keyWindow.subviews;
    if (arrayViews.count>0) {
        //array会有两个对象，一个是UILayoutContainerView，另外一个是UITransitionView，我们找到最后一个
        UIView * backView = arrayViews.lastObject;
        //我们可以在这个backView上添加点击事件，当然也可以在其子view上添加，如下：
        //        NSArray * subBackView = [backView subviews];
        //        backView = subBackView[0];  或者如下
        //        backView = subBackView[1];
        backView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(back)];
        [backView addGestureRecognizer:tap];
    }
    if (_devId) {
        
        _fieldDevId.text = _devId;
        _fieldDevId.enabled = NO;
        
    }
    
    
    NSString *user = [NSString stringWithFormat:@"%@/%@",[User user].userSelf.name,[User user].userSelf.mobile];
    _labelInspP.text = user;
    NSMutableArray *dataSource = [[NSMutableArray alloc]init];
    for (FirePrincipalBean *user in [User user].maintUserList) {
        [dataSource addObject:[[EBDropdownListItem alloc]initWithItem:nil itemName:user.name]];
        
    }
    _dropAsiNm.dataSource = dataSource;
    _dropAsiNm.customData = [User user].maintUserList;
    [_dropAsiNm setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        
        _fieldAsiPhone.text = ((FirePrincipalBean*)_dropAsiNm.customData[_dropAsiNm.selectedIndex]).mobile;
        _inspRequest.designUserId = ((FirePrincipalBean*)_dropAsiNm.customData[_dropAsiNm.selectedIndex]).userId;
    }];
    
    
}

- (void)keyboardDidShow
{
    _keyboardIsVisible = YES;
}

- (void)keyboardDidHide
{
    _keyboardIsVisible = NO;
}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    CGRect contentFrame = CGRectMake(10, self.view.frame.size.height/2 - 175, self.view.frame.size.width-20, 350);
    _contentView.frame = contentFrame;
   
}


-(UIView *)contentView{
    if (!_contentView) {
        [[NSBundle mainBundle]loadNibNamed:@"AddInspVc" owner:self options:nil];
    }
    
    return _contentView;
}
- (IBAction)clickBtnStartDate:(UIButton *)sender {

    UIDatePicker *datePicker = [[UIDatePicker alloc] init];//初始化一个UIDatePicker
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.locale =  [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"选择时间" message:@"\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];//初始化一个标题为“选择时间”，风格是ActionSheet的UIAlertController，其中"\n"是为了给DatePicker腾出空间
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];//设置输出的格式
        [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
        [sender setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
        _inspRequest.startTime = datePicker.date;
    
    }];
   
    
    
    [alert.view addSubview:datePicker];//将datePicker添加到UIAlertController实例中
    [alert addAction:confirm];//将确定按钮添加到UIAlertController实例中
    [self presentViewController:alert animated:YES completion:nil];

}
- (IBAction)clickBtnEndDate:(id)sender {
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];//初始化一个UIDatePicker
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.locale =  [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"选择时间" message:@"\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];//初始化一个标题为“选择时间”，风格是ActionSheet的UIAlertController，其中"\n"是为了给DatePicker腾出空间
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];//设置输出的格式
        [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
        [sender setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
        _inspRequest.endTime = datePicker.date;
        
    }];
    
    
    
    [alert.view addSubview:datePicker];//将datePicker添加到UIAlertController实例中
    [alert addAction:confirm];//将确定按钮添加到UIAlertController实例中
    [self presentViewController:alert animated:YES completion:nil];

}
-(FireInspPlanRequest *)inspRequest{
    _inspRequest.devId = _fieldDevId.text;
    NSString *cycle = _fieldInspCycle.text;
    if ([Util isNull:cycle]) {
        _inspRequest.inspType = [NSNumber numberWithInt:1];
    }else{
        _inspRequest.inspType = [NSNumber numberWithInt:0];
        _inspRequest.inspPeriod = [NSNumber numberWithInt:cycle.intValue];
        
    }
    _inspRequest.responUserId = [User user].userSelf.userId;
    _inspRequest.currentUserId = [User user].userSelf.userId;
    
    
    return _inspRequest;
}

-(void)back{
    if (_keyboardIsVisible) {
        [self.view endEditing:YES];
    }else{
        [self dismissViewControllerAnimated:NO completion:nil];
    }
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickConfirm:(id)sender {
    NSLog(@"%f",[[NSDate date] timeIntervalSince1970]);
    
    [[NetWork network] saveInsp:self.inspRequest success:^(id data) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
    } fail:^(NSString *message) {
        
        
    } error:^(NSError *error) {
        NSLog(@"%@",error);
        
    }];
    
}

- (IBAction)clickCancel:(id)sender {
      [self dismissViewControllerAnimated:NO completion:nil];
}


@end

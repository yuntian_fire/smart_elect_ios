//
//  DevRDPage.m
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "DevRDPage.h"
#import "LoginViewController.h"
#import "DevRdSenserCell.h"
#import "DevInfoViewController.h"
@implementation DevRDPage{
    NSInteger _sensorPageIndex;
}
static NSString *devRdSenserCell = @"DevRdSenserCell";
-(void)awakeFromNib{
    [super awakeFromNib];
    UINib *nib = [UINib nibWithNibName:@"DevRdSenserCell" bundle:[NSBundle mainBundle]];
    [_tableDTU registerNib:nib forCellReuseIdentifier:devRdSenserCell];
    [_tableSenser registerNib:nib forCellReuseIdentifier:devRdSenserCell];
    _tableDTU.rowHeight = 25;
    _tableSenser.rowHeight = 25;
}



- (IBAction)clickBTMode:(id)sender {
    NSMapTable *map = _devConfigBean.testNameDisplay;
    NSLog(@"%@",map);
 
}
- (IBAction)clickGPRSMode:(id)sender {
    
    
}
- (void)setDevConfigBean:(FireDevAppConfigBean *)devConfigBean{
    _devConfigBean = devConfigBean;
    [self changeHeartBeatUnit];
    _labelDevMode.text = devConfigBean.devMode;
    _labelDevId.text = devConfigBean.devId;
  
    [self initEx];
    [self initSenserData];
    [self initEle];
//    _tableDTU.translatesAutoresizingMaskIntoConstraints = YES;
//    CGRect frame = _tableDTU.frame;
//    frame.size.height = 20*3;
//    _tableDTU.frame = frame;
    
}

-(void)initEx{
    NSArray *exArray = _devConfigBean.configExtList;
    NSMutableArray *dataSource = [[NSMutableArray alloc]init];
    if (exArray) {
        for (int i = 0; i < exArray.count; i++) {
            ConfigExt *configExt = exArray[i];
            if (configExt.devType) {
                if (configExt.devType.intValue == 0) {
                    [dataSource addObject:
                     [[EBDropdownListItem alloc]initWithItem:nil itemName:@"DTU"]];
                }else{
                    NSString *name = [NSString stringWithFormat:@"探测器%d",configExt.devType.intValue];
                    [dataSource addObject:
                    [[EBDropdownListItem alloc]initWithItem:nil itemName:name]];
                }
            }else{
                [dataSource addObject:
                 [[EBDropdownListItem alloc]initWithItem:nil itemName:@""]];
            }
        }
    }
    
    
    
    if (dataSource.count == 0) {
        return;
    }
    
    _dropDev.dataSource = dataSource;
    _dropDev.customData = _devConfigBean.configExtList;
    
    [self initRelay];
    
    [_dropDev setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        [self initRelay];
    }];
   
}

-(void)initRelay{
    NSMutableArray *dataSource = [[NSMutableArray alloc]initWithObjects:
                                  [[EBDropdownListItem alloc]initWithItem:nil itemName:@"常开"],
                              [[EBDropdownListItem alloc]initWithItem:nil itemName:@"常闭"], nil];
    _dropRelay.dataSource = dataSource;
    _dropRelay.customBody = _dropDev.customData[_dropDev.selectedIndex];
    
    ConfigExt *configExt = _dropRelay.customBody;
    [_dropRelay setSelectedIndex:configExt.expSwitch.integerValue];
    
    [_dropRelay setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        ((ConfigExt *)_dropRelay.customBody).expSwitch = [NSNumber numberWithInteger:_dropRelay.selectedIndex];
        [self initEWC];
    }];
    
    [self initEWC];
    
}

-(void)initEWC{
    NSMutableArray *dataSource = [[NSMutableArray alloc]initWithObjects:
                                  [[EBDropdownListItem alloc]initWithItem:nil itemName:@"脉冲"],
                                  [[EBDropdownListItem alloc]initWithItem:nil itemName:@"保持"], nil];
    _dropEx.dataSource = dataSource;
    [_dropEx setSelectedIndex:((ConfigExt *)_dropRelay.customBody).expPulse.integerValue];
    [_dropEx setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        ((ConfigExt *)_dropRelay.customBody).expPulse = [NSNumber numberWithInteger:_dropEx.selectedIndex];
    }];
    
}


-(void)initSenserTitles{
    for (UIView *view in _scrollSenserTitle.subviews) {
        [view removeFromSuperview];
    }
    for (int i = 0; i < _sensorTitlesList.count; i++) {
        UIButton *titleButton = [[UIButton alloc]initWithFrame:CGRectMake( _scrollSenserTitle.subviews.count*60, 0, 59, 25)];
        [_scrollSenserTitle addSubview: titleButton];
        [titleButton setTitle:_sensorTitlesList[i] forState:UIControlStateNormal];
        [titleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [titleButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
        
        titleButton.tag = i;
        [titleButton addTarget:self action:@selector(clickSenserTitles:) forControlEvents:UIControlEventTouchUpInside];
        
        if([_sensorTitlesList[i] isEqualToString:@"DTU"]){
            _dtuArray = _detList[0];
            [self layoutDTUTable];
            [_tableDTU reloadData];
           
            [titleButton setBackgroundColor:[UIColor darkGrayColor]];
        }
        
    }
    
    _scrollSenserTitle.contentSize = CGSizeMake(_scrollSenserTitle.subviews.count*60, 0);

    
}

-(void)clickSenserTitles:(UIButton *)sender{
    for (int i = 0; i<_scrollSenserTitle.subviews.count; i++) {
        UIButton *button = _scrollSenserTitle.subviews[i];
        if (button.tag == sender.tag) {
            [button setBackgroundColor:[UIColor darkGrayColor]];
            _sensorPageIndex = button.tag;
            if ([button.currentTitle isEqualToString:@"DTU"]) {
                _sensorArray = nil;
            }else{
                _sensorArray = _detList[i];
            }
            [self layoutSensorTable];
            [_tableSenser reloadData];
        
            
        }else{
            [button setBackgroundColor:[UIColor whiteColor]];
        }
    }
    
}
-(void)layoutDTUTable{
    CGRect frame = _tableDTU.frame;
    if(_dtuArray){
//        frame.size.height = _dtuArray.count*25;
        frame.size.height = 5*25;
    }else{
        frame.size.height = 0;
    }
    _tableDTU.translatesAutoresizingMaskIntoConstraints = YES;
    _tableDTU.frame = frame;
}
-(void)layoutSensorTable{
    CGRect frame = _tableSenser.frame;
    if(_sensorArray){
        frame.size.height = _sensorArray.count*25;
    }else{
        frame.size.height = 0;
    }
    _tableSenser.translatesAutoresizingMaskIntoConstraints = YES;
    _tableSenser.frame = frame;
}

-(void)changeHeartBeatUnit{
    
    if ([_btnUnit.currentTitle isEqualToString:@"s"]) {
  
        _fieldHBTime.text = [NSString stringWithFormat:@"%d",_devConfigBean.heartTime.intValue];
     
    }else{
        int mTime = (int)(_devConfigBean.heartTime.floatValue/60 + 0.5);
        _fieldHBTime.text = [NSString stringWithFormat:@"%d",mTime];
    }
    
}
-(void)removeFromSuperview{
    [super removeFromSuperview];
    NSLog(@"removeFromSuperView");
}
- (IBAction)clickBtnUnit:(UIButton *)sender {
   
    if ([ sender.currentTitle isEqualToString:@"s"]) {
        [sender setTitle:@"m" forState:UIControlStateNormal];
    }else{
        [sender setTitle:@"s" forState:UIControlStateNormal];
    }
    [self changeHeartBeatUnit];
}

-(void)initSenserData{
    NSMutableArray<NSMutableArray<ConfigSense*>*>* detList = [[NSMutableArray alloc]init];
    NSMutableArray<ConfigSense*>* list = [[NSMutableArray alloc]init];
    NSMutableArray<NSString*> *sensorTitlesList = [[NSMutableArray alloc]init];
    int index = 0;
    NSArray<ConfigSense*> *configSenseList = _devConfigBean.configSenseList;
    if (configSenseList) {
        
        for (int i = 0; i < configSenseList.count - 1; i++) {
            if (configSenseList[i].devType) {
                NSInteger circle = configSenseList[i].circle.integerValue;
                NSInteger partAddr = configSenseList[i].partAddr.integerValue;
                NSInteger circle1 = configSenseList[i+1].circle.integerValue;
                NSInteger partAddr1 = configSenseList[i+1].partAddr.integerValue;
                
      
                
                if ([[NSString stringWithFormat:@"%ld%ld",circle,partAddr]isEqualToString:[NSString stringWithFormat:@"%ld%ld",circle1,partAddr1]]) {
                    
                    [list addObject:configSenseList[i]];
                    
                } else {
                    [list addObject:configSenseList[i]];
                    [detList addObject:list];
                    if (configSenseList[i].devType.intValue == 0) {
                        [sensorTitlesList addObject:@"DTU"];
                    } else {
                        index++;
                        
                        [sensorTitlesList addObject:[NSString stringWithFormat:@"探测器%d",index]];
                    }
                    list = [[NSMutableArray alloc]init];
                }
                if (i == configSenseList.count - 2) {
                    
                    [list addObject:configSenseList[i+1]];
                    [detList addObject:list];
                    if (configSenseList[i+1].devType.intValue == 0) {
                        [sensorTitlesList addObject:@"DTU"];
                    } else {
                        index++;
                        [sensorTitlesList addObject:[NSString stringWithFormat:@"探测器%d",index]];
                    }
                }
            }
        }
        
        
        for (int i = 0; i < sensorTitlesList.count; i++) {
            if ([sensorTitlesList[i] isEqualToString:@"DTU"]) {
                _sensorPageIndex = i;
                break;
            }
        }
        _detList = detList;
        _sensorTitlesList = sensorTitlesList;
    }
    [self initSenserTitles];
    
}

-(void) initEle {
   
    NSArray *eleclRatioJudge = _devConfigBean.eleclRatioJudge;
    
    NSArray * configSenseList = _devConfigBean.configSenseList;
    if (eleclRatioJudge  && configSenseList ) {
        NSMutableArray *configSenses = [[NSMutableArray alloc]init];
        for (int i = 0; i < configSenseList.count; i++) {
            ConfigSense *configSense = configSenseList [i];
            for (int j = 0; j < eleclRatioJudge.count; j++) {
                if ([configSense.testName isEqualToNumber:eleclRatioJudge[j]]) {
                    [configSenses addObject:configSense];
                }
            }
        }
        for (int i = 1; i < 6; i++) {
            UILabel *label = (UILabel*)[self viewWithTag:i];
            EBDropdownListView *drop = (EBDropdownListView*)[self viewWithTag:10+i];
           
            NSArray *dataSource = [NSArray arrayWithObjects:[[EBDropdownListItem alloc]initWithItem:nil itemName:@"100:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"200:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"400:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"500:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"800:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"1000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"2000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"3000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"4000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"5000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"6000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"8000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"10000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"20000:1"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"40000:1"], nil];
            
            drop.dataSource = dataSource;
           
            if (configSenses.count>=i) {
               
                drop.hidden = NO;
                ConfigSense *configSense = configSenses [i - 1];
                if (configSense.devType&&configSense.devType.intValue == 0) {
                    label.text = [NSString stringWithFormat:@"DTU-CH%@",configSense.passage];
                }else{
                    label.text = [NSString stringWithFormat:@"CH%@",configSense.passage];
                }
                
                if (configSense.elecRatio&&configSense.elecRatio.intValue>=1 &&configSense.elecRatio.intValue<=15) {
                    drop.selectedIndex = configSense.elecRatio.intValue;
                }
                [drop setSelectedBlock:^(EBDropdownListView *dropdownListView) {
                    configSense.elecRatio = [NSNumber numberWithInteger:drop.selectedIndex-1];
                }];
            }else{
                drop.hidden = YES;
                
                
            }
            
            
        }
    }
}

-(void)changeDevRuntime{
    
    
    if (!_devConfigBean||!_devConfigBean.regisTime||!_devConfigBean.onlineStat||!_devConfigBean.onlineStat.intValue) {
        _labelDevRunTime.text = [NSString stringWithFormat:@"%d",0];
        return;
    }
    
    long secondTime = [[NSDate date] timeIntervalSince1970] - [_devConfigBean.regisTime timeIntervalSince1970];
    long day = secondTime/3600/24;
    long hour = (secondTime - day*3600*24)/3600;
    long min = (secondTime - day*3600*24 - hour*3600)/60;
    long second = secondTime - day*3600*24 - hour*3600 - min*60;
    NSString *dayStr = [NSString stringWithFormat:@"%ld",day];
    NSString *hourStr = [NSString stringWithFormat:@"%ld",hour];
    NSString *minStr = [NSString stringWithFormat:@"%ld",min];
    NSString *secStr = [NSString stringWithFormat:@"%ld",second];
    if (day<0) {
        dayStr = [NSString stringWithFormat:@"%d%@",0,dayStr];
    }
    if (hour<0) {
        hourStr = [NSString stringWithFormat:@"%d%@",0,hourStr];
    }
    
    if (min<0) {
        minStr = [NSString stringWithFormat:@"%d%@",0,minStr];
    }
    
    if (second<0) {
        secStr = [NSString stringWithFormat:@"%d%@",0,secStr];
    }
    _labelDevRunTime.text = [NSString stringWithFormat:@"%@天 %@:%@:%@",dayStr,hourStr,minStr,secStr];
    
    
  
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _tableDTU) {
        DevRdSenserCell *cell = [_tableDTU dequeueReusableCellWithIdentifier:devRdSenserCell];
        
        cell.configSense = _dtuArray[indexPath.row];
        cell.labelChannel.text = [NSString stringWithFormat:@"DTU-CH%d",cell.configSense.passage.intValue];
        
        return  cell;
    }else{
        DevRdSenserCell *cell = [_tableSenser dequeueReusableCellWithIdentifier:devRdSenserCell];
        cell.configSense = _sensorArray[indexPath.row];
        cell.labelChannel.text = [NSString stringWithFormat:@"CH%d",cell.configSense.passage.intValue];
        return  cell;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_tableDTU == tableView){
        return _dtuArray?_dtuArray.count:0;
    }else{
        return _sensorArray?_sensorArray.count:0;
    }
    
 
}

-(void)setIsSet:(BOOL)isSet{
    _isSet = isSet;
    for (UIView *subView in self.subviews) {
        if (!(subView == _viewTitle||subView == _btnRefresh || subView == _btnGetDevConfig || subView == _tableDTU || subView == _tableSenser)){
            subView.userInteractionEnabled = isSet;
        }else if(subView == _tableDTU){
           
        }
    }
}
- (IBAction)clickBtnRefresh:(id)sender {
    [_selfVC refreshRd:TYPE_REFRESH];
}
- (IBAction)clickBtnGetDevConfig:(id)sender {
    [_selfVC refreshRd:TYPE_GET_DEV_CONFIG];
}
@end

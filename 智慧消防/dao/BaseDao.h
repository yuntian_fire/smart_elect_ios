//
//  BaseDao.h
//  智慧消防
//
//  Created by yuntian on 2018/12/13.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDB.h>
#import "智慧消防-Swift.h"

@interface BaseDao : NSObject<NSStreamDelegate>{
    FMDatabase *_db;
}
@property (nonatomic,copy)NSString *areaSql;
+ (instancetype)sharedDao;
- (NSArray*)getAreaProvince;
-(NSArray*)findAreaByParentId:(int)parentId;
-(Area*)findAreaByCode:(NSString*)code;
@end

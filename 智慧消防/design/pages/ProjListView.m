//
//  ProjListView.m
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "ProjListView.h"
#import "ProjCell.h"
#import "Const.h"
@implementation ProjListView
static NSString * projCell = @"ProjCell";
-(void)awakeFromNib{
    [super awakeFromNib];
    UINib *nib = [UINib nibWithNibName:@"ProjCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:projCell];
    self.tableView.rowHeight = 75;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.tableView addGestureRecognizer:tapGesture];
}






- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ProjCell *cell = [self.tableView dequeueReusableCellWithIdentifier:projCell];
    FireProjBean *projBean = _projArray[indexPath.row];
    cell.labelProjNm.text = projBean.projNm;
    cell.labelProjId.text = projBean.projId;
    cell.labelLocation.text = projBean.projAddr;
    cell.labelTotalCount.text = [NSString stringWithFormat:@"%d",projBean.totalCount.intValue];
    cell.labelNormalCount.text = [NSString stringWithFormat:@"%d",projBean.nomal.intValue];
    cell.labelOfflineCount.text = [NSString stringWithFormat:@"%d",projBean.offline.intValue];
    cell.labelWarnCount.text = [NSString stringWithFormat:@"%d",projBean.alarm.intValue];
    cell.labelFaultCount.text = [NSString stringWithFormat:@"%d",projBean.fault.intValue];
    switch (indexPath.row%4) {
        case 0:
            cell.imgProj.image = [UIImage imageNamed:@"proj_red"];
            break;
        case 1:
            cell.imgProj.image = [UIImage imageNamed:@"proj_green"];
            break;
        case 2:
            cell.imgProj.image = [UIImage imageNamed:@"proj_yellow"];
            break;
            
        case 3:
            cell.imgProj.image = [UIImage imageNamed:@"proj_blue"];
            
            break;
        default:
            break;
    }
   
    
    
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.projArray?_projArray.count:0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
}
- (void)tapGesture:(UIGestureRecognizer *)gesture{
    //获得当前手势触发的在UITableView中的坐标
    CGPoint location = [gesture locationInView:self.tableView];
    //获得当前坐标对应的indexPath
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    if (indexPath) {
        //通过indexpath获得对应的Cell
        ProjCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        //获得当前手势点击在UILabe中的坐标
        CGPoint p = [gesture locationInView:cell.contentView];
        //看看手势点的坐标是不是在UILabel中
        NSMutableDictionary *info = [NSMutableDictionary dictionary];
        NSInteger rawIndex = indexPath.row;
        NSNotification *notificaiton = [[NSNotification alloc]initWithName:MESSAGE_PAGE object:nil userInfo:info];
        [info setValue:[NSNumber numberWithInteger:rawIndex] forKey:@"rowIndex"];
        if (CGRectContainsPoint(cell.labelAddDev.frame, p)) {
            [info setValue:@"dev_add" forKey:@"to"];
        }else{
            [info setValue:@"proj_info" forKey:@"to"];
            
        }
        [[NSNotificationCenter defaultCenter] postNotification:notificaiton];
    }

}

-(void)setProjArray:(NSArray<FireProjBean *> *)projArray{
    _projArray = projArray;
    [self.tableView reloadData];
   
}
-(void)removeFromSuperview{
    [super removeFromSuperview];
    NSLog(@"removeFromSuperView");
  
}


@end

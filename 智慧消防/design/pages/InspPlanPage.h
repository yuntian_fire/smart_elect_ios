//
//  InspPlanPage.h
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "智慧消防-Swift.h"
typedef NS_ENUM(uint,InspSelType) {
    InspSelTypeAll,
    InspSelTypeSelfAdd,
    InspSelTypeSys
};
@class InspViewController;
@interface InspPlanPage : UIView<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSArray<FireInspPlanResponse*> *inspPlanArray;
@property (strong,nonatomic) NSArray<FireInspPlanResponse*> *inspPlanSelArray;
@property (strong,nonatomic) InspViewController *selfVC;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segTitle;
@property (nonatomic)InspSelType selType;
@end

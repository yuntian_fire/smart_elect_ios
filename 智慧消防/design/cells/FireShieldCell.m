//
//  FireShieldCell.m
//  智慧消防
//
//  Created by yuntian on 2018/12/21.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "FireShieldCell.h"

@implementation FireShieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickBtnShield:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    if (sender.isSelected) {
        _shieldBean.shieldStat = [NSNumber numberWithInt:0];
    }else{
        _shieldBean.shieldStat = [NSNumber numberWithInt:1];
    }
    
}
-(void)setShieldBean:(FireDevShieldBean *)shieldBean{
    _shieldBean = shieldBean;
    if (_shieldBean.devType) {
        if (_shieldBean.devType.intValue == 0) {
            if (_shieldBean.passage)
                _lableChannel.text = [NSString stringWithFormat:@"DTU-CH%@",_shieldBean.passage];
            
        } else {
            if (_shieldBean.passage)
                _lableChannel.text = [NSString stringWithFormat:@"CH%@",_shieldBean.passage];
        }
    }
    if(_shieldBean.shieldStat){
        if(_shieldBean.shieldStat.intValue == 0){
            _btnShield.selected = YES;
        }else{
            _btnShield.selected = NO;
        }
    }
    
}

@end

//
//  User.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class UserBean: BaseBean {
    var userId:String?
    var name:String?
    var email:String?
    var phone:String?
    var mobile:String?
    
    override public func mapping(map: Map) {
        userId <- map["userId"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        mobile <- map["mobile"]
    }
}

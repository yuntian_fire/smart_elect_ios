//
//  User.h
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "智慧消防-Swift.h"
#import "Const.h"
@interface User : NSObject
@property(strong,nonatomic)NSArray<LoginResBean *>* userList;
@property(strong,nonatomic)FirePrincipalBean *userSelf;
@property (retain,nonatomic) NSString *roleId;
@property (retain,nonatomic) NSString *roleName;
@property (retain,nonatomic) NSString *userName;
@property (strong,nonatomic)NSArray<FirePrincipalBean*> *maintUserList;
@property (strong,nonatomic)NSArray<FireProjBean*>*projArray;
@property(strong,nonatomic)NSData *pushDevToken;
+(User *)user;

@end

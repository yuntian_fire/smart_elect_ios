//
//  DevShieldTableViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/12/21.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "DevShieldTableViewController.h"
#import "FireShieldCell.h"
@interface DevShieldTableViewController ()

@end

@implementation DevShieldTableViewController
static NSString *fireshieldCell = @"FireShieldCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView.rowHeight = 40;
    UINib *nib = [UINib nibWithNibName:@"FireShieldCell" bundle: [NSBundle mainBundle]];
    [_tableView registerNib:nib forCellReuseIdentifier:fireshieldCell];
    
    
    // Do any additional setup after loading the view.
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    _tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    FireShieldCell *cell = [_tableView dequeueReusableCellWithIdentifier:fireshieldCell];
    cell.shieldBean = _dataSource[indexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _dataSource?_dataSource.count:0;
    
}



@end

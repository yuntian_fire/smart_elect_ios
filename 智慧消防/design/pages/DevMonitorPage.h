//
//  DevMonitorPage.h
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetWork.h"
#import "智慧消防-Bridging-Header.h"
@class DevInfoViewController;
@interface DevMonitorPage : UIView<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *btnTemp;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrent;
@property (weak, nonatomic) IBOutlet UIButton *btnResiduleCurrent;
@property (weak, nonatomic) IBOutlet UIButton *btnVol;
@property (weak, nonatomic) IBOutlet UITableView *alarmTableView;
@property (weak, nonatomic) IBOutlet UITableView *eventTableView;
@property (strong,nonatomic) FireDevMonitorBean *monitorBean;
@property (strong,nonatomic) NSArray * devAlarmArray;
@property (strong,nonatomic)NSArray *dtuEventList;
@property (strong,nonatomic)NSArray<NSArray<FireDevSenseStatBean*> *> *tempArray;
@property (strong,nonatomic)NSArray<NSArray<FireDevSenseStatBean*> *> *currentArray;
@property (strong,nonatomic)NSArray<NSArray<FireDevSenseStatBean*> *> *rCurrentArray;
@property (weak, nonatomic) IBOutlet LineChartView *lineChartView;
@property (strong,nonatomic)NSArray<NSArray<FireDevSenseStatBean*> *> *voltageArray;
@property (weak, nonatomic) IBOutlet UIButton *btnAlarmConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnAlarmProcess;
@property (weak, nonatomic) IBOutlet UIButton *btnPushMsg;
@property(strong,nonatomic)DevInfoViewController *selfVC;
@end

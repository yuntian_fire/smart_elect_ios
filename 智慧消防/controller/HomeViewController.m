//
//  HomeViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/11/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "HomeViewController.h"
#import "Const.h"
#import "User.h"
#import "BaseDao.h"
@interface HomeViewController ()
@property (copy,nonatomic)NSString *scanResult;
@end

@implementation HomeViewController
static NSString *homeDevCell = @"HomeDevCell";
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleMessage:) name:MESSAGE_ADD_DEV object:nil];
    UINib *nib = [UINib nibWithNibName:@"HomeDevCell" bundle:nil];
    [self.devTableView registerNib:nib forCellReuseIdentifier:homeDevCell];
    self.devTableView.rowHeight = 66;
    self.devTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.probDevArray = [[NSMutableArray alloc]init];
    
    [BaseDao sharedDao];
    
 

    
  
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

     [[NSNotificationCenter defaultCenter]postNotificationName:MESSAGE_MAIN_HANDLE object:@"home"];
    [self refresh];
}
-(void)handleMessage:(id)message{

    if ([[message name] isEqualToString:MESSAGE_ADD_DEV]) {
        self.target = VCTargetAddDev;
        _scanResult = [message object];
        [self performSegueWithIdentifier:@"home_to_dev_info" sender:self];
    }
}

-(void)refresh{
    UserIdBody *userIdBody = [[UserIdBody alloc]init];
    userIdBody.userId = [User user].userSelf.userId;
    userIdBody.roleId = [User user].roleId;
    [[NetWork network] getProbDev:userIdBody success:^(id data) {
        
        if (data) {
            [self.probDevArray removeAllObjects];
            NSArray<FireProbDevBean*> * probDevArray = data;
            for (FireProbDevBean *probDev in probDevArray) {
                for (FireDevInfoBean *devInfo in probDev.devInfoList) {
                    devInfo.projNm = probDev.projNm;
                    devInfo.projId = probDev.projId;
                    devInfo.projAddr = probDev.projAddr;
                    [self.probDevArray addObject:devInfo];
                }
            }
            dispatch_queue_t main_queue = dispatch_get_main_queue();
            dispatch_async(main_queue, ^{
                   [self.devTableView reloadData];
            });
         
        }
        
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    
    
    
    
    
}
- (IBAction)clickTheSafe:(id)sender {
    self.target = VCTargetAlarmList;
    [self performSegueWithIdentifier:@"home_to_warn_list" sender:self];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if (_target == VCTargetAlarmList) {
        ((DevWarnListController *)[segue destinationViewController]).vcSelf = _target;
    }else if(_target == VCTargetDevInfo){
        DevInfoViewController *controller = (DevInfoViewController*)[segue destinationViewController];
        controller.devInfoBean = _probDevArray[_rowIndex];
    }else if(_target == VCTargetAddDev){
        DevInfoViewController *controller = (DevInfoViewController *)[segue destinationViewController];
        controller.devBasic = [[FireDevBasicInfo alloc]init];
        controller.devBasic.devId = _scanResult;
        
    }
    
    
    _target = -1;
}



- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    HomeDevCell *cell = [self.devTableView dequeueReusableCellWithIdentifier:homeDevCell];
    FireDevInfoBean * probDev = self.probDevArray[indexPath.row];
    cell.labelProjNm.text = probDev.projNm;
    cell.labelDevId.text = probDev.devId;
    cell.labelLocation.text = probDev.projAddr;

    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = DATE_FORMAT;
    cell.labelAlarmTime.text = [formatter stringFromDate:probDev.alarmTime];
    cell.labelAlarmType.text = probDev.alarmType;
    switch (probDev.devStatus.intValue) {
        case 0:
            cell.imgHomeDev.image = [UIImage imageNamed:@"list_offline"];
            break;
        case 1:
            cell.imgHomeDev.image = [UIImage imageNamed:@"list_offline"];
            break;
        case 2:
            cell.imgHomeDev.image = [UIImage imageNamed:@"home_warn"];
            break;
        case 3:
            cell.imgHomeDev.image = [UIImage imageNamed:@"list_fault"];
            break;
        default:
            break;
    }
    
    
    
    
    
    return cell;
}
-(void)viewDidDisappear:(BOOL)animated{
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.probDevArray?_probDevArray.count:0;
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    
}

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
    
}

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     _rowIndex =indexPath.row;
    _target = VCTargetDevInfo;
    [self performSegueWithIdentifier:@"home_to_dev_info" sender:self];
}

-(void)viewDidUnload{
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end

//
//  FireUserInfoBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireUserInfoBean: BaseBean {
    var projNum:NSNumber?
    var deviceNum:NSNumber?
    var norDevNum:NSNumber?
    var subUserNum:NSNumber?
    var createDate:Date?
    
    override public func mapping(map: Map) {
        projNum <- map["projNum"]
        deviceNum <- map["deviceNum"]
        norDevNum <- map["norDevNum"]
        subUserNum <- map["subUserNum"]
        createDate <- (map["createDate"],DateTransform())
    }
}

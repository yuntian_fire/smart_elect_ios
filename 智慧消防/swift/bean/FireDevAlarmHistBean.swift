//
//  FireDevAlarmHistBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/11.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevAlarmHistBean: BaseBean {
    var alarmHistList:Array<FireDevInfoBean>?
    var yesAlarmCount:Array<NSNumber>?
    var todAlarmCount:Array<NSNumber>?
    override public func mapping(map: Map) {
        alarmHistList <- map["alarmHistList"]
        yesAlarmCount <- map["yesAlarmCount"]
        todAlarmCount <- map["todAlarmCount"]
    }
    
}

//
//  ProjBasicPage.m
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "ProjBasicPage.h"
#import "MaintHeadCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>
#import "ProjInfoViewController.h"
@implementation ProjBasicPage{
    BOOL _loadLocalImage;
}
static NSString *maintHeadCell = @"MaintHeadCell";
-(void)awakeFromNib{
    [super awakeFromNib];
    _frame = self.frame;
    UINib *nib = [UINib nibWithNibName:@"MaintHeadCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:maintHeadCell];
    self.tableView.rowHeight = 50;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableData = [[NSMutableArray alloc]init];
   
}

-(void)layoutSubviews{
    [super layoutSubviews];
    _scrollView.contentSize = self.frame.size;
}
-(void)initDropProvince{
    NSArray *province = [[BaseDao sharedDao] getAreaProvince];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    Area *selArea = nil;
    for (Area *area in province) {
        [array addObject: [[EBDropdownListItem alloc]initWithItem:nil itemName:area.name]];
        if ([area.code isEqualToString:_projBean.prov]) {
            selArea = area;
        }
    }
    _dropProvince.customData = province;
    self.dropProvince.dataSource = array;
    if (!selArea) {
        selArea = [province firstObject];
    }
    _dropProvince.textLabel.text = selArea.name;
    [self initDropCity:selArea.pid.intValue];
}
-(void)initDropCity:(int)parentId{
    NSArray *city = [[BaseDao sharedDao] findAreaByParentId:parentId];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    Area *selArea = nil;
    for (Area *area in city) {
        [array addObject: [[EBDropdownListItem alloc]initWithItem:nil itemName:area.name]];
        if ([area.code isEqualToString:_projBean.city]) {
            selArea = area;
        }
    }
    _dropCity.customData = city;
    _dropCity.dataSource = array;
    if (!selArea) {
        selArea = [city firstObject];
    }
    _dropCity.textLabel.text = selArea.name;
    [self initDropCountry:selArea.pid.intValue];
}

-(void)initDropCountry:(int)parentId{
    NSArray *country = [[BaseDao sharedDao] findAreaByParentId:parentId];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    Area *selArea = nil;
    for (Area *area in country) {
        [array addObject: [[EBDropdownListItem alloc]initWithItem:nil itemName:area.name]];
        if ([area.code isEqualToString:_projBean.county]) {
             selArea = area;
         
        }
    }
    _dropCountry.customData = country;
    _dropCountry.dataSource = array;
    if (!selArea) {
        selArea = [country firstObject];
    }
    _dropCountry.textLabel.text = selArea.name;
}

- (IBAction)clickPosition:(id)sender {
    _selfVC.target = VCTargetChoosePosition;
    [_selfVC performSegueWithIdentifier:@"proj_info_to_cp" sender:_selfVC];
  
    
   
}





- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    MaintHeadCell *cell = [self.tableView dequeueReusableCellWithIdentifier:maintHeadCell];
    cell.dropMaintHead.dataSource = _dropAltLeaderNm.dataSource;
    cell.dropMaintHead.customData = _dropAltLeaderNm.customData;
    
    FirePrincipalBean *maintUser = _tableData[indexPath.row+1];
    cell.dropMaintHead.textLabel.text = maintUser.name;
    cell.fieldMaintHeadPhone.text = maintUser.mobile;
    
    cell.labelHeadTitle.text = [NSString stringWithFormat:@"维保负责人%ld:",indexPath.row+2];
    [cell.dropMaintHead setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        FirePrincipalBean *selectUser = cell.dropMaintHead.customData[cell.dropMaintHead.selectedIndex];
        [_tableData replaceObjectAtIndex:indexPath.row+1 withObject:selectUser];
        _projBean.optPrincInfoList = _tableData;
        
        FirePrincipalBean *maintUser = _tableData[indexPath.row+1];
        cell.dropMaintHead.textLabel.text = maintUser.name;
        cell.fieldMaintHeadPhone.text = maintUser.mobile;
    }];
    
    [cell.btnRemove addTarget:self action:@selector(clickCellRemove:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnRemove.tag = indexPath.row + 1;
    return cell;
}

-(void)clickCellRemove:(UIButton*)sender{
    [_tableData removeObjectAtIndex:sender.tag];
    _projBean.optPrincInfoList = _tableData;
    [_tableView reloadData];
    [self layoutTableView];
}

- (IBAction)clickAdd:(id)sender {
 
    [self.tableData addObject:[[FirePrincipalBean alloc]init]];
    [self.tableView reloadData];
    [self layoutTableView];
   
    
}
-(void)layoutTableView{
    self.tableView.translatesAutoresizingMaskIntoConstraints = YES;
    CGRect frame = self.tableView.frame;
    frame.size.height = (self.tableData.count - 1)*50;
    self.tableView.frame = frame;
    
    CGSize size = _frame.size;
    size.height = size.height + self.tableData.count*50;
    self.scrollView.contentSize = size;
//    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData != nil &&self.tableData.count>0? _tableData.count -1  : 0 ;
}

-(void)initData{
  
    
    EBDropdownListItem *projItem1 = [[EBDropdownListItem alloc]initWithItem:@"1" itemName:@"电器火灾"];
    EBDropdownListItem *projItem2 =  [[EBDropdownListItem alloc]initWithItem:@"2" itemName:@"水压监测"];
    EBDropdownListItem *projItem3 = [[EBDropdownListItem alloc]initWithItem:@"3" itemName:@"气体监测"];
    EBDropdownListItem *projItem4 = [[EBDropdownListItem alloc]initWithItem:@"4"  itemName:@"其它"];
    NSArray *projItems = [NSArray arrayWithObjects:projItem1,projItem2,projItem3,projItem4, nil];
    _dropProjType.dataSource = projItems;
    
    NSLog(@"proj.lat = %@",_projBean.lat);
    
    _fieldLat.text = _projBean.lat;
    _fieldLon.text = _projBean.lng;
    _fieldProjNm.text = _projBean.projNm;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_projBean.projType.intValue >=0 && _projBean.projType.intValue<=_dropProjType.dataSource.count) {
            [_dropProjType setSelectedIndex:_projBean.projType.intValue - 1];
        }else{
            [_dropProjType setSelectedIndex:_dropProjType.dataSource.count -1 ];
        }
    });
    _fieldEmail.text = _projBean.primPrincInfo.email;
    
    _fieldProjDetailAddr.text = _projBean.projAddr;
    _tvMark.text = _projBean.projRemark;
    
    if (_projBean.optPrincInfoList&&_projBean.optPrincInfoList.count>0) {
        [_tableData removeAllObjects];
        [_tableData addObjectsFromArray:_projBean.optPrincInfoList];
        
    }
    
//    [_tableView reloadData];

    [self selectProjLd];
    if (_projBean.optPrincInfoList && _projBean.optPrincInfoList.count>0) {
        [self selectAlt0];
    }
   

    [_dropProjLeaderNm setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        LoginResBean* customItem = _dropProjLeaderNm.customData[_dropProjLeaderNm.selectedIndex];
        if (![_projBean.primPrincInfo.userId isEqualToString:customItem.projUser.userId]) {
            _projBean.primPrincInfo = customItem.projUser;
            [self selectProjLd];
        }
        
        
    }];

    [_dropAltLeaderNm setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        FirePrincipalBean *altUser = _dropAltLeaderNm.customData[_dropAltLeaderNm.selectedIndex];
        [_tableData replaceObjectAtIndex:0 withObject:altUser];
        [self selectAlt0];

    }];

    [self initDropProvince];
    [_dropProvince setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        Area *area = _dropProvince.customData[_dropProvince.selectedIndex];

        if(![area.code isEqualToString:_projBean.prov]){
            _projBean.prov = area.code;
            [self initDropCity:area.pid.intValue];

            Area *firstArea = [_dropCity.customData firstObject];
            _projBean.city = firstArea.code;

            firstArea = [_dropCountry.customData firstObject];
            _projBean.county = firstArea.code;
        }


    }];

    [_dropCity setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        Area *area = _dropCity.customData[_dropCity.selectedIndex];

        if(![area.code isEqualToString:_projBean.city]){
            _projBean.city = area.code;

            [self initDropCountry:area.pid.intValue];
            Area *firstArea = [_dropCountry.customData firstObject];
            _projBean.county = firstArea.code;


        }

    }];

    [_dropCountry setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        Area *area = _dropCountry.customData[_dropCountry.selectedIndex];
        _projBean.county = area.code;
    }];
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL1,_projBean.picPath];
    NSURL *imgURL = [NSURL URLWithString:url];
//    [_topImg sd_setImageWithURL:imgURL
//                        placeholderImage:[UIImage imageNamed:@"img_default"]
//                                 options:SDWebImageRefreshCached];
    if(!_loadLocalImage){
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        if (_projBean.projId) {
            NSNumber *myPicTime = [userDefaults objectForKey:_projBean.projId];
            
            if (myPicTime&&(myPicTime.integerValue != _projBean.picTime.integerValue)) {
                [[SDImageCache sharedImageCache] removeImageForKey:url withCompletion:^{
                    [_topImg sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"img_default"]];
                }];
            }else{
                [_topImg sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"img_default"]];
            }
            NSLog(@"_projBean.picTime = %@",_projBean.picTime);
            [userDefaults setObject:_projBean.picTime forKey:_projBean.projId];
            [userDefaults synchronize];
        }
        
        
    }else{
        _loadLocalImage = NO;
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
          [self layoutTableView];
    });
    
    
  
}

-(void)selectProjLd{
    NSMutableArray *userArray = [[NSMutableArray alloc]init];
    
    LoginResBean *currentLeader;
    for (LoginResBean *user in [User user].userList) {
        [userArray addObject:[[EBDropdownListItem alloc]initWithItem:user.projUser.userId itemName:user.projUser.name]];
        if([_projBean.primPrincInfo.userId isEqualToString:user.projUser.userId]){
            currentLeader = user;
        }
    }
    _dropProjLeaderNm.dataSource = userArray;
    _dropProjLeaderNm.customData = [User user].userList;
    _dropProjLeaderNm.textLabel.text = _projBean.primPrincInfo.name;
    _fieldProjLeaderPhone.text = _projBean.primPrincInfo.mobile;
    
    if (currentLeader) {
        NSMutableArray *altArray = [[NSMutableArray alloc]init];
        for (FirePrincipalBean *altUser in currentLeader.maintUser) {
            [altArray addObject: [[EBDropdownListItem alloc]initWithItem:altUser.userId itemName:altUser.name]];
        }
        _dropAltLeaderNm.dataSource = altArray;
        _dropAltLeaderNm.customData = currentLeader.maintUser;
        _fieldAltLeaderPhone.text = ((FirePrincipalBean*)currentLeader.maintUser[_dropAltLeaderNm.selectedIndex]).mobile;
        [_tableView reloadData];
        
        
    }
    
    
    
   
   
}

-(void)setImage:(UIImage *)image{
    
    self.topImg.image = image;
    _loadLocalImage = YES;
    
}
-(void)selectAlt0{
    _projBean.optPrincInfoList = _tableData;
    _dropAltLeaderNm.textLabel.text = _projBean.optPrincInfoList[0].name;
    _fieldAltLeaderPhone.text = _projBean.optPrincInfoList[0].mobile;
}

-(void)setProjBean:(FireProjBean *)projBean{
    _projBean = projBean;
    [self initData];
    
}
-(FireProjBean *)projBean{
    _projBean.lat = _fieldLat.text;
    _projBean.lng = _fieldLon.text;
    _projBean.projNm = _fieldProjNm.text;
    _projBean.projAddr = _fieldProjDetailAddr.text;
    _projBean.projRemark = _tvMark.text;
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (FirePrincipalBean *user in _projBean.optPrincInfoList) {
        BOOL isContains = NO;
        for (FirePrincipalBean *user1 in array) {
            if ([user1.userId isEqualToString:user.userId]) {
                isContains = YES;
            }
        }
        if (!isContains) {
            [array addObject:user];
        }
    }
    _projBean.optPrincInfoList = array;
    return _projBean;
}

- (IBAction)clickCamera:(id)sender {
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    [info setObject:@"camera" forKey:@"to"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:MESSAGE_PAGE object:nil userInfo:info];
    
}
- (IBAction)clickPic:(id)sender {
    [_selfVC choosePhoto];
}
- (IBAction)clickToM:(id)sender {
    
    
}


@end

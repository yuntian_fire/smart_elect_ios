//
//  BaseDao.m
//  智慧消防
//
//  Created by yuntian on 2018/12/13.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "BaseDao.h"
#define TABLE_NAME_AREA @"FIRE_AREA"

@implementation BaseDao
-(void)dealloc{
    [_db close];
    _db = nil;
}

- (instancetype)initPrivate {
    if (self = [super init]) {
        NSString *doc =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)  lastObject];
        
        NSString *dbFilePath = [doc stringByAppendingPathComponent:@"db.sqlite"];
        _db = [FMDatabase databaseWithPath:dbFilePath];
        if([_db open]){
            [_db setShouldCacheStatements:YES];
            
            if(![_db tableExists:TABLE_NAME_AREA]){
                NSString *sqlPath = [NSString stringWithFormat:@"%@/area.sql",[[NSBundle mainBundle]bundlePath]];
                NSInputStream *inputStream = [[NSInputStream alloc]initWithFileAtPath:sqlPath];
                inputStream.delegate = self;
                [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
                [inputStream open];
            }
        }
    }
    return self;
}

+ (instancetype)sharedDao {
    static BaseDao *instance = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        if (!instance) {
            instance = [[self alloc] initPrivate];
        }
    });
    return instance;
}
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{

    static NSMutableData * data;
    switch (eventCode) {
        case NSStreamEventHasBytesAvailable:{
            if (!data) {
                data = [[NSMutableData alloc]init];
            }
        
            uint8_t buf[1024];
            NSInteger len = 0;
            len = [(NSInputStream *)aStream read:buf maxLength:1024];  // 读取数据
            if (len) {
                [data appendBytes:(const void *)buf length:len];
            }
            break;
        }
           
        case NSStreamEventEndEncountered:{
            if (data) {
                _areaSql = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
               _areaSql = [_areaSql stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            }
            NSArray *sqls = [_areaSql componentsSeparatedByString:@";"];
            for (NSString *sql in sqls) {
                if (sql.length>0) {
                    [_db executeUpdate:sql];
                }
            }
            
            [aStream close];
            [aStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            aStream = nil;
            break;
        }

        default:
            
            break;
    }
}
- (NSArray*)getAreaProvince{
    NSString *sql = [NSString stringWithFormat:@"select * from %@ where %@ = 1",TABLE_NAME_AREA,@"level"];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    FMResultSet * set = [_db executeQuery:sql];
    while ([set next]) {
        [array addObject: [self createArea:set]];
    }
    
    return array;
}

-(Area*)createArea:(FMResultSet *)set{
  
    Area *area = [[Area alloc]init];
    area.pid = [NSNumber numberWithInt:[set intForColumn:@"PID"]];
    area.code = [set stringForColumn:@"CODE"];
    area.name = [set stringForColumn:@"NAME"];
    area.level = [NSNumber numberWithInt:[set intForColumn:@"LEVEL"]];
    area.cityCode = [set stringForColumn:@"CITY_CODE"];
    area.center = [set stringForColumn:@"CENTER"];
    area.parentId = [NSNumber numberWithInt:[set intForColumn:@"PARENT_ID"]];
    return area;
}

-(NSArray*)findAreaByParentId:(int)pid{
    NSString *sql = [NSString stringWithFormat:@"select * from %@ where PARENT_ID = %d",TABLE_NAME_AREA,pid];
    FMResultSet * set = [_db executeQuery:sql];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    while ([set next]) {
        [array addObject:[self createArea:set]];
    }
    return array;
}
-(Area*)findAreaByCode:(NSString*)code{
    NSString *sql = [NSString stringWithFormat:@"select * from %@ where CODE = '%@'",TABLE_NAME_AREA,code];
    FMResultSet * set = [_db executeQuery:sql];
    if ([set next]) {
        return [self createArea:set];
    }
    return nil;
}

@end

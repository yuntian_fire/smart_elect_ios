//
//  DevShieldTableViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/21.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "智慧消防-Swift.h"
@interface DevShieldTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong,nonatomic)UITableView *tableView;
@property (strong,nonatomic)NSArray<FireDevShieldBean *>* dataSource;
@end

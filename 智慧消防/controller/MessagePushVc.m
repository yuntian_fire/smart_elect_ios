//
//  MessagePushVc.m
//  智慧消防
//
//  Created by yuntian on 2018/12/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "MessagePushVc.h"
#import "AddInspViewController.h"
@interface MessagePushVc ()

@end

@implementation MessagePushVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_maintId) {
        _labelPushType.text = @"任务处理";
        _viewContentTaskType.translatesAutoresizingMaskIntoConstraints = YES;
        CGRect frame = _viewContentTaskType.frame;
        _viewContentTaskType.hidden = YES;
        frame.size.height = 0;
        _viewContentTaskType.frame = frame;
        _maintHandleRequest = [[FireDevMaintHandlerRequest alloc]init];
    }else{
        _labelPushType.text = @"任务派发";
        _maintRequest = [[FireMaintRequest alloc]init];
       
        _btnTHasDeal.enabled = NO;
        _btnTToSuper.enabled = NO;
    }
 
    
    
    NSArray *dataSource = [NSArray arrayWithObjects:[[EBDropdownListItem alloc]initWithItem:nil itemName:@"检修"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"更换"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"告警"], nil];
    _dropTaskType.dataSource = dataSource;
    dataSource = [NSArray arrayWithObjects:[[EBDropdownListItem alloc]initWithItem:nil itemName:@"一般"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"重要"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"紧急"], nil];
    _dropLevel.dataSource = dataSource;
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[User user].userSelf.userId,@"userId", nil];
    NSMutableArray *userDataSource = [NSMutableArray array];
    NSMutableArray *userData = [NSMutableArray array];
    
    UserBean *userBean = [[UserBean alloc]init];
    userBean.name = [User user].userSelf.name;
    userBean.userId = [User user].userSelf.userId;
    [userData addObject:userBean];
    [userDataSource addObject:[[EBDropdownListItem alloc]initWithItem:nil itemName:userBean.name]];
    [[NetWork network] getSubUsers:param success:^(id data) {
        for (UserBean *user in (NSArray*)data) {
            [userDataSource addObject:[[EBDropdownListItem alloc]initWithItem:nil itemName:user.name]];
        }
        [userData addObjectsFromArray:(NSArray*)data];
        _dropCC.dataSource = userDataSource;
        _dropCC.customData = userData;
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
    [_dropCC setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        UserBean *user = (UserBean *)_dropCC.customData[_dropCC.selectedIndex];
        _maintRequest.maintUserId = user.userId;
       
    }];
    
    [_dropTaskType setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        _maintRequest.taskType = [NSNumber numberWithInteger:_dropTaskType.selectedIndex + 1];
        
    }];
    
    [_dropLevel setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        _maintRequest.degree = [NSNumber numberWithInteger:_dropLevel.selectedIndex + 1];
    }];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)clickUntreated:(UIButton *)sender {
    sender.selected = YES;
    _btnTToSuper.selected = NO;
    _btnTHasDeal.selected = NO;
    if (_maintId) {
        _maintHandleRequest.maintType = [NSNumber numberWithInt:3];
    }
}
- (IBAction)clickTosuper:(UIButton *)sender {
    sender.selected = YES;
    _btnTuntreated.selected = NO;
    _btnTHasDeal.selected = NO;
    if (_maintId) {
        _maintHandleRequest.maintType = [NSNumber numberWithInt:3];
    }
    
}
- (IBAction)clickHasDeal:(UIButton *)sender {
    sender.selected = YES;
    _btnTuntreated.selected = NO;
    _btnTToSuper.selected = NO;
    if (_maintId) {
        _maintHandleRequest.maintType = [NSNumber numberWithInt:6];
    }
}
- (IBAction)clickToInsp:(UIButton *)sender {
    AddInspViewController *controller = [[AddInspViewController alloc]init];
    controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
    controller.devId = _devId;
    [self presentViewController:controller animated:NO completion:^{
       
    }];

}

-(FireMaintRequest *)maintRequest{
    _maintRequest.currentUserId = [User user].userSelf.userId;
    _maintRequest.currentRoleId = [User user].roleId;
    _maintRequest.devId = _devId;
    _maintRequest.des = _fieldTheme.text;
    _maintRequest.task = _tvMark.text;
    if (_fieldTime.text.length >0) {
        _maintRequest.expeTime = [NSNumber numberWithInt:_fieldTime.text.intValue];
    }
    
    
    
    return _maintRequest;
}

-(FireDevMaintHandlerRequest *)maintHandleRequest{
    _maintHandleRequest.maintId = _maintId;
    _maintHandleRequest.devId = _devId;
    if (_btnTHasDeal.isSelected) {
        _maintHandleRequest.comment = _tvMark.text;
    }else{
        _maintHandleRequest.process = _tvMark.text;
    }
    
    return _maintHandleRequest;
}
- (IBAction)clickBtnPush:(id)sender {
    if (_maintId) {
        [[NetWork network] processMaintenance:self.maintHandleRequest success:^(id data) {
            NSLog(@"%@",data);
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        } fail:^(NSString *message) {
            NSLog(@"%@",message);
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        } error:^(NSError *error) {
             NSLog(@"%@",error);
        }];
    }else{
        [[NetWork network]saveMaintence:self.maintRequest success:^(id data) {
             NSLog(@"%@",data);
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        } fail:^(NSString *message) {
             NSLog(@"%@",message);
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        } error:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
    
}


@end

//
//  FireInspPlanResponse.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireInspPlanRequest: BaseBean {
    var id:NSNumber?
    var inspPlanId:String?
    var devId:String?
    var inspType:NSNumber?
    var inspPeriod:NSNumber?
    var inspStat:NSNumber?
    var responUserId:String?
    var designUserId:String?
    var fireProjId:String?
    var pageNo:NSNumber?
    var startTime:Date?
    var endTime:Date?
    var startTimeFormat:String?
    var endTimeFormat:String?
    var startTimeFormat1:String?
    var endTimeFormat1:String?
    var pageSize:NSNumber?
    var currentUserId:String?
    var inspPlanStat:NSNumber?
    var createDateTime:String?
    
    override public func mapping(map: Map) {
        id <- map["id"]
        inspPlanId <- map["inspPlanId"]
        devId <- map["devId"]
        inspType <- map["inspType"]
        inspPeriod <- map["inspPeriod"]
        inspStat <- map["inspStat"]
        responUserId <- map["responUserId"]
        designUserId <- map["designUserId"]
        fireProjId <- map["fireProjId"]
        startTime <- (map["startTime"],DateTransform())
        endTime <- (map["endTime"],DateTransform())
        startTimeFormat <- map["startTimeFormat"]
        endTimeFormat <- map["endTimeFormat"]
        startTimeFormat1 <- map["startTimeFormat1"]
        endTimeFormat1 <- map["endTimeFormat1"]
        pageSize <- map["pageSize"]
        currentUserId <- map["currentUserId"]
        inspPlanStat <- map["inspPlanStat"]
        createDateTime <- map["createDateTime"]
    }

}

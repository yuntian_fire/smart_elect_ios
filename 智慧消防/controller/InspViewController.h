//
//  InspViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/11/29.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageControl.h"
#import "InspPlanPage.h"
#import "InspRecordPage.h"
#import "Const.h"
#import "NetWork.h"
#import "User.h"
#import "SearchView.h"
@interface InspViewController : UIViewController
@property (weak, nonatomic) IBOutlet PageControl *pageControl;
@property (weak, nonatomic) IBOutlet SearchView *searchView;

@end

//
//  DevInfoViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/4.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageControl.h"
#import "NetWork.h"
#import "DevBasicPage.h"
#import "DevMonitorPage.h"
#import "DevRDPage.h"
@interface DevInfoViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet PageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIView *navRightItem;
@property (weak, nonatomic) IBOutlet UIButton *btnSet;
@property (strong,nonatomic)FireDevInfoBean *devInfoBean;
@property (nonatomic) VCTarget target;
@property (strong,nonatomic)FireDevBasicInfo *devBasic;
-(void)openCamera;
-(void)refreshRd:(NSString *)refreshType;
-(void)refreshMonitor;
-(void)choosePhoto;
@end

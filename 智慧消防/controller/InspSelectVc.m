//
//  InspSelectVc.m
//  智慧消防
//
//  Created by yuntian on 2018/12/26.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "InspSelectVc.h"

@interface InspSelectVc ()

@end

@implementation InspSelectVc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    [self.view addSubview:self.contentView];
    // Do any additional setup after loading the view.
}

-(UIView *)contentView{
    if (!_contentView) {
        [[NSBundle mainBundle]loadNibNamed:@"InspSelVc" owner:self options:nil];
    }
    return _contentView;
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    CGRect frame = CGRectMake(15, self.view.frame.size.height/2-75, self.view.frame.size.width-30, 150);
    _contentView.frame = frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickAll:(id)sender {
        _clickAll();
    [self dismissViewControllerAnimated:NO
                             completion:nil];
}

- (IBAction)clickRandomInsp:(id)sender {
        _clickRandom();
    [self dismissViewControllerAnimated:NO
                             completion:nil];
}

- (IBAction)clickSysInsp:(id)sender {
        _clickSys();
    [self dismissViewControllerAnimated:NO
                             completion:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSArray * arrayViews = [UIApplication sharedApplication].keyWindow.subviews;
    if (arrayViews.count>0) {
        //array会有两个对象，一个是UILayoutContainerView，另外一个是UITransitionView，我们找到最后一个
        UIView * backView = arrayViews.lastObject;
        //我们可以在这个backView上添加点击事件，当然也可以在其子view上添加，如下：
        //        NSArray * subBackView = [backView subviews];
        //        backView = subBackView[0];  或者如下
        //        backView = subBackView[1];
        backView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(back)];
        [backView addGestureRecognizer:tap];
    }
}

-(void)back{
    [self dismissViewControllerAnimated:NO completion:nil];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

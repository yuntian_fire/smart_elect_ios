//
//  RadiusButton.h
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RadiusButton : UIButton
@property (nonatomic,assign) IBInspectable CGFloat radius;

@end

//
//  MaintListViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetWork.h"
#import "Util.h"
@interface MaintListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic)VCTarget vcSelf;
@property (strong,nonatomic)NSMutableArray<FireMaintResponse*>* maintArray;
@property (nonatomic)NSInteger rowIndex;
@end

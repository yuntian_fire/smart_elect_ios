//
//  UserIdBody.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class UserIdBody: BaseBean {
    var userId:String?
    var roleId:String?
    
    override public func mapping(map: Map) {
        userId <- map["userId"]
        roleId <- map["roleId"]
    }
}

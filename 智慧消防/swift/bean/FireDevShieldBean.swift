//
//  FireDevShieldBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevShieldBean: BaseBean {
    var devType:NSNumber?
    var circle:NSNumber?
    var partAddr:NSNumber?
    var passage:NSNumber?
    var shieldStat:NSNumber?
    override public func mapping(map: Map) {
        devType <- map["devType"]
        circle <- map["circle"]
        partAddr <- map["partAddr"]
        passage <- map["passage"]
        shieldStat <- map["shieldStat"]
    }
}

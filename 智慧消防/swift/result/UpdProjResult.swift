//
//  UpdProjResult.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/24.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class UpdProjResult: BaseBean {
    var data:PicResponse?
    var state:NSNumber?
    var message:String?
    override public func mapping(map: Map) {
        data <- map["data"];
        state <- map["state"]
        message <- map["message"]
    }
}

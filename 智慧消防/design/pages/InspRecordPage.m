//
//  InspRecordPage.m
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "InspRecordPage.h"
#import "InspRecordCell.h"
#import "InspViewController.h"
@implementation InspRecordPage
static NSString *inspRecordCell = @"InspRecordCell";
-(void)awakeFromNib{
    [super awakeFromNib];
    UINib *nib = [UINib nibWithNibName:@"InspRecordCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:inspRecordCell];
    self.tableView.rowHeight = 30;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _segTitle.selectedSegmentIndex = 3;
    
}


- (IBAction)segValueChanged:(UISegmentedControl *)sender {
    
    [self configSelRecord];
    [_tableView reloadData];
    
}

-(void)configSelRecord{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    switch (_segTitle.selectedSegmentIndex) {
        case 0:{
            for (FireInsRecordResponse *recordRespose in _inspRecordArray) {
                if (recordRespose.inspResult&&recordRespose.inspResult.intValue == 2) {
                    [array addObject:recordRespose];
                }
                
            }
            _inspSelRecordArray = array;
            
        }
            
            break;
        case 1:
            for (FireInsRecordResponse *recordRespose in _inspRecordArray) {
                if (recordRespose.inspResult&&recordRespose.inspResult.intValue == 0) {
                    [array addObject:recordRespose];
                }
                
            }
            _inspSelRecordArray = array;
            
            break;
        case 2:{
            for (FireInsRecordResponse *recordRespose in _inspRecordArray) {
                if (recordRespose.inspResult&&recordRespose.inspResult.intValue == 1) {
                    [array addObject:recordRespose];
                }
                
            }
            _inspSelRecordArray = array;
        }
            
            break;
        case 3:{
            
            _inspSelRecordArray = _inspRecordArray;
        }
            
            break;
            
        default:
            break;
    }
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    InspRecordCell *cell = [self.tableView dequeueReusableCellWithIdentifier:inspRecordCell];
    FireInsRecordResponse * inspRecord = self.inspSelRecordArray[indexPath.row];
    cell.labelDevId.text = inspRecord.devId;
    if (inspRecord.lastInspTime) {
        cell.labelLastInspTime.text = inspRecord.lastInspTime;
    }else{
        cell.labelLastInspTime.text = @"无";
    }

    if (inspRecord.inspPeriod) {
        cell.labelInspPeriod.text = [NSString stringWithFormat:@"%d天",inspRecord.inspPeriod.intValue];
    }else{
        cell.labelInspPeriod.text = @"自定义";
    }

    cell.labelToNextInspTime.text = [NSString stringWithFormat:@"%d天",inspRecord.legacyTime.intValue ];
    switch (inspRecord.inspResult.intValue) {
        case 0:
            cell.labelInspResult.text = @"正常";
            break;
        case 1:
            cell.labelInspResult.text = @"异常";
            break;

        case 2:
            cell.labelInspResult.text = @"超期";

            break;
        default:
            break;
    }

    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.inspSelRecordArray?_inspSelRecordArray.count:0;
}
-(void)setInspRecordArray:(NSArray<FireInsRecordResponse *> *)inspRecordArray{
    _inspRecordArray = inspRecordArray;
    [self configSelRecord];
    [self.tableView reloadData];
}


@end

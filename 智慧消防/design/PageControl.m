//
//  ProjPageControl.m
//  智慧消防
//
//  Created by yuntian on 2018/11/29.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "PageControl.h"

@implementation PageControl

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
-(void)awakeFromNib{
    [super awakeFromNib];
    [self initChild];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
  
}


-(void)initChild{
    
    CGRect frame = self.frame;
    frame.origin.y = 0;
    frame.origin.x = 0;
    if(_firstView){
        [_firstView removeFromSuperview];
        
    }
    if (_secondView) {
        [_secondView removeFromSuperview];
    }
    
    if (_thirdView) {
        [_thirdView removeFromSuperview];
    }
    if (self.pageCount >= 2) {
        [[NSBundle mainBundle]loadNibNamed:self.firstNib owner:self options:nil];
        [[NSBundle mainBundle]loadNibNamed:self.secondNib owner:self options:nil];
        self.firstView.frame = frame;
        //        frame.origin.x = frame.size.width;
        self.secondView.frame = frame;
        [self addSubview:_firstView];
        [self addSubview:_secondView];
        
        _firstView.hidden = NO;
        _secondView.hidden = YES;
    }
    if(self.pageCount >= 3){
        [[NSBundle mainBundle]loadNibNamed:self.thirdNib owner:self options:nil];
        //         frame.origin.x = frame.size.width*2;
        self.thirdView.frame = frame;
        [self addSubview:_thirdView];
        _thirdView.hidden = YES;
    }
    
    
    _currentPage = 0;
}

-(void)setCurrentPage:(NSInteger)currentPage{
//    NSInteger offSet = currentPage - self.currentPage;
//
//    for (int i=0; i<[[self subviews]count]; i++) {
//        UIView *child = [[self subviews] objectAtIndex:i];
//        CGRect frame = [[self subviews] objectAtIndex:i].frame;
//        frame.origin.x -= self.frame.size.width * offSet;
//        child.frame = frame;
//    }
//    _currentPage = currentPage;
    
    switch (currentPage) {
        case 1:
            _firstView.hidden = YES;
            _secondView.hidden = NO;
            if (_pageCount == 3) {
                _thirdView.hidden = YES;
            }
            break;
        case 2:
            _firstView.hidden = YES;
            _secondView.hidden = YES;
            _thirdView.hidden = NO;
            
            break;
            
        default:
            _firstView.hidden = NO;
            _secondView.hidden = YES;
            if (_pageCount == 3) {
                _thirdView.hidden = YES;
            }
            break;
    }
    _currentPage = currentPage;
}

@end

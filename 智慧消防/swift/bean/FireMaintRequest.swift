//
//  FireMaintRequest.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireMaintRequest: BaseBean {
    var devId:String?
    var des: String?
    var taskType:NSNumber?
    var degree:NSNumber?
    var expeTime:NSNumber?
    var task:String?
    var maintUserId:String?
    var fireProj:FireProj?
    var maintType:String?
    var maintState:NSNumber?
    var currentUserId:String?
    var currentRoleId:String?
    
    override public func mapping(map: Map) {
        devId <- map["devId"]
        des <- map["description"]
        taskType <- map["taskType"]
        degree <- map["degree"]
        expeTime <- map["expeTime"]
        task <- map["task"]
        maintUserId <- map["maintUserId"]
        fireProj <- map["fireProj"]
        maintType <- map["maintType"]
        maintState <- map["maintState"]
        currentUserId <- map["currentUserId"]
        currentRoleId <- map["currentRoleId"]
    }
    

}

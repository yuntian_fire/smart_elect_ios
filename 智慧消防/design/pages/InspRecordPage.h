//
//  InspRecordPage.h
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "智慧消防-Swift.h"
@class InspViewController;
@interface InspRecordPage : UIView<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSArray<FireInsRecordResponse*> *inspRecordArray;
@property (strong,nonatomic) NSArray<FireInsRecordResponse*> *inspSelRecordArray;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segTitle;
@property (strong,nonatomic)InspViewController *selfVC;
@end

//
//  MonitorAlarmCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/17.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonitorAlarmCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelAlarmChannel;
@property (weak, nonatomic) IBOutlet UILabel *labelAlarmType;
@property (weak, nonatomic) IBOutlet UILabel *labelNoteMessage;
@property (weak, nonatomic) IBOutlet UILabel *labelAlarmTime;

@end

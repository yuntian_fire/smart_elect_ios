//
//  InspViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/11/29.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "InspViewController.h"

@interface InspViewController ()

@end

@implementation InspViewController{
    dispatch_block_t _refreshBlock;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    InspPlanPage* inspPlanPage = (InspPlanPage*)self.pageControl.firstView;
    InspRecordPage *inspRecordPage = (InspRecordPage *)self.pageControl.secondView;
    inspPlanPage.selfVC = self;
    inspRecordPage.selfVC = self;
    [self refreshPlan];
    [self refreshRecord];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:MESSAGE_MAIN_HANDLE object:@"insp"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNavMessage:) name:MESSAGE_NAV_CONTROL object:nil];
    
    [self addRefreshBlock];
    
}

-(void)refreshPlan{
    InspPlanPage* inspPlanPage = (InspPlanPage*)self.pageControl.firstView;
    UserIdBean *userIdBean = [[UserIdBean alloc]init];
    userIdBean.currentRoleId = [User user].roleId;
    userIdBean.currentUserId = [User user].userSelf.userId;
    [[NetWork network] getInspPlanList:userIdBean success:^(id data) {
        
        dispatch_queue_t main_queue = dispatch_get_main_queue();
        dispatch_async(main_queue, ^{
            inspPlanPage.inspPlanArray = data;
        });
        
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)refreshRecord{
    InspRecordPage *inspRecordPage = (InspRecordPage *)self.pageControl.secondView;
    UserIdBean *userIdBean = [[UserIdBean alloc]init];
    userIdBean.currentRoleId = [User user].roleId;
    userIdBean.currentUserId = [User user].userSelf.userId;
    [[NetWork network] getInspRecordList:userIdBean success:^(id data) {
        
        dispatch_queue_t main_queue = dispatch_get_main_queue();
        dispatch_async(main_queue, ^{
            inspRecordPage.inspRecordArray = data;
        });
        
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
  
}

-(void)addRefreshBlock{
    dispatch_queue_t queue =
    dispatch_queue_create("inspRefreshBlock", DISPATCH_QUEUE_SERIAL);
    _refreshBlock = dispatch_block_create(DISPATCH_BLOCK_ASSIGN_CURRENT, ^{
        [self refreshPlan];
        [self refreshRecord];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), queue, _refreshBlock);
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)),queue, _refreshBlock);
}

-(void)handleNavMessage:(id)message{
    if ([message isKindOfClass:NSClassFromString(@"NSConcreteNotification")]) {
        if([[message name] isEqualToString:MESSAGE_NAV_CONTROL]){
            if([[message object] intValue]>=0){
                [self.pageControl setCurrentPage:[[message object] intValue]];
            }else if([[message object] intValue] == -1){
                _searchView.isOpen = !_searchView.isOpen;
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    dispatch_block_cancel(_refreshBlock);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

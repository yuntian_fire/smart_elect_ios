//
//  FileDevInfoBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevInfoBean: BaseBean {
    var devId:String?
    var location:String?
    var devStatus:NSNumber?
    var sigtre:NSNumber?
    var alarmType:String?
    var alarmTime:Date?
    
    var projId:String?
    var projNm:String?
    var projAddr:String?
    
    override public func mapping(map: Map) {
      
        devId <- map["devId"]
        location <- map["location"]
        devStatus <- map["devStatus"]
        sigtre <- map["sigtre"]
        alarmType <- map["alarmType"]
        alarmTime <- (map["alarmTime"],DateTransform())
    }
}

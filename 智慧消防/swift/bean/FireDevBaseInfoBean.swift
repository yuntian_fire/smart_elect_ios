//
//  FireDevBasicInfo.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/13.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevBaseInfoBean: BaseBean {
    var devId:String?
    var location:String?
    var projId:String?
    var projNm:String?
    
    var projUserId:String?
    var projUserNm:String?
    var projUserMobile:String?
    var dtuStat:NSNumber?
    var onlineStat:NSNumber?
    var deviceStat:String?
    

    
    override public func mapping(map: Map) {
        devId <- map["devId"]
        location <- map["location"]
        projId <- map["projId"]
        projNm <- map["projNm"]
        projUserId <- map["projUserId"]
        projUserNm <- map["projUserNm"]
        projUserMobile <- map["projUserMobile"]
        dtuStat <- map["dtuStat"]
        onlineStat <- map["onlineStat"]
        deviceStat <- map["deviceStat"]

   
        
    }
}

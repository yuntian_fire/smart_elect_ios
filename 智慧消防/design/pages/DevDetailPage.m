//
//  DevDetailPage.m
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "DevDetailPage.h"
#import "UIColor+Utils.h"
#import "DevListCell.h"
#import "Const.h"
@implementation DevDetailPage
static NSString *devListCell = @"DevListCell";

-(void)awakeFromNib{
    [super awakeFromNib];
    UINib *nib = [UINib nibWithNibName:@"DevListCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:devListCell];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.rowHeight = 50;
    _countArray = [[NSMutableArray alloc]init];
    _devCountsArray = [[NSMutableArray alloc]init];
    [self initPieChartDesign];
}


- (void)initPieChartDesign{
    /* 基本样式 */
    //    self.pieChartView.delegate = self;//设置代理
    [self.pieChartView setExtraOffsetsWithLeft:5.f top:5.f right:5.f bottom:5.f];//饼状图距离边缘的间隙
    self.pieChartView.usePercentValuesEnabled = YES; //是否根据所提供的数据, 将显示数据转换为百分比格式
    self.pieChartView.dragDecelerationEnabled = YES;//拖拽饼状图后是否有惯性效果
    
    /* 设置饼状图中间的文本 */
    self.pieChartView.drawCenterTextEnabled = NO;//是否绘制中间的文本
    /* 设置饼状图扇形区块文本*/
    self.pieChartView.drawEntryLabelsEnabled = NO; //是否显示扇形区块文本描述
    /* 设置饼状图图例样式 */
    self.pieChartView.legend.enabled = NO;//显示饼状图图例解释说明
    self.pieChartView.chartDescription.enabled = NO; //是否显示饼状图名字
    /*饼状图交互*/
    self.pieChartView.rotationEnabled = YES;//是否可以选择旋转
    self.pieChartView.highlightPerTapEnabled = YES;//每个扇形区块是否可点击
    self.pieChartView.drawHoleEnabled = NO;//扇形是否是空心圆
    
    
 
}
-(void)setPieData{
    /*
     图例
     values : values数组
     label : 图例的名字
     */
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] init];//图例说明
    dataSet.values = _devCountsArray;
    /* 设置每块扇形区块的颜色 */
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    if(_btnNormal.selected){
        [colors addObject:[UIColor colorWithHexRGB:0x3F9EFB]];
    }
    if (_btnOffline.selected) {
         [colors addObject:[UIColor colorWithHexRGB:0x64C9CA]];
    }
   
    if (_btnFault.selected) {
        [colors addObject:[UIColor colorWithHexRGB:0x6CCA72]];
    }
    
    if (_btnWarn.selected) {
        [colors addObject:[UIColor colorWithHexRGB:0xFAD23F]];
    }

    
    dataSet.colors = colors;
    
    dataSet.sliceSpace = 2; //相邻区块之间的间距
    dataSet.selectionShift = 5;//选中区块时, 放大的半径
    
    //    dataSet.drawIconsEnabled = NO; //扇形区块是否显示图片
    
    //    dataSet.entryLabelColor = [UIColor redColor];//每块扇形文字描述的颜色
    //    dataSet.entryLabelFont = [UIFont systemFontOfSize:15];//每块扇形的文字字体大小
    
    dataSet.drawValuesEnabled = YES;//是否显示每块扇形的数值
    dataSet.valueFont = [UIFont systemFontOfSize:11];//每块扇形数值的字体大小
    dataSet.valueColors = colors;//每块扇形数值的颜色,如果数值颜色要一样，就设置一个色就好了
    
    
    /* 数值与区块之间的用于指示的折线样式*/
    dataSet.xValuePosition = PieChartValuePositionInsideSlice;//文字的位置
    dataSet.yValuePosition = PieChartValuePositionOutsideSlice;//数值的位置，只有在外面的时候，折线才有用
    dataSet.valueLinePart1OffsetPercentage = 0.8; //折线中第一段起始位置相对于区块的偏移量, 数值越大, 折线距离区块越远
    dataSet.valueLinePart1Length = 0.4;//折线中第一段长度占比
    dataSet.valueLinePart2Length = 0.6;//折线中第二段长度占比
    dataSet.valueLineWidth = 1;//折线的粗细
    dataSet.valueLineColor = [UIColor brownColor];//折线颜色
    
    
    //设置每块扇形数值的格式
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    dataSet.valueFormatter = [[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter];
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    
    
    self.pieChartView.data = data;
    
    /* 设置饼状图动画 */
    self.pieChartView.rotationAngle = 0.0;//动画开始时的角度在0度
    [self.pieChartView animateWithXAxisDuration:2.0f easingOption:ChartEasingOptionEaseOutExpo];//设置动画效果
}


- (IBAction)clickNormal:(id)sender {
    if(!(_btnNormal.selected&&!_btnOffline.selected&&!_btnWarn.selected&&!_btnFault.selected)){
        NSLog(@"normal.selected = %d",_btnNormal.selected);
        _btnNormal.selected = !_btnNormal.selected;
        NSLog(@"normal.selected = %d",_btnNormal.selected);
    }

    [self changePieData];
}
- (IBAction)clickOffline:(id)sender {
    if(!(!_btnNormal.selected&&_btnOffline.selected&&!_btnWarn.selected&&!_btnFault.selected)){
        _btnOffline.selected = !_btnOffline.selected;
    }
    [self changePieData];
}

- (IBAction)clickFault:(id)sender {
    if(!(!_btnNormal.selected&&!_btnOffline.selected&&!_btnWarn.selected&&_btnFault.selected)){
        _btnFault.selected = !_btnFault.selected;
    }
    
    [self changePieData];
}

- (IBAction)clickWarn:(id)sender {
    if(!(!_btnNormal.selected&&!_btnOffline.selected&&_btnWarn.selected&&!_btnFault.selected)){
        _btnWarn.selected = !_btnWarn.selected;
    }
    
    [self changePieData];
}

-(void)changePieData{
    [_devCountsArray removeAllObjects];
    
    if(!_devCountsArray){
        _devCountsArray = [[NSMutableArray alloc]init];
    }

    if(_btnNormal.isSelected){
        [_devCountsArray addObject:[[PieChartDataEntry alloc]initWithValue:[_countArray[0] doubleValue]]];
        
    }
    
    if(_btnOffline.isSelected){
           [_devCountsArray addObject:[[PieChartDataEntry alloc]initWithValue:[_countArray[1] doubleValue]]];
    }
    
    if(_btnFault.isSelected){
          [_devCountsArray addObject:[[PieChartDataEntry alloc]initWithValue:[_countArray[2] doubleValue]]];
    }
   
    if(_btnWarn.isSelected){
          [_devCountsArray addObject:[[PieChartDataEntry alloc]initWithValue:[_countArray[3] doubleValue]]];
    }

    [self setPieData];
  
}



- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    DevListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:devListCell];
    FireDevInfoBean *devInfo = self.devArray[indexPath.row];
    cell.labelDevId.text = devInfo.devId;
    cell.labelLocation.text = devInfo.location;
    cell.labelTime.text = [Util dateStrFromDate:devInfo.alarmTime];
    if(!devInfo.devStatus){
        return cell;
    }
    switch (devInfo.devStatus.intValue) {
        case 0:{
            cell.labelDevStat.text = @"正常";
            cell.imgDevStat.image = [UIImage imageNamed:@"dtu_normal"];
            cell.imgPoint.image = [UIImage imageNamed:@"point_normal"];
            cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
            
        }
           
            break;
            
        case 1:{
            cell.labelDevStat.text = @"离线";
            cell.imgDevStat.image = [UIImage imageNamed:@"dtu_offline"];
            cell.imgPoint.image = [UIImage imageNamed:@"point_offline"];
            cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_offline"];
        }
            
            break;
        case 2:{
            cell.labelDevStat.text = @"报警";
            cell.imgDevStat.image = [UIImage imageNamed:@"dtu_warn"];
            cell.imgPoint.image = [UIImage imageNamed:@"point_alarm"];
            cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
        }
            
            break;
            
        case 3:{
            cell.labelDevStat.text = @"故障";
            cell.imgDevStat.image = [UIImage imageNamed:@"dtu_fault"];
            cell.imgPoint.image = [UIImage imageNamed:@"point_fault"];
            cell.imgOnlineStat.image = [UIImage imageNamed:@"dev_online"];
        }
            
            
            break;
        default:
            break;
    }
    
    
    return cell;
}


- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.devArray?_devArray.count:0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    NSInteger rawIndex = indexPath.row;
    NSNotification *notificaiton = [[NSNotification alloc]initWithName:MESSAGE_PAGE object:nil userInfo:info];
    [info setValue:[NSNumber numberWithInteger:rawIndex] forKey:@"rowIndex"];
    [info setValue:@"dev_info" forKey:@"to"];
    [[NSNotificationCenter defaultCenter] postNotification:notificaiton];
    
}

-(void)setDevArray:(NSArray<FireDevInfoBean *> *)devArray{
    _devArray = devArray;
    [self.tableView reloadData];
    int normalCount = 0;
    int offlineCount = 0;
    int faultCount = 0;
    int alarmCount = 0;
    for (FireDevInfoBean *devInfo in devArray) {
        if (devInfo.devStatus) {
            switch (devInfo.devStatus.intValue) {
                case 0:
                    normalCount ++;
                    break;
                case 1:
                    offlineCount ++;
                    break;
                case 2:
                     alarmCount ++;
                 
                    break;
                case 3:
                      faultCount ++;
                    break;
                    
                default:
                    break;
            }
        }
    }
   
    [_countArray removeAllObjects];
    [_countArray addObject:[NSNumber numberWithInt:normalCount]];
    [_countArray addObject:[NSNumber numberWithInt:offlineCount]];
    [_countArray addObject:[NSNumber numberWithInt:faultCount]];
    [_countArray addObject:[NSNumber numberWithInt:alarmCount]];
    [self changePieData];
    
}

-(void)setProjBean:(FireProjBean *)projBean{
    _projBean = projBean;
    _labelTotalCount.text = _projBean.totalCount.stringValue;
    _labelNormal.text = _projBean.nomal.stringValue;
    _labelOffline.text = _projBean.offline.stringValue;
    _labelAlarm.text = _projBean.alarm.stringValue;
    _labelFault.text = _projBean.fault.stringValue;
    
}
@end

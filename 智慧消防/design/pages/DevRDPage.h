//
//  DevRDPage.h
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "智慧消防-Swift.h"
#import "EBDropdownListView.h"
@class DevInfoViewController;
@interface DevRDPage : UIView <UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)FireDevAppConfigBean *devConfigBean;
-(void)changeDevRuntime;
@property (weak, nonatomic) IBOutlet UILabel *labelDevRunTime;
@property (weak, nonatomic) IBOutlet UIButton *btnUnit;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropDev;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropRelay;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropEx;

@property (weak, nonatomic) IBOutlet UILabel *labelDevMode;
@property (weak, nonatomic) IBOutlet UILabel *labelDevId;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollSenserTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableDTU;
@property (weak, nonatomic) IBOutlet UITableView *tableSenser;

@property (weak, nonatomic) IBOutlet UITextField *fieldHBTime;
@property (strong,nonatomic)NSArray<NSArray<ConfigSense*>*> *detList;
@property (strong,nonatomic)NSArray<NSString *>*sensorTitlesList;
@property (strong,nonatomic)NSArray<ConfigSense*>*dtuArray;
@property (strong,nonatomic)NSArray<ConfigSense*>*sensorArray;
@property (nonatomic)BOOL isSet;
@property (strong,nonatomic)DevInfoViewController *selfVC;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;

@property (weak, nonatomic) IBOutlet UIButton *btnGetDevConfig;

@property (weak, nonatomic) IBOutlet UIView *viewTitle;

@end

//
//  EMViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/11/29.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "EMViewController.h"
@interface EMViewController ()

@end

@implementation EMViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
 
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickWarnList:(id)sender {
    self.target = VCTargetAlarmList;
    [self performSegueWithIdentifier:@"em_to_warn_list" sender:self];
}
- (IBAction)clickFaultList:(id)sender {
    self.target = VCTargetFaultList;
    [self performSegueWithIdentifier:@"em_to_warn_list" sender:self];
}
- (IBAction)clickMaintTask:(id)sender {
    self.target = VCTargetMaintTask;
    [self performSegueWithIdentifier:@"em_to_maint" sender:self];
}
- (IBAction)clickMaintRecord:(id)sender {
    self.target = VCTargetMaintRecord;
    [self performSegueWithIdentifier:@"em_to_maint" sender:self];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:MESSAGE_MAIN_HANDLE object:@"em"];
   
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if (self.target == VCTargetAlarmList||self.target == VCTargetFaultList) {
        ((DevWarnListController *)[segue destinationViewController]).vcSelf = self.target;
    }else if(self.target == VCTargetMaintTask||self.target == VCTargetMaintRecord){
        ((MaintListViewController *)[segue destinationViewController]).vcSelf = self.target;
    }
   self.target = -1;
}


@end

//
//  FireDevSenseStatBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevSenseStatBean: BaseBean {
    var devType:NSNumber?
    var circle:NSNumber?
    var partAddr:NSNumber?
    var passage:NSNumber?
    var testName:NSNumber?
    var testDisplayName:String?
    var devValue:String?
    var unit:NSNumber?
    var unitName:String?
    var detectTime:Date?
    var xPoint:NSNumber?
    override public func mapping(map: Map) {
        devType <- map["devType"]
        circle <- map["circle"]
        partAddr <- map["partAddr"]
        passage <- map["passage"]
        testName <- map["testName"]
        testDisplayName <- map["testDisplayName"]
        devValue <- map["devValue"]
        unit <- map["unit"]
        unitName <- map["unitName"]
        detectTime <- (map["detectTime"],DateTransform())
    
    }
}

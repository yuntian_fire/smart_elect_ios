//
//  FireInsRecordResponse.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireInsRecordResponse: BaseBean {
    var inspRecordId:String?
    var inspPlanId:String?
    var devId:String?
    var inspType:NSNumber?
    var inspPeriod:NSNumber?
    var inspResult:NSNumber?
    var responUser:UserBean?
    var designUser:UserBean?
    var fireProj:FireProj?
    var lastInspTime:String?
    var nextInspTime:String?
    var thisIsnpTime:String?
    var legacyTime:String?
    var createDate:Date?
    var updateDate:Date?
    var startTimeFormat:String?
    var endTimeFormat:String?
    
    override public func mapping(map: Map) {
        inspRecordId <- map["inspRecordId"]
        inspPlanId <- map["inspPlanId"]
        devId <- map["devId"]
        inspPeriod <- map["inspPeriod"]
        inspResult <- map["inspResult"]
        responUser <- map["responUser"]
        designUser <- map["designUser"]
        fireProj <- map["fireProj"]
        lastInspTime <- map["lastInspTime"]
        nextInspTime <- map["nextInspTime"]
        legacyTime <- map["legacyTime"]
        createDate <- (map["createDate"],DateTransform())
        updateDate <- (map["updateDate"],DateTransform())
        startTimeFormat <- map["startTimeFormat"]
        endTimeFormat <- map["endTimeFormat"]
        
    }
}

//
//  ConfigExt.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class ConfigExt: BaseBean {
    var devType:NSNumber?
    var circle:NSNumber?
    var partAddr:NSNumber?
    var passage:NSNumber?
    var expSwitch:NSNumber?
    var expPulse:NSNumber?
    
    override public func mapping(map: Map) {
        devType <- map["devType"]
        circle <- map["circle"]
        partAddr <- map["partAddr"]
        passage <- map["passage"]
        expSwitch <- map["expSwitch"]
        expPulse <- map["expPulse"]
    }
}

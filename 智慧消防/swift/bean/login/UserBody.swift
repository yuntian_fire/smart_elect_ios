//
//  UserBody.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/7.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import Foundation
import ObjectMapper
public class UserBody:BaseBean {

    //这里要定义好需要转换的内容相对于的字段和类型,以后日后方便赋值转换
    var userName: String?
    var password: String?
    
    // Mappable
    override public func mapping(map: Map) {
        userName <- map["userName"]
        password <- map["password"]
        
    }
    
    
}

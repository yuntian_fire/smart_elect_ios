//
//  PagerPopViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/20.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSYViewPagerVC.h"
#import "智慧消防-Swift.h"
@interface PagerPopViewController : LSYViewPagerVC<LSYViewPagerVCDataSource,LSYViewPagerVCDelegate>
@property(strong,nonatomic)FireSendCmdBean *cmd;
@property (strong,nonatomic)NSArray<NSArray<FireDevShieldBean*>*> *detList;
@property (strong,nonatomic)NSArray<NSString *>*sensorTitlesList;
@end

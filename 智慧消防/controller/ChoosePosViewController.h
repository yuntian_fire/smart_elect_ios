//
//  ChoosePosViewController.h
//  智慧消防
//
//  Created by yuntian on 2018/12/25.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Map/BMKMapView.h>
#import <BaiduMapAPI_Map/BMKPointAnnotation.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import "智慧消防-Swift.h"
@interface ChoosePosViewController : UIViewController <BMKMapViewDelegate,BMKLocationServiceDelegate>

@property (strong, nonatomic) IBOutlet UIView *rightItem;

@property (weak, nonatomic) IBOutlet BMKMapView *mapView;
@property (strong,nonatomic)FireProjBean *projBean;
@property (strong,nonatomic)FireDevBasicInfo *devBasicInfo;
@property (strong,nonatomic) BMKLocationService *locationService;
@end

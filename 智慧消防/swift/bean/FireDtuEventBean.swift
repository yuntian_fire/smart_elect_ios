//
//  FireDtuEventBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDtuEventBean: BaseBean {
    var eventType:NSNumber?
    var eventName:String?
    var eventTime:Date?
    override public func mapping(map: Map) {
        eventType <- map["eventType"]
        eventName <- map["eventName"]
        eventTime <- (map["eventTime"],DateTransform())
    }

}

//
//  InspPlanPage.m
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "InspPlanPage.h"
#import "InspPlanCell.h"
#import "Util.h"
#import "InspViewController.h"
#import "AddInspViewController.h"
#import "AddRandomInspVC.h"
#import "InspSelectVc.h"

@implementation InspPlanPage
static NSString *inspPlanCell = @"InspPlanCell";

-(void)awakeFromNib{
    [super awakeFromNib];
    UINib *nib = [UINib nibWithNibName:@"InspPlanCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:inspPlanCell];
    self.tableView.rowHeight = 30;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _selType = InspSelTypeAll;
    _segTitle.selectedSegmentIndex = -1;
}







- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    InspPlanCell *cell = [self.tableView dequeueReusableCellWithIdentifier:inspPlanCell];
    cell.inspPlan = self.inspPlanSelArray[indexPath.row];
    
   
    

    
    
    
    
    return cell;
}



- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.inspPlanSelArray?_inspPlanSelArray.count:0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AddRandomInspVC *controller = [[AddRandomInspVC alloc]init];
    controller.inspResponse = _inspPlanArray[indexPath.row];
    controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [_selfVC presentViewController:controller animated:NO completion:nil];
    
}
- (void)setInspPlanArray:(NSArray<FireInspPlanResponse *> *)inspPlanArray{

    _inspPlanArray = inspPlanArray;
  
    [self configSelInsp];
    [self.tableView reloadData];
}
-(void)configSelInsp{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    switch (_selType) {
        case InspSelTypeAll:
            _inspPlanSelArray = _inspPlanArray;
            break;
        case InspSelTypeSelfAdd:
        {
            for (FireInspPlanResponse *inspResoponse in _inspPlanArray) {
                if (inspResoponse.inspPlanStat&&inspResoponse.inspPlanStat.intValue == 0) {
                    [array addObject:inspResoponse];
                    
                }
                
                
            }
            _inspPlanSelArray = array;
            
            
        }
            
            break;
            
        case InspSelTypeSys:
        {
            for (FireInspPlanResponse *inspResoponse in _inspPlanArray) {
                if (inspResoponse.inspPlanStat&&inspResoponse.inspPlanStat.intValue == 1) {
                    [array addObject:inspResoponse];
                    
                }
                
                
            }
            _inspPlanSelArray = array;
            
            
        }
            
            break;
        default:
            break;
    }
    
}

- (IBAction)segValueChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//         AddInspViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"add_insp_vew_controller"];
//
        AddInspViewController *controller = [[AddInspViewController alloc]init];
        controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [_selfVC presentViewController:controller animated:NO completion:nil];
    }else if(sender.selectedSegmentIndex == 1){
        AddRandomInspVC *controller = [[AddRandomInspVC alloc]init];
        controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
        controller.lastVC = _selfVC;
        [_selfVC presentViewController:controller animated:NO completion:nil];
        
        
    }else if(sender.selectedSegmentIndex == 2){
        InspSelectVc*controller = [[InspSelectVc alloc]init];
        controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [controller setClickAll:^{
            _selType = InspSelTypeAll;
            [self configSelInsp];
            [_segTitle setTitle:@"全部" forSegmentAtIndex:2];
            [_tableView reloadData];
        
        }];
        [controller setClickRandom:^{
            _selType = InspSelTypeSelfAdd;
            [self configSelInsp];
            [_segTitle setTitle:@"自添加" forSegmentAtIndex:2];
            [_tableView reloadData];
            
        }];
        [controller setClickSys:^{
            _selType = InspSelTypeSys;
            [self configSelInsp];
            [_segTitle setTitle:@"系统指派" forSegmentAtIndex:2];
            [_tableView reloadData];
            
        }];
        
        [_selfVC presentViewController:controller animated:NO completion:nil];
    }
      _segTitle.selectedSegmentIndex = -1;
}



@end

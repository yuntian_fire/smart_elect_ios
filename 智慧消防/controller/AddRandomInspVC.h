//
//  AddRandomInspVC.h
//  智慧消防
//
//  Created by yuntian on 2018/12/26.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EBDropdownListView.h"
#import "NetWork.h"
#import "InspViewController.h"
@interface AddRandomInspVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelCurrentStat;
@property (weak, nonatomic) IBOutlet UILabel *labelInspP;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *fieldDevId;
@property (weak, nonatomic) IBOutlet UITextField *fieldInspCycle;
@property (weak, nonatomic) IBOutlet UITextField *fieldAsiPhone;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropAsiNm;
@property (weak, nonatomic) IBOutlet UILabel *labelProjNm;
@property (weak, nonatomic) IBOutlet UILabel *labelDevId;
@property (weak, nonatomic) IBOutlet UIView *devIdContentView;
@property (weak, nonatomic) IBOutlet UIView *devIdScanContentView;
@property (weak, nonatomic) IBOutlet UIButton *btnStartTime;
@property (weak, nonatomic) IBOutlet UIButton *btnEndTime;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong,nonatomic) FireInspPlanRequest *inspRequest;
@property (strong,nonatomic)FireInspPlanResponse *inspResponse;
@property (strong,nonatomic)InspViewController *lastVC;
@end

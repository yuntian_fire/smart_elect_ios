//
//  FireDevBasicInfo.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/13.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevBasicInfo: BaseBean {
    var devId:String?
    var location:String?
    var projId:String?
    var projNm:String?
    var devLng:String?
    var devLat:String?
    var picPath:String?
    var scene:NSNumber?
    var sceneRemark:NSString?
    var devRemark:NSString?
    var devModel:NSString?
    var onlineStat:NSNumber?
    var primPrincInfo:FirePrincipalBean?
    var picTime:NSNumber?
    
    override public func mapping(map: Map) {
        devId <- map["devId"]
        location <- map["location"]
        projId <- map["projId"]
        projNm <- map["projNm"]
        devLng <- map["devLng"]
        devLat <- map["devLat"]
        picPath <- map["picPath"]
        scene <- map["scene"]
        sceneRemark <- map["sceneRemark"]
        devRemark <- map["devRemark"]
        devModel <- map["devModel"]
        onlineStat <- map["onlineStat"]
        primPrincInfo <- map["primPrincInfo"]
        picTime <- map["picTime"]
        
    }
}

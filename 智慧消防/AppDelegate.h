//
//  AppDelegate.h
//  智慧消防
//
//  Created by yuntian on 2018/11/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/SDWebImageDownloader.h>
#import <SDWebImage/SDWebImageManager.h>
#import <BaiduMapAPI_Base/BMKMapManager.h>
#import <TACCore/TACCore.h>
#import <TACMessaging/XGPush.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>{
    BMKMapManager* _mapManager;
}

@property (strong, nonatomic) UIWindow *window;


@end


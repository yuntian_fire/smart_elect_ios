//
//  HomeDevCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDevCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelProjNm;
@property (weak, nonatomic) IBOutlet UILabel *labelLocation;
@property (weak, nonatomic) IBOutlet UILabel *labelAlarmType;
@property (weak, nonatomic) IBOutlet UILabel *labelAlarmTime;
@property (weak, nonatomic) IBOutlet UILabel *labelDevId;
@property (weak, nonatomic) IBOutlet UIImageView *imgHomeDev;

@end

//
//  InspPlanCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "智慧消防-Swift.h"
#import "Util.h"
@interface InspPlanCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDevId;
@property (weak, nonatomic) IBOutlet UILabel *labelCreateTime;
@property (weak, nonatomic) IBOutlet UILabel *labelInspPeriod;
@property (weak, nonatomic) IBOutlet UILabel *labelInspStat;
@property (weak, nonatomic) IBOutlet UILabel *labelInspType;
@property (strong,nonatomic) FireInspPlanResponse *inspPlan;

@end

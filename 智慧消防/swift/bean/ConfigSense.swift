//
//  ConfigSense.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class ConfigSense: BaseBean {
    var devType:NSNumber?
    var circle:NSNumber?
    var partAddr:NSNumber?
    var passage:NSNumber?
    var testName:NSNumber?
    var ctrlSensStat:NSNumber?
    var zeroAdjust:String?
    var alarmThresTwo:String?
    var installPos:NSNumber?
    var alarmDelay:NSNumber?
    var number:NSNumber?
    var measuring:NSNumber?
    var elecRatio:NSNumber?
    var devValue:String?
    var unit:String?
    override public func mapping(map: Map) {
        devType <- map["devType"]
         circle <- map["circle"]
         partAddr <- map["partAddr"]
         passage <- map["passage"]
         testName <- map["testName"]
         ctrlSensStat <- map["ctrlSensStat"]
         zeroAdjust <- map["zeroAdjust"]
         alarmThresTwo <- map["alarmThresTwo"]
         installPos <- map["installPos"]
         alarmDelay <- map["alarmDelay"]
         number <- map["number"]
         measuring <- map["measuring"]
         elecRatio <- map["elecRatio"]
         devValue <- map["devValue"]
         unit <- map["unit"]
        
    }
    
}

//
//  DevRdSenserCell.m
//  智慧消防
//
//  Created by yuntian on 2018/12/19.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "DevRdSenserCell.h"

@implementation DevRdSenserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initDataSource];
}
-(void)initDataSource{
    // Initialization code
    NSArray *dataSource = [NSArray arrayWithObjects:[[EBDropdownListItem alloc]initWithItem:nil itemName:@"剩余电流"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"三相交流"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"单相交流"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"交流电压"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"温度"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"off"],nil];
    _dropSensor.dataSource = dataSource;
    
    [_dropSensor setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        int sensorType = 0;
        switch (_dropSensor.selectedIndex) {
            case 0:
                sensorType = 2;
                break;
            case 1:
                sensorType = 5;
                break;
                
            case 2:
                sensorType = 7;
                break;
                
            case 3:
                sensorType = 8;
                break;
                
            case 4:
                sensorType = 11;
                break;
        }
        if (sensorType != 0)
            _configSense.testName = [NSNumber numberWithInt:sensorType];
    }];
    
    dataSource = [NSArray arrayWithObjects:[[EBDropdownListItem alloc]initWithItem:nil itemName:@"A"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"B"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"C"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"L"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"LN"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"T-A"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"T-B"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"T-C"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"T-L"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"T-N"],[[EBDropdownListItem alloc]initWithItem:nil itemName:@"T-BOX"],nil];
    _dropMl.dataSource = dataSource;
    
    [_dropMl setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        _configSense.installPos = [NSNumber numberWithInteger:_dropMl.selectedIndex + 1];
    }];
    [_fieldRD addTarget:self action:@selector(rdValueChange:) forControlEvents:UIControlEventEditingChanged];
    [_fieldAlarmThre addTarget:self action:@selector(alarmThrValueChange:) forControlEvents:UIControlEventEditingChanged];
    
    _dropSensor.font = [UIFont systemFontOfSize:10];
    _dropMl.font = [UIFont systemFontOfSize:10];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setConfigSense:(ConfigSense *)configSense{
    _configSense = configSense;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self initSensors];
    });
}
-(void)initSensors{
    NSLog(@"_configSense.testName = %@",_configSense.testName);
    switch (_configSense.testName.intValue) {
        case 2:
            [_dropSensor setSelectedIndex:0];//剩余电流
            break;
        case 5:
            [_dropSensor setSelectedIndex:1];//三相电流
            break;
        case 7:
            [_dropSensor setSelectedIndex:2];//单相电流
            break;
        case 8:
            
            [_dropSensor setSelectedIndex:3];//交流电压
            break;
        case 11:
            [_dropSensor setSelectedIndex:4];//温度
            break;
        default:
            
            _dropSensor.textLabel.text = @"";
            break;
    }
    if (_configSense.installPos&& _configSense.installPos.intValue > 0 && _configSense.installPos.intValue <= 12) {
        [_dropMl setSelectedIndex:_configSense.installPos.intValue - 1];
    }
    
    _fieldAlarmThre.text = _configSense.alarmThresTwo;
    
    _labelCurrentData.text = [NSString stringWithFormat:@"%@%@",_configSense.devValue,_configSense.unit];
    _labelUnit.text = _configSense.unit;
    _fieldRD.text = _configSense.devValue;
    _fieldRD.text = _configSense.devValue;
    
}

- (void)rdValueChange:(UITextField*)sender {
    _configSense.devValue = sender.text;
   
}
- (void)alarmThrValueChange:(UITextField *)sender {
    _configSense.alarmThresTwo = sender.text;
}


@end

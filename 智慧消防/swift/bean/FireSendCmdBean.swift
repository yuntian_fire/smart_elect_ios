//
//  FireSendCmdBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireSendCmdBean: BaseBean {
    var devId:String?
    var srcAddr:String?
    var cmd:NSNumber?
    var shieldBeanList:Array<FireDevShieldBean>?
    override public func mapping(map: Map) {
        devId <- map["devId"]
        srcAddr <- map["srcAddr"]
        cmd <- map["cmd"]
        shieldBeanList <- map["shieldBeanList"]
    }
}

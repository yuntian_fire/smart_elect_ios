//
//  MessagePushVc.h
//  智慧消防
//
//  Created by yuntian on 2018/12/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EBDropdownListView.h"
#import "NetWork.h"
@interface MessagePushVc : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelPushType;
@property (weak, nonatomic) IBOutlet UITextField *fieldTheme;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropCC;
@property (weak, nonatomic) IBOutlet UITextView *tvMark;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropTaskType;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropLevel;
@property (weak, nonatomic) IBOutlet UITextField *fieldTime;
@property (weak, nonatomic) IBOutlet UIButton *btnTMsgPush;
@property (weak, nonatomic) IBOutlet UIButton *btnTToSuper;
@property (weak, nonatomic) IBOutlet UIButton *btnTuntreated;
@property (weak, nonatomic) IBOutlet UIButton *btnTHasDeal;
@property (weak, nonatomic) IBOutlet UIButton *btnTToInsp;
@property (copy,nonatomic) NSString *maintId;
@property (weak, nonatomic) IBOutlet UIView *viewContentTaskType;
@property (strong,nonatomic) FireMaintRequest *maintRequest;
@property (strong,nonatomic)FireDevMaintHandlerRequest *maintHandleRequest;
@property (copy,nonatomic)NSString *devId;

@end

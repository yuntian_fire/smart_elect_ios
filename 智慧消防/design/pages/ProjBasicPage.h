//
//  ProjBasicPage.h
//  智慧消防
//
//  Created by yuntian on 2018/12/5.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EBDropdownListView.h"
#import "NetWork.h"
#import "BaseDao.h"
#import "UIImageView+AFNetworking.h"
#import "ChoosePosViewController.h"
@class ProjInfoViewController;
@interface ProjBasicPage : UIView<UITableViewDelegate,UITableViewDataSource>{
    CGRect _frame;
    FireProjBean *_projBean;
}

@property (weak, nonatomic) IBOutlet EBDropdownListView *dropProvince;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropCity;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropCountry;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropProjType;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UITextField *fieldLat;
@property (weak, nonatomic) IBOutlet UITextField *fieldProjNm;
@property (weak, nonatomic) IBOutlet UITextField *fieldProjLeaderPhone;

@property (weak, nonatomic) IBOutlet UIImageView *topImg;
@property (strong,nonatomic) NSMutableArray *tableData;
@property (weak, nonatomic) IBOutlet UITextField *fieldLon;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropProjLeaderNm;
@property (weak, nonatomic) IBOutlet UITextField *fieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *fieldProjDetailAddr;
@property (weak, nonatomic) IBOutlet UITextView *tvMark;
@property (weak, nonatomic) IBOutlet EBDropdownListView *dropAltLeaderNm;
@property (weak, nonatomic) IBOutlet UITextField *fieldAltLeaderPhone;


@property (nonatomic)BOOL isSet;
@property (strong,nonatomic)ProjInfoViewController *selfVC;
-(void)setImage:(UIImage *)image;
-(void)setProjBean:(FireProjBean *)projBean;
-(FireProjBean*)projBean;
@end

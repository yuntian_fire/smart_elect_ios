//
//  MainViewController.m
//  智慧消防
//
//  Created by yuntian on 2018/11/27.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "MainViewController.h"
#import "Const.h"
#import "ProjInfoViewController.h"

#import <TACMessaging/XGPush.h>
@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    self.selectedIndex = 2;
    [self changeNavLeftItemsFromNib:@"HomeLeftItems"];
    [self changeNavRightItemsFromNid:@"HomeRightItems"];
    
    UserIdBody *userIdBody = [[UserIdBody alloc]init];
    userIdBody.userId = [User user].userSelf.userId;
    userIdBody.roleId = [User user].roleId;
    [[NetWork network] getProjsWithUserIdBody:userIdBody success:^(id data) {
        [User user].projArray = data;
    } fail:^(NSString *message) {
        NSLog(@"%@",message);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
  
   
    // Do any additional setup after loading the view.
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
  
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleMessage:) name:MESSAGE_MAIN_HANDLE object:NULL];
}
#pragma mark - 标题栏事件
- (IBAction)clickSignIn:(id)sender {
    
}
- (IBAction)clickListAndMode:(id)sender {
    _btnListMode.selected = !_btnListMode.selected;
    _btnMapMode.selected = !_btnMapMode.selected;

    if(_btnListMode.selected){
        [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_NAV_CONTROL object:@0L];
        _projPageIndex = 0;
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_NAV_CONTROL object:@1L];
        _projPageIndex = 1;
    }
    
}
- (IBAction)clickPlanAndRecord:(id)sender {
    _btnInspPlan.selected = !_btnInspPlan.selected;
    _btnInspRecord.selected = !_btnInspRecord.selected;
    if(_btnInspPlan.selected){
        [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_NAV_CONTROL object:@0L];
        _inspIndex = 0;
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_NAV_CONTROL object:@1L];
        _inspIndex = 1;
    }
}
- (IBAction)clickInspNavSear:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:MESSAGE_NAV_CONTROL object:@-1];
}

- (IBAction)clickLogout:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults removeObjectForKey:@"userId"];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    
    [UIApplication sharedApplication].keyWindow.rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"login_view_controller"];

    NSString *userName = [userDefaults objectForKey:@"userName"];
    [[XGPushTokenManager defaultTokenManager] unbindWithIdentifer:userName type:XGPushTokenBindTypeAccount];
    [userDefaults synchronize];
    
}
- (IBAction)clickProjNavSearch:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_NAV_CONTROL object:@-1];
    
}
- (IBAction)clickProjNavAdd:(id)sender {
    if (![[User user].roleId isEqualToString:ROLE_PROJ_PRIN]) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"只有项目管理员可以添加项目" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    self.target = VCTargetProjInfo;
    [self performSegueWithIdentifier:@"main_to_proj_info" sender:self];
}
- (IBAction)clickScan:(id)sender {
//    ScanCodeViewController *controller = [[ScanCodeViewController alloc]init];
//    [self.navigationController pushViewController:controller animated:YES];
    ScanCodeViewController *controller = [[ScanCodeViewController alloc]init];
    controller.isAddDev = YES;
    [self.navigationController pushViewController:controller animated:YES];
    
}
#pragma mark ---------------------

#pragma mark - 底部导航栏切换处理
-(void)handleMessage:(id)message{
    if ([message isKindOfClass:NSClassFromString(@"NSConcreteNotification")]) {
        if ([[message name] isEqualToString:MESSAGE_MAIN_HANDLE]) {
            NSString *msgStr = [message object];
            if ([msgStr isEqualToString:@"proj"]) {
                [self changeNavLeftItemsFromNib:@"ProjLeftItems"];
                [self changeNavRightItemsFromNid:@"ProjRightItems"];
                if (_projPageIndex == 0) {
                    _btnListMode.selected = true;
                    _btnMapMode.selected = false;
                }else{
                    _btnListMode.selected = false;
                    _btnMapMode.selected = true;
                }
            }else if([msgStr isEqualToString:@"home"]){
                [self changeNavLeftItemsFromNib:@"HomeLeftItems"];
                [self changeNavRightItemsFromNid:@"HomeRightItems"];
            }else if([msgStr isEqualToString:@"insp"]){
                [self changeNavLeftItemsFromNib:@"InspLeftItems"];
                [self changeNavRightItemsFromNid:@"InspRightItems"];
                if (_inspIndex == 0) {
                    _btnInspPlan.selected = true;
                    _btnInspRecord.selected = false;
                }else{
                    _btnInspPlan.selected = false;
                    _btnInspRecord.selected = true;
                }
            }else if([msgStr isEqualToString:@"em"]){
                [self changeNavLeftItemsFromNib:nil];
                [self changeNavRightItemsFromNid:nil];
            }else if([msgStr isEqualToString:@"pc"]){
                [self changeNavLeftItemsFromNib:nil];
                [self changeNavRightItemsFromNid:@"PcRightItems"];
            }
        }
        
    }
}


-(void) changeNavLeftItemsFromNib:(NSString*) nibName{
    [self.navigationItem setLeftBarButtonItems:nil];
    if (nibName!=nil) {
        [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_leftItems];
    }
    
 
}
- (void) changeNavRightItemsFromNid:(NSString *)nibName{
    [self.navigationItem setRightBarButtonItems:nil];
    if (nibName!=nil) {
        if (nibName!=nil) {
            [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_rightItems];
        }
    }
}



- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    self.navigationItem.title = item.title;
}

#pragma mark ---------------------------------


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if (_target == VCTargetProjInfo) {
        ProjInfoViewController *controller = (ProjInfoViewController *)[segue destinationViewController];
        controller.projBean = [[FireProjBean alloc]init];
        controller.projBean.primPrincInfo = [User user].userSelf;
        controller.projBean.optPrincInfoList = [NSArray arrayWithObject:[User user].userSelf];
    }
}


@end

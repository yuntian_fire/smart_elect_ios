//
//  FireProbDevBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireProbDevBean: BaseBean {
    var projId:String?
    var projNm:String?
    var projAddr:String?
    var devInfoList:Array<FireDevInfoBean>?
    
    override public func mapping(map: Map) {
        projId <- map["projId"]
        projNm <- map["projNm"]
        projAddr <- map["projAddr"]
        devInfoList <- map["devInfoList"]
        
    }
}

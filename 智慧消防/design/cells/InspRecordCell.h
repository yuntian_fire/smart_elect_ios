//
//  InspRecordCell.h
//  智慧消防
//
//  Created by yuntian on 2018/12/3.
//  Copyright © 2018年 yuntian. All rights reserved.
//


 #import <UIKit/UIKit.h>

@interface InspRecordCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelDevId;
@property (weak, nonatomic) IBOutlet UILabel *labelLastInspTime;
@property (weak, nonatomic) IBOutlet UILabel *labelInspPeriod;
@property (weak, nonatomic) IBOutlet UILabel *labelToNextInspTime;
@property (weak, nonatomic) IBOutlet UILabel *labelInspResult;

@end

//
//  CustomAnnotationView.h
//  智慧消防
//
//  Created by yuntian on 2018/12/25.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKAnnotationView.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>//引入地图功能所有的头文件

@interface CustomAnnotationView : BMKAnnotationView

@property (strong,nonatomic)UILabel *label;


@end

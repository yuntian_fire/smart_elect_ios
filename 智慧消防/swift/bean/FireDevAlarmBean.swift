//
//  FireDevAlarmBean.swift
//  智慧消防
//
//  Created by yuntian on 2018/12/18.
//  Copyright © 2018年 yuntian. All rights reserved.
//

import UIKit
import ObjectMapper
class FireDevAlarmBean: BaseBean {
    var devStatus:NSNumber?
    var devStatusNm:String?
    var circle:NSNumber?
    var partAddr:NSNumber?
    var passageName:String?
    var alarmType:NSNumber?
    var alarmTypeName:String?
    var alarmTime:Date?
    override public func mapping(map: Map) {
        devStatus <- map["devStatus"]
        devStatusNm <- map["devStatusNm"]
        circle <- map["circle"]
        partAddr <- map["partAddr"]
        passageName <- map["passageName"]
        alarmType <- map["alarmType"]
        alarmTypeName <- map["alarmTypeName"]
        alarmTime <- (map["alarmTime"],DateTransform())
    }
}

//
//  User.m
//  智慧消防
//
//  Created by yuntian on 2018/12/10.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "User.h"

@implementation User
+(User *)user{
    static User* instance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}
- (void)setUserList:(NSArray<LoginResBean *>*)userList{
    _userList = userList;
    for (LoginResBean *userBean in userList) {
        if ([USER_TYPE_SELF isEqualToString:userBean.userType]) {
            _userSelf = userBean.projUser;
            self.roleId = userBean.roleId;
            
            if ([_roleId isEqualToString:@"1"]) {
                self.roleName = @"系统管理员";
            }else if([_roleId isEqualToString:@"2"]){
                self.roleName = @"普通管理员";
            }else if([_roleId isEqualToString:@"3"]){
                self.roleName = @"省级代理";
            }else if([_roleId isEqualToString:@"4"]){
                self.roleName = @"市级代理";
            }else if([_roleId isEqualToString:@"5"]){
                self.roleName = @"县级或区域代理";
            }else if([_roleId isEqualToString:@"6"]){
                self.roleName = @"项目负责人";
            }else if([_roleId isEqualToString:@"7"]){
                self.roleName = @"维保人员";
            }else if([_roleId isEqualToString:@"8"]){
                self.roleName = @"运营人员";
            }
            self.maintUserList = userBean.maintUser;
            break;
        }
    }
}
@end



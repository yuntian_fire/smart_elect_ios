//
//  DevBasicPage.m
//  智慧消防
//
//  Created by yuntian on 2018/12/6.
//  Copyright © 2018年 yuntian. All rights reserved.
//

#import "DevBasicPage.h"
#import "DevInfoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>
#import "ScanCodeViewController.h"
@implementation DevBasicPage{
    BOOL _loadLocalImage;
}


-(void)awakeFromNib{
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleMessage:) name:MESSAGE_SCAN_RESULT object:nil];
    NSMutableArray *dataSource = [[NSMutableArray alloc]init];
    for (FireProjBean *proj in [User user].projArray) {
        EBDropdownListItem *item = [[EBDropdownListItem alloc]initWithItem:nil itemName:proj.projNm];
        [dataSource addObject:item];
    }
    _dropProj.dataSource = dataSource;
    _dropProj.customData = [User user].projArray;
    [_dropProj setSelectedBlock:^(EBDropdownListView *dropdownListView) {
        _devBasicInfo.projNm = [User user].projArray[_dropProj.selectedIndex].projNm;
        _devBasicInfo.projId = [User user].projArray[_dropProj.selectedIndex].projId;
        _devBasicInfo.primPrincInfo = [User user].projArray[_dropProj.selectedIndex].primPrincInfo;
        _fieldProjHead.text = _devBasicInfo.primPrincInfo.name;
        _fieldProjHeadPhone.text = _devBasicInfo.primPrincInfo.mobile;
        _fieldEmail.text = _devBasicInfo.primPrincInfo.email;
    }];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)handleMessage:(id)message{
    if ([MESSAGE_SCAN_RESULT isEqualToString:[message name]]) {
        if ([message object]) {
            _fieldDevId.text = [message object];
        }
    }
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return nil;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

-(FireDevBasicInfo *)devBasicInfo{
    _devBasicInfo.devLat = _fieldLat.text;
    _devBasicInfo.devLng = _fieldLon.text;
    _devBasicInfo.location = _fieldLocation.text;
    _devBasicInfo.devId = _fieldDevId.text;
    _devBasicInfo.devRemark = _tvRemark.text;
    

    
    return _devBasicInfo;
}

-(void)refreshLocation{
    _fieldLat.text = _devBasicInfo.devLat;
    _fieldLon.text = _devBasicInfo.devLng;
}
-(void)setDevBasicInfo:(FireDevBasicInfo *)devBasicInfo{
    _devBasicInfo = devBasicInfo;
    _fieldLat.text = devBasicInfo.devLat;
    _fieldLon.text = devBasicInfo.devLng;
    _fieldDevId.text = devBasicInfo.devId;
    _fieldLocation.text = devBasicInfo.location;
    if (!devBasicInfo.primPrincInfo) {
        for (FireProjBean *proj in [User user].projArray) {
            if ([proj.projId isEqualToString:_devBasicInfo.projId]) {
                devBasicInfo.primPrincInfo = proj.primPrincInfo;
            }
        }
    }
    
    _fieldProjHead.text = devBasicInfo.primPrincInfo.name;
    _fieldEmail.text = devBasicInfo.primPrincInfo.email;
    _fieldProjHeadPhone.text = devBasicInfo.primPrincInfo.mobile;
    
    
    _tvRemark.text = devBasicInfo.devRemark;
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL1,_devBasicInfo.picPath];
    NSURL *imgURL = [NSURL URLWithString:url];
    if (!_loadLocalImage&&_devBasicInfo.devId) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSNumber *myPicTime = [userDefaults objectForKey:_devBasicInfo.devId];
        
        if (myPicTime&&(myPicTime.integerValue != _devBasicInfo.picTime.integerValue)) {
            [[SDImageCache sharedImageCache] removeImageForKey:url withCompletion:^{
                [_imgTop sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"img_default"]];
            }];
        }else{
            [_imgTop sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"img_default"]];
        }
        NSLog(@"_projBean.picTime = %@",_devBasicInfo.picTime);
        [userDefaults setObject:_devBasicInfo.picTime forKey:_devBasicInfo.devId];
        [userDefaults synchronize];
    }else{
        
        _loadLocalImage = NO;
    }
    _dropProj.textLabel.text = _devBasicInfo.projNm;
    
}

-(void)setImage:(UIImage *)image{
    _imgTop.image = image;
}

- (IBAction)clickBtnCamera:(id)sender {
    [_selfVC openCamera];
    
}
- (IBAction)clickBtnPic:(id)sender {
    [_selfVC choosePhoto];
    
}
- (IBAction)clickBtnThird:(id)sender {
    
}
- (IBAction)clickBtnScan:(id)sender {
    ScanCodeViewController *controller = [[ScanCodeViewController alloc]init];
    [_selfVC.navigationController pushViewController:controller animated:YES];
    
}

-(void)setIsSet:(BOOL)isSet{
    _isSet = isSet;
    self.userInteractionEnabled = isSet;
}
- (IBAction)clickCp:(id)sender {
    _selfVC.target = VCTargetChoosePosition;
    [_selfVC performSegueWithIdentifier:@"dev_info_to_cp" sender:_selfVC];
}


@end
